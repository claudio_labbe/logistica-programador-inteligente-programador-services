from suds.client import Client
from suds import WebFault
from suds.transport.http import HttpAuthenticated
from suds.sudsobject import asdict
import logging

import xmltodict
from json import loads, dumps



def to_dict(input_ordered_dict):
    return loads(dumps(input_ordered_dict))
   
def build_response(item_json):   
    response = []
    #print(item_json["item"])
    
    for elem in item_json:
        if elem == "item":
            return build_response(item_json[elem])
        
        else:
            hash = {}
            for item in elem:
                if type(elem[item]).__name__ == "dict":
                    hash[item] = build_response(elem[item])
                else:
                    hash[item] = elem[item]
            response.append(hash)    
    return response       
        
    
    
    

        
    
    
    
    
user = "aelguedaa"
pwd = "Sofia2020"
url_auth="http://coqabroker1.copec.cl:7812/autorizacionSAP?wsdl"
#url_prog="http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl"

t = HttpAuthenticated(username=user, password=pwd)
client = Client(url=url_auth,transport=t,retxml=True)

response = client.service[0].ZprProgSapAutoriza()

string_response_xml = response.decode("utf-8")

json=to_dict(xmltodict.parse(string_response_xml))

#for j in json:
if "soapenv:Envelope" in json:
    if json["soapenv:Envelope"]["soapenv:Body"]["NS1:ZprProgSapAutorizaResponse"]["ERc"] == "1":
        print(json["soapenv:Envelope"]["soapenv:Body"]["NS1:ZprProgSapAutorizaResponse"]["EMessage"])
        exit(1)



base = json["soap-env:Envelope"]["soap-env:Body"]["n0:ZprProgSapAutorizaResponse"]

message = base["EMessage"]

erc = base["ERc"]
autoriza = base["EtAutoriza"]   
counc = base["EtCounc"]
groupname = base["EtGroupname"]

#res = build_response(autoriza)
res = build_response(counc)
print(res)
#print(erc)

#print(autoriza)