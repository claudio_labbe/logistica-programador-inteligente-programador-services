import boto3
import time
import math 

client = boto3.client('athena')

queryStart = client.start_query_execution(
    #QueryString = 'SELECT origin, destination, travel_time_minutes FROM time_estimation_travel_time LIMIT 10',
    QueryString = 'SELECT origin, destination, travel_time_minutes FROM "repositorio_optimizador"."time_estimation_travel_time" where origin=\'1208\' and destination=\'0000040022\' ',
    QueryExecutionContext = {
        'Database': 'repositorio_optimizador'
    },
    ResultConfiguration={
        'OutputLocation': 's3://athena-results-tests/loporto'
        
    }
)

#print(queryStart)

if queryStart["ResponseMetadata"]["HTTPStatusCode"] == 200:
    
    response = client.get_query_execution(
        QueryExecutionId=queryStart["QueryExecutionId"]
    )
    
   
    start_time = time.time()
    #t = 0
    while response["QueryExecution"]["Status"]["State"] == "QUEUED" or response["QueryExecution"]["Status"]["State"]=="RUNNING":
        response = client.get_query_execution(
            QueryExecutionId=queryStart["QueryExecutionId"]
        )
    end_time = time.time()
    print("--- %s seconds ---" % (end_time - start_time))
    print(response["QueryExecution"]["Status"])
    
    
    if response["QueryExecution"]["Status"]["State"]== "SUCCEEDED":
        #ahora puedo
        response = client.get_query_results(
            QueryExecutionId = queryStart["QueryExecutionId"]
        )
        
        #print(response)
        
        rows = response["ResultSet"]["Rows"]
        if len(rows)==2:
            ret = {}
            
            ret[rows[0]["Data"][0]["VarCharValue"]]=rows[1]["Data"][0]["VarCharValue"]
            ret[rows[0]["Data"][1]["VarCharValue"]]=rows[1]["Data"][1]["VarCharValue"]
            ret[rows[0]["Data"][2]["VarCharValue"]]=rows[1]["Data"][2]["VarCharValue"]
            
            minutes = float(ret["travel_time_minutes"])
            #transform hh:mm:ss
            
            h = math.floor(minutes/60)
            m = math.floor(minutes%60)
            minutes_remaining = (minutes%60) - m
            s = math.ceil(60*minutes_remaining)
            
            hhmmss=str(h)+":"+str(m)+":"+str(s)
            ret["duracion"]=hhmmss
            #return
            print(ret)
        else:
            print("no result")
        
                
    else:
        print("there was an error")
#No output location provided. 
#An output location is required either through 
#the Workgroup result configuration setting or as an API input