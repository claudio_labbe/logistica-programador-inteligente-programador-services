#from suds.client import Client
#from suds import WebFault
#from suds.transport.http import HttpAuthenticated
#from suds.sudsobject import asdict
from requests import Session
from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.
from zeep import Client
from zeep.transports import Transport

import logging
#import xmltodict
#para el lambda
from .xmltodict import xmltodict
from json import loads, dumps
import time


class soapServiceZeep:
    def to_dict(self,input_ordered_dict):
        return loads(dumps(input_ordered_dict))
    
    def build_response(self,item_json):   
        response = []
        
        for elem in item_json:
            if elem == "item":
                return self.build_response(item_json[elem])
            
            else:
                hash = {}
                for item in elem:
                    if type(elem[item]).__name__ == "dict":
                        hash[item] = self.build_response(elem[item])
                    else:
                        hash[item] = elem[item]
                response.append(hash)    
        return response     

    def recursive_asdict(self,d):
        """Convert Suds object into serializable format."""
        out = {}
        for k, v in asdict(d).items():
            if hasattr(v, '__keylist__'):
                out[k] = self.recursive_asdict(v)
            elif isinstance(v, list):
                out[k] = []
                for item in v:
                    if hasattr(item, '__keylist__'):
                        out[k].append(self.recursive_asdict(item))
                    else:
                        out[k].append(item)
            else:
                out[k] = v
        return out

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        #logging.getLogger('suds.client').setLevel(logging.DEBUG)
        #logging.getLogger('suds.transport').setLevel(logging.DEBUG)
        #logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)

    def client_info(self):
        print(self.client)
        
    def createClient(self,user,pwd,urlConn):
        #t = HttpAuthenticated(username=user, password=pwd)
        #self.client = Client(url=urlConn,transport=t)
        session = Session()
        session.auth = HTTPBasicAuth(user, pwd)
        self.client = Client(urlConn, transport=Transport(session=session))
        
    
    #entrega los mensajes del programador (usados para los combobox cuando das de baja pedidos o camiones)
    def get_combo_messages(self):
        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            response = self.client.service[0].ZBDP_INFO_PROGRAMACION()
            return self.recursive_asdict(response)
        except WebFault as e:
            print(e)
            return False
    
    #zeep
    def get_plan(self,fecha,plantCode):
        return self.get_plan_with_code(fecha,plantCode,2)
    
    def get_plan_with_code(self,fecha,plantCode,sapCode):
        #cliente tiene que tener la URL de get plan: zws_pr_obtiene_prog_sap
        #fecha yyyy-mm-dd
        #plantCode xxxx
        #ItClienteFecha = self.client.factory.create('ItClienteFecha')
        
        try:
            factory = self.client.type_factory('ns0')
            ItClienteFecha = factory.ZprogTtKunnrDatum()
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios.
            with self.client.settings(strict=False,raw_response=True):
                response = self.client.service.ZbdpObtieneProgramacion(fecha,sapCode,plantCode[0],ItClienteFecha)
            string_response_xml = response.content#.decode("utf-8")
            response_json=self.to_dict(xmltodict.parse(string_response_xml))
            
            base = response_json["soap-env:Envelope"]["soap-env:Body"]["n0:ZbdpObtieneProgramacionResponse"]
            
            res={}
            res["ERc"]=base["ERc"] if base["ERc"] != None else " "
            res["EMessage"]=base["EMessage"] if base["EMessage"] != None else " "
            res["EtVehicles"]=base["EtVehicles"] if base["EtVehicles"] != None else " "
            res["EtVehicleDet"]=base["EtVehicleDet"] if base["EtVehicleDet"] != None else " "
            res["EtPedidos"]=base["EtPedidos"] if base["EtPedidos"] != None else " "
            res["EtPedidoDet"]=base["EtPedidoDet"] if base["EtPedidoDet"] != None else " "
            res["EtCambioCamion"]=base["EtCambioCamion"] #if base["EtCambioCamion"] != None else " "
           
            
            return res
        except Exception as e:
            print("Err")
            print(e)
            return False
        
    #zeep
    #entrega los comentarios dado un pedido
    def get_message(self,vbeln):
        try:
            with self.client.settings(strict=False,raw_response=True):
                response = self.client.service.ZBDP_EXTRAS_PEDIDOS(vbeln)
            string_response_xml = response.content#.decode("utf-8")
            response_json=self.to_dict(xmltodict.parse(string_response_xml))
            
            base = response_json["soap-env:Envelope"]["soap-env:Body"]["n0:ZBDP_EXTRAS_PEDIDOSResponse"]
            #print(base)
            
            res = {}
            res["E_MESSAGE"]=base["E_MESSAGE"]
            res["E_RC"]=base["E_RC"]
            res["T_TEXTOS"]=base["T_TEXTOS"]
            
            return res#self.recursive_asdict(response)
        except Exception as e:
            print(e)
            return False
    
    #entrega los comentarios dado un pedido
    def get_all_message(self,plant,date):
        try:
            
            with self.client.settings(strict=False,raw_response=True):
                response = self.client.service.ZWS_BDP_OBTIENE_MENSAJES(date,plant)
                
            print("RESP")
            print(response)
            
            string_response_xml = response.content#.decode("utf-8")
            response_json=self.to_dict(xmltodict.parse(string_response_xml))
            
            
            base = response_json["soap-env:Envelope"]["soap-env:Body"]["n0:ZWS_BDP_OBTIENE_MENSAJESResponse"]
            
            res = {}
            #res["VBELN"]=base["VBELN"]
            #res["TIPO"]=base["TIPO"]
            res["TEXTOS"]=base["TTextos"]
            
            return res#self.recursive_asdict(response)
        except Exception as e:
            print(e)
            return False
    
    #entrega todos los pedidos como dados de baja, excepto los confirmados
    #se asocia a la funcionalidad LIMPIAR del programador
    def clear(self,fecha,plantCode): 
        return self.get_plan_with_code(fecha,plantCode,3)
        """
        #cliente tiene que tener la URL de get plan: zws_pr_obtiene_prog_sap
        #fecha yyyy-mm-dd
        #plantCode xxxx
        ItClienteFecha = self.client.factory.create('ItClienteFecha')

        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            parametro_dar_de_baja = 3
            response = self.client.service[0].ZbdpObtieneProgramacion(fecha,parametro_dar_de_baja,plantCode,ItClienteFecha)
            return self.recursive_asdict(response)
        except WebFault as e:
            print(e)
            return False
        """
        
    def yyyymmddToDateSap(self,yyyymmdd):
        year = yyyymmdd[0:4]
        month = yyyymmdd[4:6]
        day = yyyymmdd[6:8]
        ret = year+"-"+month+"-"+day
        return ret
    
    def hhmmssToSap(self,hhmmss):
        #print("hhmmss")
        #print(hhmmss)
        #print(self.hhmmmmToHour(hhmmss))
        #print(int(self.hhmmmmToHour(hhmmss)))
        h =  self.hhmmmmToHour(hhmmss)
        m =  self.hhmmmmToMinute(hhmmss)
        s =  self.hhmmmmToSeconds(hhmmss)
        return h+":"+m+":"+s;
  
    def hhmmmmToHour(self,hhmmss):
        h = hhmmss[0:2]
        return h
  
    def hhmmmmToMinute(self,hhmmss):
        m = hhmmss[2:4]
        return m
  
    def hhmmmmToSeconds(self,hhmmss):
        s = hhmmss[4:6]
        return s
    
    def confirm(self,input):
        #try:
        #TODO FIXIT
        IDatum = input["fecha"]    #"2020-05-06"
        IWerks = input["codigo_planta"]    #"1208"
        
        factory = self.client.type_factory('ns1')
        
        ItCambioCamion = factory.ZttdfAudCami()
        ItPedidos = factory.ZprogTtConfirmarProg()

        
        for pedido in input["items"]:
            ped = {}
            ped["Agrupador"] = pedido["AGRUPADOR"]
            ped["PosAgrupador"] = pedido["POS_AGRUPADOR"]
            ped["Vbeln"] = pedido["VBELN"]
            ped["Vbtyp"] = pedido["VBTYP"]
            ped["Auart"] = pedido["AUART"]
            ped["Bsark"] = pedido["BSARK"]
            ped["Vehicle"] = pedido["VEHICLE"]
            
            ped["StartDate"] = self.yyyymmddToDateSap(pedido["CONFIR_START_DATE"])
            ped["StartTime"] = self.hhmmssToSap(pedido["CONFIR_START_TIME"])
            ped["EndDate"] = self.yyyymmddToDateSap(pedido["PRO_ENDDA"])
            ped["EndTime"] = self.hhmmssToSap(pedido["PRO_ENDTI"])
            ped["Duracion"] = self.hhmmssToSap(pedido["DURACION"])
            if pedido["DURACION_GRUPO"].strip() != "":
                ped["DuracionGrupo"] = self.hhmmssToSap(pedido["DURACION_GRUPO"])
            ped["Volumen"] = ""
            
            ped["TpoCarga"] = pedido["TPO_CARGA"]
            ped["TpoDesca"] = pedido["TPO_DESCA"]
            #ped["Ruta"] = self.hhmmssToSap(pedido["DURACION"]) #recordar que duracion es tiempo ida y vuelta sin considerar los tiempos de carga y descarga.
            ped["Ruta"] = self.hhmmssToSap(pedido["RUTA"])
            ped["TpoCombi"] = pedido["TPO_COMBI"]
            
            
            #ped["Volumen"] = pedido["VOLUMEN"]
            ItPedidos.item.append(ped)
        print(ItPedidos)
        with self.client.settings(strict=True,raw_response=True):
            response = self.client.service.ZprProgSapConfirmar(IDatum, IWerks,[], ItPedidos)
        
        string_response_xml = response.content#.decode("utf-8")
        response_json=self.to_dict(xmltodict.parse(string_response_xml))
        #print(response_json['soap-env:Envelope']['soap-env:Body']['n0:ZprProgSapConfirmarResponse'])
        #base = response_json["soap-env:Envelope"]["soap-env:Body"]["n0:ZbdpObtieneProgramacionResponse"]
        return response_json['soap-env:Envelope']['soap-env:Body']['n0:ZprProgSapConfirmarResponse']    
        
        #except Exception as e:
        #    print(e)
        #    return False
        
      
    
    def deconfirm(self,input):
        CtPedidos = self.client.factory.create('ZprogTtDesconfirmarProg')
        IFechaFin = self.client.factory.create('date10')
        IFechaIni = self.client.factory.create('date10')
        IHoraFin = self.client.factory.create('time')
        IHoraIni = self.client.factory.create('time')
        print("DESCONOCNC:")
        print(input)
        try:
            #Se desconfirma un solo elemento, a diferencia de confirmar o guardar, que son varios.
            pedido = input["items"]
            
            ped = {}
            ped["Vbeln"] = pedido["VBELN"]
            ped["Vbtyp"] = pedido["VBTYP"]
            ped["Motivo"] = pedido["MOTIVO"]
            ped["Subrc"] = pedido["SUBRC"]
            ped["Message"] = pedido["MESSAGE"]
            #ped["DuracionCalcula"] = pedido[""]
            ped["Tipo"] = pedido["TIPO"]
            ped["Observacion"] = pedido["OBSERVACION"]
            
            CtPedidos.item.append(ped)
            
            #ZprProgSapDesconfirmar(ZprogTtDesconfirmarProg CtPedidos, date10 IFechaFin, date10 IFechaIni, time IHoraFin, time IHoraIni, )
            response = self.client.service[0].ZprProgSapDesconfirmar(CtPedidos, IFechaFin, IFechaIni, IHoraFin, IHoraIni )
            print(response)
            return response
            
        except WebFault as e:
            #print(e)
            return e
            
            
            
    def save_plan(self,input):
        #input = {"items": event["plan"],"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
        #ZprProgSapGrabar(ns0:date10 IDatum, ns0:char4 IWerks, ZttdfAudCami ItCambioCamion, ZprogTtConfirmarProg ItPedidos, )
        
        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            IDatum = input["fecha"]    #"2020-05-06"
            IWerks = input["codigo_planta"]    #"1208"
            
            #la lista de camiones. Esto no es tarea del programador, pero se podria hacer
            ItCambioCamion = self.client.factory.create('ZttdfAudCami')
            #la lista de pedidos
            ItPedidos = self.client.factory.create('ZprogTtConfirmarProg')
            
            #item = self.client.factory.create('item')  
            
            for pedido in input["items"]:
                #item = self.client.factory.create('item')    
                #FORMATEAR FECHAS QUE VIENEN DE PEDIDO YYYYDDMM TO - NOTATION AND FOR HOUR TOO
                #if pedido["AGRUPADOR"].strip() != "" :
                ped = {}
                ped["Agrupador"] = pedido["AGRUPADOR"]
                ped["PosAgrupador"] = pedido["POS_AGRUPADOR"]
                ped["Vbeln"] = pedido["VBELN"]
                ped["Vbtyp"] = pedido["VBTYP"]
                ped["Auart"] = pedido["AUART"]
                ped["Bsark"] = pedido["BSARK"]
                ped["Vehicle"] = pedido["VEHICLE"]
                ped["StartDate"] = pedido["PRO_BEGDA"]
                ped["StartTime"] = pedido["PRO_BEGTI"]
                ped["EndDate"] = pedido["PRO_ENDDA"]
                ped["EndTime"] = pedido["PRO_ENDTI"]
                ped["Duracion"] = pedido["DURACION"]
                ped["DuracionGrupo"] = pedido["DURACION_GRUPO"]
                #ped["Volumen"] = pedido["VOLUMEN"]
                
                ItPedidos.item.append(ped)
            
            
            response = self.client.service[0].ZprProgSapGrabar(IDatum, IWerks,ItCambioCamion, ItPedidos)
            return response
        except WebFault as e:
            #print(e)
            return e
    
    
    def login(self,user,pwd,url):
        try:
        
            session = Session()
            session.auth = HTTPBasicAuth(user, pwd)
            self.client = Client(url, transport=Transport(session=session))
        
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios.
            with self.client.settings(strict=False,raw_response=True):
                response = self.client.service.ZprProgSapAutoriza()
            string_response_xml = response.content#.decode("utf-8")
            json=self.to_dict(xmltodict.parse(string_response_xml))
            
            if "soapenv:Envelope" in json:
                if json["soapenv:Envelope"]["soapenv:Body"]["NS1:ZprProgSapAutorizaResponse"]["ERc"] == "1":
                    return {"retCode":401, "message":json["soapenv:Envelope"]["soapenv:Body"]["NS1:ZprProgSapAutorizaResponse"]["EMessage"]}
            try:
                #    (json["soap-env:Envelope"]["soap-env:Body"]["soap-env:Fault"]["faultstring"]["#text"])
                msg = json["soap-env:Envelope"]["soap-env:Body"]["soap-env:Fault"]["faultstring"]["#text"]
                if msg.strip() != "":
                    return {"retCode":401, "message":"El usuario no tiene acceso al programador."}
            except:
                pass
            
            base = json["soap-env:Envelope"]["soap-env:Body"]["n0:ZprProgSapAutorizaResponse"]
            autoriza = base["EtAutoriza"]  
            return {"retCode":200, "message":"ok", "data": self.build_response(autoriza)}
        except Exception as e:
            print(e)
            return False
    
    
