from .ms_dynamo_services import DynamoService
import datetime
#from datetime import datetime

class TruckStructureGenerator:

    def __init__(self): 
        pass

    #detalleType de la forma 24=6-5-4-8/9 por ejemplo o 24=6-5-4-8
    def get_capacity(self,detalleType):
        detalles = detalleType.split("=") #separo detalle de la suma
        
        compartimientos = detalles[1].strip().split("-")
        return compartimientos
    
    def get_countLiter(self,value):
        if value == "S":
            return True
        return False

    def format_date(self,yyyymmdd):
        #print("yyyymmdd")
        #print(yyyymmdd)
        ret = yyyymmdd[6:8] + "-" + yyyymmdd[4:6] + "-" +  yyyymmdd[:4]
        #print(ret)
        return ret
        #datetime_str = str(yyyymmdd[:4] + "-" + yyyymmdd[4:6] + "-" + yyyymmdd[6:8])
        #datetime_object = datetime.strptime(datetime_str, '%Y-%m-%d')
        #return datetime.strftime(datetime_object, '%d-%m-%Y')

    def soap_time_to_hhmmss(self,soap_time):
        return soap_time.replace(":","")
    
    def soap_date_to_yyyymmdd(self,soap_date):
        return soap_date.replace("-","")

    def get_time(self,time,unit):
        if unit == "h":
            return int(time[:2])
        if unit == "m":
            return int(time[2:4])
        if unit == "s":
            return int(time[4:6])

    def get_date_from_shift(self,shift):
        #print(shift)
        #print(shift["dayInit"])
        
        #year = int(shift["dayInit"].split("-")[0])
        #month = int(shift["dayInit"].split("-")[1])
        #day = int(shift["dayInit"].split("-")[2])
        
        year = int(shift["dayInit"].split("-")[2])
        month = int(shift["dayInit"].split("-")[1])
        day = int(shift["dayInit"].split("-")[0])
        
        
        hour=int(shift["hourInit"])
        minutes=int(shift["minutesInit"])
        seconds=int(shift["secondsInit"])
        #print(str(year) + "-" + str(month) + "-" + str(day) + "-" + str(hour)  + ":" + str(minutes) + ":" + str(seconds)) 
        #print(datetime)
        #return datetime.datetime(2008,2,18,hour=0,minute=0,second=0).timestamp() 
        return datetime.datetime(year,month,day,hour=hour,minute=minutes,second=seconds).timestamp() 
    
    def append_shift(self,new_shift,shift_array):
        shift_array.append(new_shift)
    
    
    def order_shifts(self,shift_array):
        self.quickSort(shift_array,0,len(shift_array)-1)
        
    def quickSort(self,arr,low,high):    
        if low < high:
            pi = self.partition(arr,low,high)
            self.quickSort(arr,low,pi-1)
            self.quickSort(arr,pi+1,high)
            
    def partition(self,arr,low,high):
        i = low -1
        pivot = arr[high]
        
        for j in range(low,high):
            if self.get_date_from_shift(arr[j]) <= self.get_date_from_shift(pivot):
                i = i + 1
                arr[i],arr[j] = arr[j],arr[i]
                
        arr[i+1],arr[high] = arr[high],arr[i+1]
        return i + 1
        
        
    
    #takes a yyyymmdd and return a mmdd_N
    def from_date_to_fecha_code(self,yyyymmdd):
        ret = yyyymmdd[0][4:6] + yyyymmdd[0][6:8] + "_N"
        return ret
    
        
    def generate_structure(self,input_search):
        #service_vehiculos_cab = DynamoService("Vehiculos_cab")
        soap_vehiculos_cab = input_search["soap_vehicles_cab"]
        #service_vehiculos_comp = DynamoService("Vehiculos_det")

        #input_search["fecha_plan"] viene en formato YYYYMMDD
        #Criterio 1 obtener camiones cuyos turnos partan en START_DATE y terminen en END_DATE para la fecha_plan dada. 
        #input = {"START_DATE": input_search["fecha_plan"],"END_DATE": input_search["fecha_plan"],"CODIGO_PLANTA": input_search["codigo_planta"]}
        
        #Criterio 2. Pasar fecha_plan a formato mmdd (que deberia cambiar pronto porque no  soporta agno)  y buscar por FECHA_PLAN=MMDD_N
        #input = {"FECHA_PLAN": self.from_date_to_fecha_code(input_search["fecha_plan"]),"CODIGO_PLANTA": input_search["codigo_planta"]}
        #input = {"FECHA_PLAN": {"item":self.from_date_to_fecha_code(input_search["fecha_plan"]),"condition":"eq"},"CODIGO_PLANTA": {"item":input_search["codigo_planta"],"condition":"eq"}}
        
        #vehiculos_cab = service_vehiculos_cab.scan_items(input)
        #vehiculos_comp = service_vehiculos_comp.scan_items(input)
        
        #print("vehiculos_cab (dyna) ======>")
        #print(vehiculos_cab)
        #print("soap_vehiculos_cab (soap) ======>")
        #print(soap_vehiculos_cab.keys())
        
        
        array_trucks = []
        trucks_by_id = {}
        
        #we use now values from sap format
        for vcab in soap_vehiculos_cab["item"]:
            if not vcab["Vehicle"] in trucks_by_id:
                trucks_by_id[vcab["Vehicle"]] = {}            
            current_vehicle =  trucks_by_id[vcab["Vehicle"]]
            current_vehicle["truckId"] = vcab["Vehicle"]
            current_vehicle["capacity"] = self.get_capacity(vcab["Detalle"])
            current_vehicle["countLiter"] = self.get_countLiter(vcab["CuentaLitros"])
            current_vehicle["fuelTypes"] = vcab["Grouptext"]
            current_vehicle["fuelTypeCode"] = vcab["Groupname"]
            if not "shifts" in current_vehicle:
                current_vehicle["shifts"] = []
            #current_vehicle["shifts"]
            new_shift={}        
            new_shift["name"]="Txxx" #hardcoded turn #determinar como poblarlo
            new_shift["dayInit"] = self.format_date(self.soap_date_to_yyyymmdd(vcab["StartDate"]))
            new_shift["hourInit"] = self.get_time(self.soap_time_to_hhmmss(vcab["StartTime"]),"h")
            new_shift["minutesInit"] = self.get_time(self.soap_time_to_hhmmss(vcab["StartTime"]),"m")
            new_shift["secondsInit"] = self.get_time(self.soap_time_to_hhmmss(vcab["StartTime"]),"s")         
            new_shift["dayEnd"] = self.format_date(self.soap_date_to_yyyymmdd(vcab["EndDate"]))
            new_shift["hourEnd"] = self.get_time(self.soap_time_to_hhmmss(vcab["EndTime"]),"h")
            new_shift["minutesEnd"] = self.get_time(self.soap_time_to_hhmmss(vcab["EndTime"]),"m")
            new_shift["secondsEnd"] = self.get_time(self.soap_time_to_hhmmss(vcab["EndTime"]),"s") 
            new_shift["active"] = True
            self.append_shift(new_shift,current_vehicle["shifts"])
            #self.append_shift(new_shift,current_vehicle["shifts"],vcab["VEHICLE"])
            self.order_shifts(current_vehicle["shifts"])
            #current_vehicle["shifts"] 
            #print(vcab["VEHICLE"])
        #print(trucks_by_id)
        for t in trucks_by_id:
            array_trucks.append(trucks_by_id[t])
            
        #print(array_trucks)
        return array_trucks    