from suds.client import Client
from suds import WebFault
from suds.transport.http import HttpAuthenticated
from suds.sudsobject import asdict
import logging

class soapService:

    def recursive_asdict(self,d):
        """Convert Suds object into serializable format."""
        out = {}
        for k, v in asdict(d).items():
            if hasattr(v, '__keylist__'):
                out[k] = self.recursive_asdict(v)
            elif isinstance(v, list):
                out[k] = []
                for item in v:
                    if hasattr(item, '__keylist__'):
                        out[k].append(self.recursive_asdict(item))
                    else:
                        out[k].append(item)
            else:
                out[k] = v
        return out

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        #logging.getLogger('suds.client').setLevel(logging.DEBUG)
        #logging.getLogger('suds.transport').setLevel(logging.DEBUG)
        # logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)

    def client_info(self):
        print(self.client)
        
    def createClient(self,user,pwd,urlConn):
        t = HttpAuthenticated(username=user, password=pwd)
        self.client = Client(url=urlConn,transport=t)
        
    def get_plan(self,fecha,plantCode): 
        #cliente tiene que tener la URL de get plan: zws_pr_obtiene_prog_sap
        #fecha yyyy-mm-dd
        #plantCode xxxx
        ItClienteFecha = self.client.factory.create('ItClienteFecha')

        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            response = self.client.service[0].ZbdpObtieneProgramacion(fecha,2,plantCode,ItClienteFecha)
            return self.recursive_asdict(response)
        except WebFault as e:
            print(e)
            return false
    
    def get_message(self,vbeln):
        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            response = self.client.service[0].ZBDP_EXTRAS_PEDIDOS(vbeln)
            return self.recursive_asdict(response)
        except WebFault as e:
            print(e)
            return false
    
    #entrega todos los pedidos como dados de baja, excepto los confirmados
    #se asocia a la funcionalidad LIMPIAR del programador
    def clear(self,fecha,plantCode): 
        #cliente tiene que tener la URL de get plan: zws_pr_obtiene_prog_sap
        #fecha yyyy-mm-dd
        #plantCode xxxx
        ItClienteFecha = self.client.factory.create('ItClienteFecha')

        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            parametro_dar_de_baja = 3
            response = self.client.service[0].ZbdpObtieneProgramacion(fecha,parametro_dar_de_baja,plantCode,ItClienteFecha)
            return self.recursive_asdict(response)
        except WebFault as e:
            print(e)
            return false
    
    def yyyymmddToDateSap(self,yyyymmdd):
        year = yyyymmdd[0:4]
        month = yyyymmdd[4:6]
        day = yyyymmdd[6:8]
        ret = year+"-"+month+"-"+day
        return ret
    
    def hhmmssToSap(self,hhmmss):
        #print("hhmmss")
        #print(hhmmss)
        #print(self.hhmmmmToHour(hhmmss))
        #print(int(self.hhmmmmToHour(hhmmss)))
        h =  self.hhmmmmToHour(hhmmss)
        m =  self.hhmmmmToMinute(hhmmss)
        s =  self.hhmmmmToSeconds(hhmmss)
        return h+":"+m+":"+s;
  
    def hhmmmmToHour(self,hhmmss):
        h = hhmmss[0:2]
        return h
  
    def hhmmmmToMinute(self,hhmmss):
        m = hhmmss[2:4]
        return m
  
    def hhmmmmToSeconds(self,hhmmss):
        s = hhmmss[4:6]
        return s
    
    def confirm(self,input):
        print("input")
        print(input)
        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            IDatum = input["fecha"]    #"2020-05-06"
            IWerks = input["codigo_planta"]    #"1208"
            
            #la lista de camiones. Esto no es tarea del programador, pero se podria hacer
            ItCambioCamion = self.client.factory.create('ZttdfAudCami')
            #la lista de pedidos
            ItPedidos = self.client.factory.create('ZprogTtConfirmarProg')
            
            item = self.client.factory.create('item')  
            
            for pedido in input["items"]:
                ped = {}
                ped["Agrupador"] = pedido["AGRUPADOR"]
                ped["PosAgrupador"] = pedido["POS_AGRUPADOR"]
                ped["Vbeln"] = pedido["VBELN"]
                ped["Vbtyp"] = pedido["VBTYP"]
                ped["Auart"] = pedido["AUART"]
                ped["Bsark"] = pedido["BSARK"]
                ped["Vehicle"] = pedido["VEHICLE"]
                
                ped["StartDate"] = self.yyyymmddToDateSap(pedido["PRO_BEGDA"])
                ped["StartTime"] = self.hhmmssToSap(pedido["PRO_BEGTI"])
                ped["EndDate"] = self.yyyymmddToDateSap(pedido["PRO_ENDDA"])
                ped["EndTime"] = self.hhmmssToSap(pedido["PRO_ENDTI"])
                ped["Duracion"] = self.hhmmssToSap(pedido["DURACION"])
                if pedido["DURACION_GRUPO"].strip() != "":
                    ped["DuracionGrupo"] = self.hhmmssToSap(pedido["DURACION_GRUPO"])
                
                #ped["Volumen"] = pedido["VOLUMEN"]
                
        
                
                ItPedidos.item.append(ped)
            
            #print(IDatum)
            #print(IWerks)
            #print(ItCambioCamion)
            print(ItPedidos)
            
            response = self.client.service[0].ZprProgSapConfirmar(IDatum, IWerks,ItCambioCamion, ItPedidos)
            print(response)
            return response
        except WebFault as e:
            #print(e)
            return e
    
    def deconfirm(self,input):
        CtPedidos = self.client.factory.create('ZprogTtDesconfirmarProg')
        IFechaFin = self.client.factory.create('date10')
        IFechaIni = self.client.factory.create('date10')
        IHoraFin = self.client.factory.create('time')
        IHoraIni = self.client.factory.create('time')
        print("DESCONOCNC:")
        print(input)
        try:
            #Se desconfirma un solo elemento, a diferencia de confirmar o guardar, que son varios.
            pedido = input["items"]
            
            ped = {}
            ped["Vbeln"] = pedido["VBELN"]
            ped["Vbtyp"] = pedido["VBTYP"]
            ped["Motivo"] = pedido["MOTIVO"]
            ped["Subrc"] = pedido["SUBRC"]
            ped["Message"] = pedido["MESSAGE"]
            #ped["DuracionCalcula"] = pedido[""]
            ped["Tipo"] = pedido["TIPO"]
            ped["Observacion"] = pedido["OBSERVACION"]
            
            CtPedidos.item.append(ped)
            
            #ZprProgSapDesconfirmar(ZprogTtDesconfirmarProg CtPedidos, date10 IFechaFin, date10 IFechaIni, time IHoraFin, time IHoraIni, )
            response = self.client.service[0].ZprProgSapDesconfirmar(CtPedidos, IFechaFin, IFechaIni, IHoraFin, IHoraIni )
            print(response)
            return response
            
        except WebFault as e:
            #print(e)
            return e
            
            
            
    def save_plan(self,input):
        #input = {"items": event["plan"],"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
        #ZprProgSapGrabar(ns0:date10 IDatum, ns0:char4 IWerks, ZttdfAudCami ItCambioCamion, ZprogTtConfirmarProg ItPedidos, )
        
        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            IDatum = input["fecha"]    #"2020-05-06"
            IWerks = input["codigo_planta"]    #"1208"
            
            #la lista de camiones. Esto no es tarea del programador, pero se podria hacer
            ItCambioCamion = self.client.factory.create('ZttdfAudCami')
            #la lista de pedidos
            ItPedidos = self.client.factory.create('ZprogTtConfirmarProg')
            
            #item = self.client.factory.create('item')  
            
            for pedido in input["items"]:
                #item = self.client.factory.create('item')    
                #FORMATEAR FECHAS QUE VIENEN DE PEDIDO YYYYDDMM TO - NOTATION AND FOR HOUR TOO
                #if pedido["AGRUPADOR"].strip() != "" :
                ped = {}
                ped["Agrupador"] = pedido["AGRUPADOR"]
                ped["PosAgrupador"] = pedido["POS_AGRUPADOR"]
                ped["Vbeln"] = pedido["VBELN"]
                ped["Vbtyp"] = pedido["VBTYP"]
                ped["Auart"] = pedido["AUART"]
                ped["Bsark"] = pedido["BSARK"]
                ped["Vehicle"] = pedido["VEHICLE"]
                ped["StartDate"] = pedido["PRO_BEGDA"]
                ped["StartTime"] = pedido["PRO_BEGTI"]
                ped["EndDate"] = pedido["PRO_ENDDA"]
                ped["EndTime"] = pedido["PRO_ENDTI"]
                ped["Duracion"] = pedido["DURACION"]
                ped["DuracionGrupo"] = pedido["DURACION_GRUPO"]
                #ped["Volumen"] = pedido["VOLUMEN"]
                
        
                
                ItPedidos.item.append(ped)
            
            #print(IDatum)
            #print(IWerks)
            #print(ItCambioCamion)
            #print(ItPedidos)
            
            response = self.client.service[0].ZprProgSapGrabar(IDatum, IWerks,ItCambioCamion, ItPedidos)
            print(response)
            return response
        except WebFault as e:
            #print(e)
            return e
    
"""    
    for i in range(0,len(response.EtPedidos.item)):
        print(type(response.EtPedidos.item[i]))
        print(response.EtPedidos.item[i].ZbdpEsPreprogramaPedidos.name)
"""
    
"""
EtCambioCamion 
EtCamionDedicado

EtPedidoDet
EtPedidos
EtVehicleDet
EtVehicles
EtVehiclesExtras

ItClienteFecha
"""

