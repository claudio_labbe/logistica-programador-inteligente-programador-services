from requests import Session
from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.
#from zeep import Client
#from zeep.transports import Transport
#from zeep import *
#from zeep_.zeep import Transport

import sys
sys.path.append('./zeep') 
from zeep import Client
from zeep import Transport
import time
from collections.abc import Iterable
import json
from collections import OrderedDict
from json import loads, dumps
import xmltodict

def build_response(item_json):   
        response = []
        #print(item_json["item"])
        
        for elem in item_json:
            print(elem)
            if elem == "item":
                return build_response(item_json[elem])
            
            else:
                hash = {}
                for item in elem:
                    if type(elem[item]).__name__ == "dict":
                        hash[item] = build_response(elem[item])
                    else:
                        hash[item] = elem[item]
                response.append(hash)    
        return response

def recursive_asdict(d):
        """Convert Suds object into serializable format."""
        out = {}
        for k, v in zeep.helpers.serialize_object(d).items(): #dict(zeep.helpers.serialize_object(d)).items():
            #print(k)
            #print(v)
            if isinstance(v, Iterable):
                print(k + " es iter")
                print(dict(v))
            if hasattr(v, '__keylist__'):
                dict(v)
                out[k] = recursive_asdict(dict(v))
            elif isinstance(v, list):
                out[k] = []
                for item in v:
                    if hasattr(item, '__keylist__'):
                        out[k].append(recursive_asdict(item))
                    else:
                        out[k].append(item)
            else:
                out[k] = v
        return out        

def to_dict(input_ordered_dict):
        return loads(dumps(input_ordered_dict))

#print(zeep.client.Client)
user = "aelgueda"
pwd = "Qprograma360"
urlConn="http://coqabroker1.copec.cl:7812/programador/zws_pr_prog_sap_confirmar?wsdl" 
#urlConn="http://coprbroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl" 

fecha = "2020-08-26"
plantCode = "1208"

session = Session()
session.auth = HTTPBasicAuth(user, pwd)


client = Client(urlConn, transport=Transport(session=session))

print(client)
print(client.get_type("ns0:ZprogTtConfirmarProg"))

#factory = client.type_factory('ns0')
#ItClienteFecha = factory.ZprogTtKunnrDatum()

"""
#print(ItClienteFecha)

#print(client.service.ZprProgSapAutoriza)

#service[0].ZprProgSapAutoriza

t1=time.time()
#with client.settings(raw_response=True):
#    response = client.service.ZbdpObtieneProgramacion(fecha,2,plantCode,ItClienteFecha)
with client.settings(strict=False,raw_response=True):
    response = client.service.ZbdpObtieneProgramacion(fecha,2,plantCode,ItClienteFecha)

t2=time.time()
print("zeep time : " + str(t2-t1))


#print(list(response))
#print(type(response.data))

string_response_xml = response.content#.decode("utf-8")
json=to_dict(xmltodict.parse(string_response_xml))
print(json)
#input_ordered_dict = zeep.helpers.serialize_object(response)
#jsn = loads(dumps(dict(input_ordered_dict)))
#print(jsn)
#for p in response:
#    print(type(p))
    
#build_response(response)
#print(response["EtPedidoDet"])

#js = zeep.helpers.serialize_object(response, target_cls=<class 'collections.OrderedDict'>)
#print(zeep.helpers.serialize_object)#
#res = recursive_asdict(response)
#print(type(res))
#print(res)
#obj = zeep.helpers.serialize_object(response)
#print(dict(obj))
#output_dict = json.loads(json.dumps(obj))
#output_dict = dict(obj)
#jsn = json.dumps(output_dict)
#print(type(jsn))
"""
"""
for d in response["EtPedidoDet"]['item']:
    print(d['Vbeln'])
"""