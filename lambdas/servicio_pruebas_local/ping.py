#!/usr/bin/env python
import socket
import subprocess
import sys
from datetime import datetime

# Clear the screen
subprocess.call('clear', shell=True)

# Ask for input
remoteServer    = "www.google.cl" #raw_input("Enter a remote host to scan: ")
remoteServerIP  = socket.gethostbyname(remoteServer)

remoteServerBroker    = "coqabroker1.copec.cl" #raw_input("Enter a remote host to scan: ")
remoteServerIPBroker = "10.1.60.41"#socket.gethostbyname(remoteServerBroker)


try:
	port = 80
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	result = sock.connect_ex((remoteServerIP, port))
	if result == 0:
		print("Host: "+remoteServerIP+ " Port: "+str(port)+": 	 Open")#.format(port)
	sock.close()
    
	port = 7812
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	result = sock.connect_ex((remoteServerIPBroker, port))
	if result == 0:
		print("Host: "+remoteServerIPBroker+ " Port: "+str(port)+": 	 Open")#.format(port)
	sock.close()
except KeyboardInterrupt:
    print("You pressed Ctrl+C")
    sys.exit()

except socket.gaierror:
    print("Hostname could not be resolved. Exiting")
    sys.exit()

except socket.error:
    print("Couldn't connect to server")
    sys.exit()

