import boto3
import time
import math 

client = boto3.client('athena')

list_origin = [1208,1216]

list_destination = [214425,20931]


condition_origin = "origin in ( "

i = 0
for origin in list_origin:
    if i == len(list_origin)-1:
        condition_origin = condition_origin+str(origin)+") "    
    else:
        condition_origin = condition_origin+str(origin)+", "
    i=i+1
    

condition_destination = "destination in ("
i = 0
for destination in list_destination:
    if i == len(list_destination)-1:
        condition_destination = condition_destination +str(destination)+") "    
    else:
        condition_destination = condition_destination +str(destination)+", "    
    i=i+1

condition = "where "  + condition_origin + " and " + condition_destination
query = 'SELECT origin, destination, travel_time_minutes FROM "repositorio_optimizador"."tiempos_viaje_consolidados" ' + condition 
print(query)

queryStart = client.start_query_execution(
    #QueryString = 'SELECT origin, destination, travel_time_minutes FROM time_estimation_travel_time LIMIT 10',
    #QueryString = 'SELECT origin, destination, travel_time_minutes FROM "repositorio_optimizador"."time_estimation_travel_time" where origin=\'1208\' and destination=\'0000040022\' ',
    QueryString = query,
    
    QueryExecutionContext = {
        'Database': 'repositorio_optimizador'
    },
    ResultConfiguration={
        'OutputLocation': 's3://athena-results-tests/loporto'
        
    }
)

#print(queryStart)

if queryStart["ResponseMetadata"]["HTTPStatusCode"] == 200:
    
    response = client.get_query_execution(
        QueryExecutionId=queryStart["QueryExecutionId"]
    )
    
   
    start_time = time.time()
    #t = 0
    while response["QueryExecution"]["Status"]["State"] == "QUEUED" or response["QueryExecution"]["Status"]["State"]=="RUNNING":
        response = client.get_query_execution(
            QueryExecutionId=queryStart["QueryExecutionId"]
        )
    end_time = time.time()
    print("--- %s seconds ---" % (end_time - start_time))
    print(response["QueryExecution"]["Status"])
    
    
    if response["QueryExecution"]["Status"]["State"]== "SUCCEEDED":
        #ahora puedo
        response = client.get_query_results(
            QueryExecutionId = queryStart["QueryExecutionId"]
        )
        
        #print(response)
        headers = {}
        ret = {}
        rows = response["ResultSet"]["Rows"]
        if len(rows)>1:
            i = 0
            for row in rows:
                print(row)
                if i == 0: 
                    #the header. Necesitamos obtener el indice de las columnas. No sabemos si siempre respetaran el mismo orden
                    col_idx = 0
                    for col in row["Data"]:
                        headers[col["VarCharValue"]]=col_idx
                        col_idx = col_idx + 1
                else:
                    src =  row["Data"][headers["origin"]]["VarCharValue"]
                    dest =  row["Data"][headers["destination"]]["VarCharValue"]
                    minutes = row["Data"][headers["travel_time_minutes"]]["VarCharValue"]
                    minutes_float = float(minutes)
                    #transform hh:mm:ss
            
                    h = math.floor(minutes_float/60)
                    m = math.floor(minutes_float%60)
                    minutes_remaining = (minutes_float%60) - m
                    s = math.ceil(60*minutes_remaining)
                    
                    hh = str(h) if h > 10 else "0"+str(h)
                    mm = str(m) if m > 10 else "0"+str(m)
                    ss = str(s) if s > 10 else "0"+str(s)
                
                    
                    duracion=str(hh)+":"+str(mm)+":"+str(ss)
                    
                    if src not in ret:
                        ret[src] = []
                    par = {"destination":dest, "minutes":minutes, "duracion":duracion}
                    ret[src].append(par)
                i = i+1
                
            print(ret)
            """
            ret = {}
            
            ret[rows[0]["Data"][0]["VarCharValue"]]=rows[1]["Data"][0]["VarCharValue"]
            ret[rows[0]["Data"][1]["VarCharValue"]]=rows[1]["Data"][1]["VarCharValue"]
            ret[rows[0]["Data"][2]["VarCharValue"]]=rows[1]["Data"][2]["VarCharValue"]
            
            minutes = float(ret["travel_time_minutes"])
            #transform hh:mm:ss
            
            h = math.floor(minutes/60)
            m = math.floor(minutes%60)
            minutes_remaining = (minutes%60) - m
            s = math.ceil(60*minutes_remaining)
            
            hhmmss=str(h)+":"+str(m)+":"+str(s)
            ret["duracion"]=hhmmss
            #return
            print(ret)
            """
        else:
            print("no result")
        
                
    else:
        print("there was an error")
#No output location provided. 
#An output location is required either through 
#the Workgroup result configuration setting or as an API input