import requests


url = "https://oz2gwj6e8g.execute-api.us-west-2.amazonaws.com/dev/sap-integ-get-plan"
auth_token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlF6QTFPVFU0TlVSQ01qRkROMEU0UlVOQ01UWXlOMFExUVRkQlJqWkZNekk1T0RnNFJERTBOZyJ9.eyJpc3MiOiJodHRwczovL2NvcGVjLXNzby1wcm9kdWN0aW9uLmF1dGgwLmNvbS8iLCJzdWIiOiJ6dVlUUUk2aW5rUFowY0I4cGFKdXVQdWxTY2RicjBIZEBjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly9vejJnd2o2ZThnLmV4ZWN1dGUtYXBpLnVzLXdlc3QtMi5hbWF6b25hd3MuY29tL2RldiIsImlhdCI6MTU5MjgzODczMCwiZXhwIjoxNTkyOTI1MTMwLCJhenAiOiJ6dVlUUUk2aW5rUFowY0I4cGFKdXVQdWxTY2RicjBIZCIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.kcdFhFYYykZaHzLGGlKYSk2F6JEUzsmSfEnU75CPXo01OIqssQ-3zuUaETBwCLrBlMyW_8MoD6oOyAl3krA3gzI4iU6a-luONuxX8VLCb8Wupo3cJICbKwGfFHsFzZYrnClBkUiYXwyncykyj2RgQchjVWl_RxYHsLG0LiSy5pAy--cJ6_wL8BzPH1gkFedOBq2g6q03xNUYgF2gUo0qo1S4943N7CuZR2-NISyMEm_1XF7QFaE2PRMwV4_WKsRcAevp58gY9qQIIiy1U0O8wz5ztjEge2yet-Yo3gqM2jsbLlSMaEwsIaLpvCwEjW1cAw8l-e-2gJZ5kVjfn0WQvA"

sap_usr="...."
sap_pwd="...."


hed = {'Authorization': 'Bearer ' + auth_token}

data = {"codigo_fecha": ["20200506"],"codigo_planta": ["1208"],"sap_user": sap_usr,"sap_pwd": sap_pwd}

response = requests.post(url, json=data, headers=hed)

print(response._content)