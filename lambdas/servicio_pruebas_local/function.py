from lib.truck_structure_generator import TruckStructureGenerator
from lib.orders_structure_generator import OrdersStructureGenerator
from lib.soap_service_obtiene_programacion import soapServiceObtieneProgramacion
import os


#event should contain a field id_vehiculo of  type string
def handler_get(event, context):
    tg=TruckStructureGenerator()
    og=OrdersStructureGenerator()
    
    input_order = {"fecha_pedido":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}
    input_truck = {"fecha_plan":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}
    
    #add sap
    fechaSoap = event["codigo_fecha"][0][0:4] + "-" + event["codigo_fecha"][0][4:6] + "-" + event["codigo_fecha"][0][6:8] 
    soap_service = soapServiceObtieneProgramacion()
    #NO HARDCODEAR USER PASS. PASARLO cOMO PARAMETRO
    user = os.environ["USER"]
    pwd = os.environ["PWD"]
    url = "http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl"
    soap_service.createClient(user,pwd,url)
    soap_response = soap_service.connect(fechaSoap,event["codigo_planta"]) #quizas es event["codigo_planta"][0] checkear
    #print(soap_response.keys())
    soap_vehicles_cab = soap_response["EtVehicles"]
    soap_pedidos = soap_response["EtPedidos"]
    soap_pedidos_det = soap_response["EtPedidoDet"]

    input_truck["soap_vehicles_cab"] = soap_vehicles_cab
    input_order["soap_pedidos"] = soap_pedidos
    input_order["soap_pedidos_det"] = soap_pedidos_det

    
    response_truck = tg.generate_structure(input_truck)
    response = og.generate_structure(input_order)
    response["truck"]=response_truck
    return response
