from flask import Flask, request, jsonify
from flask_cors import CORS,cross_origin

from lib.truck_structure_generator import TruckStructureGenerator
from lib.orders_structure_generator import OrdersStructureGenerator
from lib.orders_structure_store import storeOrdersDynamo
from lib.soap_service import soapService
import time
from lib.ms_dynamo_services import DynamoService

from lib.soap_service_zeep import soapServiceZeep


import os
import math
import json
import zlib
import base64

import time
from datetime import datetime
from datetime import timedelta

from requests import Session
from requests.auth import HTTPBasicAuth
from zeep import Client
from zeep.transports import Transport
import logging

#from lib.xmltodict import xmltodict
import xmltodict

from json import loads, dumps
#import time

import asyncio

#usr = "aelgueda"
#pwd = "Sofia2020"

#dyna_pedidos_usr =  "Pedido_dev_usr"
#dyna_pedidos_opt =  "Pedido_dev_opt"

# PROD 's3://copec-athena-results'
os.environ['ATHENA_OUTPUT_LOCATION'] = 's3://athena-results-tests/loporto'
os.environ['DYNA_PEDIDOS_USR'] = "Pedido_dev_usr"
os.environ['DYNA_PEDIDOS_OPT'] = "Pedidos_Optimizador"
os.environ['SAP_URL_XTRAS'] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl"

#os.environ['SAP_URL_ALL_MESSAGE'] = "http://coqabroker1.copec.cl:7812/programador/zws_bdp_obtiene_mensajes?wsdl"
os.environ['SAP_URL_ALL_MESSAGE'] = "http://10.1.60.42:7812/programador/zws_bdp_obtiene_mensajes?wsdl"


#dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
#dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"


#solo se usa pa debug en prod
target="pr" # qa o pr

app = Flask(__name__)
CORS(app)
#CADA FUNCION NO DUMMY SERA UN LAMBDA
@app.route('/')
def hello():
    return "AJAJA"

@app.route('/show_post', methods=['POST'])
def show_post():
    data = request.json
    #print(data)
    #print("/////////////")
    return jsonify(data)

@app.route('/longrequest', methods=['POST'])
def longrequest():
    print("Long request")
    event = request.json
    totalminutes = 10
    for i in range(1,totalminutes+1):    
        time.sleep(60)
        print("minuto : " + str(i))
    return event

@app.route('/login', methods=['POST'])
def login():
    #event usr, pwd
    event = request.json
    user = event["user"]
    pwd = event["pwd"]
    #sap_url = os.environ['SAP_URL']
    sap_url = "http://coqabroker1.copec.cl:7812/autorizacionSAP?wsdl"
    #sap_url = "http://coprbroker1.copec.cl:7812/autorizacionSAP?wsdl"
    
    #service_sap = soapService()
    #response = service_sap.login(user,pwd,sap_url)
    
    soapService_zeep = soapServiceZeep()
    response = soapService_zeep.login(user,pwd,sap_url)
    
    #antes de obtener el auth.. llamo a dyna 
    table_plant_access = "AccesoPlantas"
    #table_plant_access = os.environ['DYNA_ACCESO_PLANTAS']
    service_dynamo = DynamoService(table_plant_access)
    dynamo_response = service_dynamo.get_item({"rule":1})
    response["enablePlantRules"] = dynamo_response["row"]["enablePlantRules"]
    response["whitelist"] = dynamo_response["row"]["whitelist"]
    
    #add auth 0 later...
    response["bearer"] = "thetoken"
    """
    #only in deploy lambda
    data = {"client_id":os.environ["cid"],"client_secret":os.environ["cse"],"audience":os.environ["deploy_url_api_gw"],"grant_type":"client_credentials"}

    response_a0 = requests.post(os.environ["auth0_request_url_proxy"], json=data)

    res = response_a0._content.decode('utf8')
    y = json.loads(res)
    response["bearer"] = y['access_token']
    """
    return response

@app.route('/login_true', methods=['POST'])
def login_true():
    
    
    #add auth 0 later...
    response["bearer"] = "thetoken"
    return response
@app.route('/login_prod', methods=['POST'])
def login_prod():
    #event usr, pwd
    event = request.json
    user = event["user"]
    pwd = event["pwd"]
    print(event)
    service_sap = soapService()
    #*OJO ESTE APUNTA A PRODUCCION
    
    #response = service_sap.login(user,pwd,"http://co"+target+"broker1.copec.cl:7812/autorizacionSAP?wsdl")
    response = service_sap.login(user,pwd,"http://coprbroker1.copec.cl:7812/autorizacionSAP?wsdl")
    
    
    
    #add auth 0 later...
    response["bearer"] = "thetoken"
    return response

@app.route('/desconfirmar', methods=['POST'])
def desconfirmar():
    event = request.json
    
    sap_user = event["sap_user"]
    sap_pwd = event["sap_pwd"]
    #sap_url = os.environ['SAP_URL']
    sap_url = "http://coqabroker1.copec.cl:7812/programador/zws_pr_prog_sap_desconfirmar?wsdl"
    
    #sap_url_get_plan = os.environ['SAP_URL_GET_PLAN']
    sap_url_get_plan = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
    #'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
    
    dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
    dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"

    input = {"items": event["plan"]}
     
    try:
        #PASOS PARA GUARDAR
        #1 GUARDAMOS EN SAP
        # ..todo comandos
        service_sap = soapService()
        service_sap.createClient(sap_user,sap_pwd,sap_url)
        sap_response = service_sap.deconfirm(input)
        
        print(sap_response.item)
        ##para cada pedido en la respuesta
        
        vbeln_desconfirmados = []
        messageResp = ""
        responseCode = None
        okMessageWritten = False
        #Criterio para retornar codigo sap de varios pedidos desconfirmados. Si uno de ellos es 0, se retorna 0 (ok)
        for response_item in sap_response.item:
            print(response_item)
            if response_item.Subrc == 0:
                responseCode = response_item.Subrc
                vbeln_desconfirmados.append(response_item.Vbeln)
                if okMessageWritten != True:
                    messageResp += response_item.Message;
                    okMessageWritten= True
                
            
            else:
                if responseCode == None:
                    responseCode = response_item.Subrc
                messageResp += "  " + response_item.Message;
        
        #ahora, para cada pedido que se desconfirma (especificado dentro de objeto_pedido), veo si su vbeln esta dentro de los items desconfirmados y lo guardo en Dynamo
        
        for plan in event["plan"]:
            pedidos_desconfirmados=[]
            
            if type(plan["OBJETO_PEDIDO"]).__name__=="list":
                for obj in plan["OBJETO_PEDIDO"]:
                    if obj["VBELN"] in vbeln_desconfirmados:
                        print("D")
                        obj["ESTADO"] = "1"
                        obj["VEHICLE"] = " "
                        obj["AGRUPADOR"] = " "
                        pedidos_desconfirmados.append(obj)
            
            if type(plan["OBJETO_PEDIDO"]).__name__=="dict":
                obj = plan["OBJETO_PEDIDO"]
                if obj["VBELN"] in vbeln_desconfirmados:
                        print("D")
                        obj["ESTADO"] = "1"
                        obj["VEHICLE"] = " "
                        obj["AGRUPADOR"] = " "
                        pedidos_desconfirmados.append(obj)
                        
            input_dyna = {"items": pedidos_desconfirmados,"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
            #print("input dyna")
            #print(input_dyna)
            
            #dejarlo arriba, como env. 
            table_dynamo_pedidos_user = dyna_pedidos_usr
            service_dynamo = storeOrdersDynamo(table_dynamo_pedidos_user)
            
            current_time =  datetime.utcnow()

            if(event["timestamp"] != -1 and event["timestamp"] != None):
                timestamp = str(event["timestamp"])
                print("UTILIZANDO TIMESTAMP DESDE FRONT EN DESCONFIRMAR")
            else:
                print("UTILIZANDO TIMESTAMP GENERADO AHORA")
                timestamp = str(int(float(current_time.timestamp())))

            print(timestamp)

            ttl = int(float((current_time + timedelta(days=5)).timestamp()))       
            
            #timestamp = str(int(float(current_time.timestamp())))
            #ttl = -1

            dynamo_store_succes = service_dynamo.store(input_dyna,timestamp,ttl)
            """
            print("ejeutada orden")
            #Guardamos los cambios de desconfirmacion en dynamo
            tries_dyna = 3
            count_dyna = 0
            while (dynamo_store_succes != True) and count_dyna != tries_dyna :
                dynamo_store_succes = service_dynamo.store(input_dyna)
                count_dyna = count_dyna + 1
                time.sleep(0.5)
            """
            #if dynamo_store_succes != True:
            #    return {"retCode":"200","progStatus":"error","message":"Se desconfirman pedidos de la lista en SAP, pero no se puede desconfirmar en dynamo","lista_desconfirmados":ret_id_pedidos_desconfirmados, "ERc" :sap_response_code, "EMessage":sap_response_message}
        return {"retCode":"200","progStatus":"ok","message":"Se desconfirman pedidos de la lista en SAP y se desconfirma en Dynamo","lista_desconfirmados":vbeln_desconfirmados, "ERc" :responseCode, "EMessage":messageResp}
   
    except Exception as e:
        print("err lcals service deconcifrmar")
        #print(e)
        return {"retCode":"500", "message":e,"ERc" :responseCode, "EMessage":messageResp}       
    
    
   
@app.route('/confirmar', methods=['POST'])
def confirmar():
    #try:
    #event.plan debe recibir los pedidos a confirmar en el estado en el que vienen del programador. 
    #El servicio tendra que filtrar que guardar 
    event = request.json
    sap_user = event["sap_user"]
    sap_pwd = event["sap_pwd"]
    sap_url = "http://coqabroker1.copec.cl:7812/programador/zws_pr_prog_sap_confirmar?wsdl"


    dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
    dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"


    input = {"items": event["plan"],"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}

    #1 GUARDAMOS EN SAP
    #service_sap = soapService()
    #service_sap.createClient(sap_user,sap_pwd,sap_url)
    #sap_response = service_sap.confirm(input)

    #""" apenas pueda paso a zeep
    
    soapService_zeep = soapServiceZeep()
    soapService_zeep.createClient(sap_user,sap_pwd,sap_url)
    sap_response = soapService_zeep.confirm(input)
    #"""

    #cuando confirmamos y no hay problemam ERc es 0. El mensaje es "espere un momento hasta que se confirme"
    #vamos a consultar sap hasta que veamos que la lista de pedidos este confirmada
    print("ZAP RESPONSE")
    print(sap_response)
    
    if sap_response["ERc"] != '0':
        response = {}
        response["retCode"]="200"
        response["ERc"] = sap_response["ERc"]
        response["EMessage"] = sap_response["EMessage"]
        return response


    current_time =  datetime.utcnow()

    if(event["timestamp"] != -1 and event["timestamp"] != None):
        timestamp = str(event["timestamp"])
        print("UTILIZANDO TIMESTAMP DESDE FRONT CONFIRMAR")
    else:
        print("UTILIZANDO TIMESTAMP GENERADO AHORA")
        timestamp = str(int(float(current_time.timestamp())))

    print(timestamp)
    #timestamp = str(int(float(current_time.timestamp())))
    #ttl = -1
    ttl = int(float((current_time + timedelta(days=5)).timestamp()))

    if sap_response["ERc"] == '0':
        id_pedidos_confirmados =[]
        for  ped in event["plan"]:
            id_pedidos_confirmados.append(ped["VBELN"])
            
        
            
        pedidos_confirmados = event["plan"]
        
        #se guarda en dynamo asumiendo que el o los pedidos van a confirmarse en SAP sin problema. El agrupador, se lo dejamos como un agrupador generado por el programador
        for pc in pedidos_confirmados:
            pc["ESTADO"] = "3"
            #print("------Pedidos a confirmar-------")
            #print(pc["VBELN"])
            #print("------fin pedido--------")
            #pc["AGRUPADOR"] = agrupador_pedidos_by_id[pc["VBELN"]]
            
        
        input_dyna = {"items": pedidos_confirmados,"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
        #dejarlo arriba, como env. 
        table_dynamo_pedidos_user = dyna_pedidos_usr
        service_dynamo = storeOrdersDynamo(table_dynamo_pedidos_user)
        dynamo_store_succes = service_dynamo.store(input_dyna,timestamp,ttl)
        tries_dyna = 3
        count_dyna = 0
        while (dynamo_store_succes != True) and count_dyna != tries_dyna :
            dynamo_store_succes = service_dynamo.store(input_dyna,timestamp,ttl)
            count_dyna = count_dyna + 1
            time.sleep(0.5)
        
        if dynamo_store_succes != True:
            return {"retCode":"200","message":"NO se pudo guardar en dynamo","lista_confirmados":id_pedidos_confirmados, "ERc":sap_response["ERc"],"EMessage":sap_response["EMessage"]}
            
        return {"retCode":"200","message":"se pudo guardar en dynamo","lista_confirmados":id_pedidos_confirmados, "ERc":sap_response["ERc"],"EMessage":sap_response["EMessage"]}
    
    #except Exception as e:
    #    #print("exception")
    #    #print(e)
    #    return {"retCode":"500", "message":e}
    

       
        
@app.route('/save_plan', methods=['POST'])
def save():
    
    event = request.json
    
    user = event["sap_user"]
    pwd = event["sap_pwd"]
    #sap_url = os.environ['SAP_URL']
    sap_url = "http://coqabroker1.copec.cl:7812/programador/zpr_prog_sap_grabar?wsdl"
    
    dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
    dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
    
    
    input = {"items": event["plan"],"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
    table_dynamo_pedidos_user = dyna_pedidos_usr
    #poner la tabla en ambiente...
    
    #1 GUARDAMOS EN SAP
    service_sap = soapService()
    service_sap.createClient(user,pwd,sap_url)
    sap_response = service_sap.save_plan(input)
    
    sap_response_code = sap_response["ERc"]
    sap_response_message = sap_response["EMessage"]
    """
    table_dynamo_pedidos_user = "Pedido_dev_usr"
    service_dynamo = storeOrdersDynamo(table_dynamo_pedidos_user)
    dynamo_store_succes = service_dynamo.store(input)
    """
    #DynamoService(pedidos_opt_table)
    
    #2 Si guarda en sap, vemos store en dynamo

    #generando timestamp para creación y ttl
    current_time =  datetime.utcnow()
    if(event["timestamp"] != -1 and event["timestamp"] != None):
        timestamp = str(event["timestamp"])
        print("UTILIZANDO TIMESTAMP DESDE FRONT EN GUARDAR")
    else:
        print("UTILIZANDO TIMESTAMP GENERADO AHORA")
        timestamp = str(int(float(current_time.timestamp())))

    print(timestamp)

    ttl = int(float((current_time + timedelta(days=5)).timestamp()))

    #for pedido in input["items"]:
    #        print("------Pedidos a confirmar-------")
    #        print(pedido["VBELN"])
    #        print("------fin pedido--------")
    
    try:
        
        if sap_response.ERc != 0:
            response = {}
            response["retCode"] = "200";
            response["ERc"] = sap_response_code
            response["EMessage"] = sap_response_message
            return response;
    
        #si guarda ok en sap
        if sap_response.ERc == 0:
            service_dynamo = storeOrdersDynamo(table_dynamo_pedidos_user)
            dynamo_store_succes = service_dynamo.store(input,timestamp,ttl)
            #en caso que falle, intentamos un par de veces
            tries = 3
            count = 0
            while (dynamo_store_succes != True) and count != tries :
                dynamo_store_succes = service_dynamo.store(input,timestamp,ttl)
                count = count + 1
                time.sleep(0.5)
            
            #si no guarda en dynamo pero si en sap, igual retornamos ok
            if dynamo_store_succes != True:
                response = {"retCode":"200", "message":"Planificacion guardada en SAP, pero no pudo ser guardada en DynamoDB"}
                response["ERc"] = sap_response_code
                response["EMessage"] = sap_response_message
                return response
            
            response = {"retCode":"200","message":"Planificacion Guardada de manera exitosa"}
            response["ERc"] = sap_response_code
            response["EMessage"] = sap_response_message
            return response
        #si no se guardo en sap, no guardo en dyna y retorno error
        
        #return {"retCode":"500", "message":"no se pudo guardar plan en Sap.1 "}
    except Exception as e:
        #print(e)
        return {"retCode":"500", "message":"no se pudo guardar plan en Sap.2 " + e}
    
   
  
@app.route('/get_plan_local', methods=['POST'])
@cross_origin()
def get_plan_local():
    print("calling get plan local")
    #fo = open("json_1228_1_dic.json", encoding="utf8")
    fo = open("maipu-13-prod.json", encoding="utf8")
    
    r = fo.read()
    return r

@app.route('/get_empty_orders_plan_local', methods=['POST'])
@cross_origin()
def get_empty_orders_plan_local():
    fo = open("empty_plan.json")
    r = fo.read()
    return r

@app.route('/optimize_local', methods=['POST'])
def optimize_local():
    fo = open("nueva_propuesta.json")
    r = fo.read()
    return r


async def zeepTest(sap_user,sap_pwd,sap_url,sap_date, plant):
    print("comienza zeep")
    logging.basicConfig(level=logging.INFO)
    
    print("logging ok, comienza session")
    session = Session()
    session.auth = HTTPBasicAuth(sap_user, sap_pwd)
    client = Client(sap_url, transport= Transport(session=session))
    print("Cliente creado")
    
    try:
        factory = client.type_factory('ns0')
        ItClienteFecha = factory.ZprogTtKunnrDatum()
        print("intentando obtener data de SAP")
        #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios.
        with client.settings(strict=False,raw_response=True):
            #response = client.service.ZbdpObtieneProgramacion('2021-03-11',2,'1208',ItClienteFecha)
            response = client.service.ZbdpObtieneProgramacion(sap_date,2,plant,ItClienteFecha)
        print("Respuesta obtenida")
        #string_response_xml = response.content#.decode("utf-8")
        #response_json=self.to_dict(xmltodict.parse(string_response_xml))
        response_json = loads(dumps(xmltodict.parse(response.content)))
        
        base = response_json["soap-env:Envelope"]["soap-env:Body"]["n0:ZbdpObtieneProgramacionResponse"]
        
        res={}
        res["ERc"]=base["ERc"] if base["ERc"] != None else " "
        res["EMessage"]=base["EMessage"] if base["EMessage"] != None else " "
        res["EtVehicles"]=base["EtVehicles"] if base["EtVehicles"] != None else " "
        res["EtVehicleDet"]=base["EtVehicleDet"] if base["EtVehicleDet"] != None else " "
        res["EtPedidos"]=base["EtPedidos"] if base["EtPedidos"] != None else " "
        res["EtPedidoDet"]=base["EtPedidoDet"] if base["EtPedidoDet"] != None else " "
        res["EtCambioCamion"]=base["EtCambioCamion"] #if base["EtCambioCamion"] != None else " "
       
        print(res)
        return res
    except Exception as e:
        print("Err")
        print(e)
        return False
        
async def asyncCalls(sap_user,sap_pwd,sap_url,date,sap_date,plant):
    print("Entró en la función")
    service_pedidos_dynamo_optimizador = DynamoService(os.environ['DYNA_PEDIDOS_OPT'])
    print(os.environ['DYNA_PEDIDOS_OPT'])
    print("Parámetros queries")
    print(date)
    print(type(date))
    print(plant)
    print(type(plant))
    service_pedidos_dynamo_usuario = DynamoService(os.environ['DYNA_PEDIDOS_USR'])
    print("se setearon las tablas")
    loop = asyncio.get_running_loop()
    res = await asyncio.gather(
        zeepTest(sap_user,sap_pwd,sap_url,sap_date,plant),
        service_pedidos_dynamo_usuario.getPedidosQuery(int(plant),date),
        service_pedidos_dynamo_optimizador.getPedidosQuery(int(plant),date)
    )
    return res
    


@app.route('/get_plan', methods=['POST'])
def get_plan():
    #'http://co'+target+'broker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
    #'http://co'+target+'broker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
    try:
        start = time.time()

        #event = {"codigo_fecha":["20200506"], "codigo_planta":["1208"]}
        event = request.json

        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        #sap_url = os.environ['SAP_URL']
        #sap_url_xtras = os.environ['SAP_URL_XTRAS']
        #sap_url_messages = os.environ['SAP_URL_MESSAGES'] 
        
        sap_url = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
        sap_url_xtras = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
        sap_url_messages = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_info_prog_sap?wsdl'

        #sap_url = 'http://coprbroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
        #sap_url_xtras = 'http://coprbroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
        #sap_url_messages = 'http://coprbroker1.copec.cl:7812/programador/zws_pr_info_prog_sap?wsdl'


        dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
        dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
        
        tg = TruckStructureGenerator()
        og = OrdersStructureGenerator()

        input_order = {"fecha_pedido":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}
        input_truck = {"fecha_plan":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}

        fechaSoap = event["codigo_fecha"][0][0:4] + "-" + event["codigo_fecha"][0][4:6] + "-" + event["codigo_fecha"][0][6:8] 

        #para obtener los mensajes usados en combobox del programador al momento de dar de baja camiones o pedidos
        soap_service_messages = soapService()
        soap_service_messages.createClient(sap_user,sap_pwd,sap_url_messages)

        soap_response = {}
        query_usr_resp = {}
        query_opt_resp = {}
        
        available_parallel_plants = ['1202','1210']

        if(request["codigo_planta"][0] in available_parallel_plants):
            print("SE REALIZA EJECUCION EN PARALELO")
            sap_start = time.time()
            
            #soap_response = zeepTest(sap_user,sap_pwd,sap_url)
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            async_soap,async_dayna_usr,async_dyna_opt = loop.run_until_complete(asyncCalls(sap_user,sap_pwd,sap_url,request["codigo_fecha"][0],fechaSoap,request["codigo_planta"][0]))
            loop.close()
    
            print(async_soap)
            print(type(async_soap))
            print(async_dayna_usr)
            print(type(async_dayna_usr))
            print(async_dyna_opt)
            #service_pedidos_dynamo_usuario = DynamoService(os.environ['DYNA_PEDIDOS_USR'])
            #resp = service_pedidos_dynamo_usuario.getPedidosQuery2(1208,'20210311')
            #print(resp)
            
            sap_end = time.time() -sap_start
            sap_hours, sap_rem = divmod(sap_end, 3600)
            sap_minutes, sap_seconds = divmod(sap_rem, 60)
            print("Tiempo Async:")
            print("{:0>2}:{:05.2f}".format(int(sap_minutes),sap_seconds))
            
            soap_response = async_soap
            query_usr_resp = async_dayna_usr
            query_opt_resp = async_dyna_opt
        
        else:
            print("SE REALIZA EJECUCION SECUENCIAL")
            sap_start = time.time()
            #para obtener los mensajes usados en combobox del programador al momento de dar de baja camiones o pedidos
    
            soapService_zeep = soapServiceZeep()
            soapService_zeep.createClient(sap_user,sap_pwd,sap_url)
            
            #print("Parámetros sap:")
            #print(fechaSoap)
            #print(type(fechaSoap))
            #print(request["codigo_planta"])
            #print(type(request["codigo_planta"]))
    
            soap_response = soapService_zeep.get_plan(fechaSoap,request["codigo_planta"])
            
            sap_end = time.time() -sap_start
            sap_hours, sap_rem = divmod(sap_end, 3600)
            sap_minutes, sap_seconds = divmod(sap_rem, 60)
            print("Tiempo SAP:")
            print("{:0>2}:{:05.2f}".format(int(sap_minutes),sap_seconds))

        #print("CLIENTE CREADO")
        #soap_response_messages = soap_service_messages.get_combo_messages()
        #soapService_zeep = soapServiceZeep()
        #soapService_zeep.createClient(sap_user,sap_pwd,sap_url)

        #soap_response = soapService_zeep.get_plan(fechaSoap,event["codigo_planta"])

        sap_response_code = soap_response["ERc"]
        sap_response_message = soap_response["EMessage"]

        soap_vehicles_cab = soap_response["EtVehicles"]
        soap_vehicles_det = soap_response["EtVehicleDet"]
        soap_pedidos = soap_response["EtPedidos"]
        soap_pedidos_det = soap_response["EtPedidoDet"]

        soap_update_vehiculos = soap_response["EtCambioCamion"]

        #entregamos los vehiculos que tengan turno abajo
        vehicle_down = []
        if soap_update_vehiculos!=None:
            #print(type(soap_update_vehiculos["item"]).__name__)
            if type(soap_update_vehiculos["item"]).__name__ == "list":
                for it in soap_update_vehiculos["item"]:
                    vehicle_item = {}
                    #vehicle = it["Vehicle"]
                    #motivos = it["Motivos"]
                    vehicle_item["Vehicle"] = it["Vehicle"]
                    vehicle_item["Motivos"] = it["Motivos"]
                    vehicle_item["TurnoFechaDesde"] = it["TurnoFechaDesde"]
                    vehicle_item["TurnoHoraDesde"] = it["TurnoHoraDesde"]
                    vehicle_item["TurnoFechaHasta"] = it["TurnoFechaHasta"]
                    vehicle_item["TurnoHoraHasta"] = it["TurnoHoraHasta"]
                    
                    
                    #print(motivos)
                    tipo_motivo = list(filter(lambda x: x["MOTIVO"] == vehicle_item["Motivos"], soap_response_messages["ET_MOTIVOS"]["item"]))
                    #si largo es distinto que 1 hay algo malo.. No puede haber mas de un elemento por motivo
                    if len(tipo_motivo)==1:
                        if tipo_motivo[0]["TIPO"] == "B":
                            vehicle_down.append(vehicle_item)
                            
            elif type(soap_update_vehiculos["item"]).__name__ == "dict":
                it = soap_update_vehiculos["item"]
                vehicle_item = {}
                #vehicle = it["Vehicle"]
                #motivos = it["Motivos"]
                vehicle_item["Vehicle"] = it["Vehicle"]
                vehicle_item["Motivos"] = it["Motivos"]
                vehicle_item["TurnoFechaDesde"] = it["TurnoFechaDesde"]
                vehicle_item["TurnoHoraDesde"] = it["TurnoHoraDesde"]
                vehicle_item["TurnoFechaHasta"] = it["TurnoFechaHasta"]
                vehicle_item["TurnoHoraHasta"] = it["TurnoHoraHasta"]
                
                
                #print(motivos)
                tipo_motivo = list(filter(lambda x: x["MOTIVO"] == vehicle_item["Motivos"], soap_response_messages["ET_MOTIVOS"]["item"]))
                #si largo es distinto que 1 hay algo malo.. No puede haber mas de un elemento por motivo
                if len(tipo_motivo)==1:
                    if tipo_motivo[0]["TIPO"] == "B":
                        vehicle_down.append(vehicle_item)
                        
                        
        #probamos 
        if type(soap_vehicles_cab).__name__ == "str":
            soap_vehicles_cab = []
        if type(soap_vehicles_det).__name__ == "str":
            soap_vehicles_det = []
        if type(soap_pedidos).__name__ == "str":
            soap_pedidos = []
        if type(soap_pedidos_det).__name__ == "str":
            soap_pedidos_det = []


        input_truck["soap_vehicles_cab"] = soap_vehicles_cab
        input_truck["soap_vehicles_det"] = soap_vehicles_det
        input_order["soap_pedidos"] = soap_pedidos
        input_order["soap_pedidos_det"] = soap_pedidos_det
        input_order["merge_dynamo"] = True
        input_order["clean"] = False
        input_order["pedidos_opt_dynamo"] = dyna_pedidos_opt
        input_order["pedidos_usr_dynamo"] = dyna_pedidos_usr

        response_truck = tg.generate_structure(input_truck)
        response_truck_cab_optimizer = tg.generate_cab_structure_optimizer(input_truck)
        response_truck_det_optimizer = tg.generate_det_structure_optimizer(input_truck)
        #generamos un response en caso que no hayan pedidos. Esto para poder agregar un camion.
        response = {"pedidos": [], "duraciones":[]}
        if len(soap_pedidos) > 0:
            response = og.generate_structure(input_order)
        response["truck"] = response_truck

        response["truck_cab_optimizer"] = response_truck_cab_optimizer
        response["truck_det_optimizer"] = response_truck_det_optimizer

        response["ERc"] = sap_response_code
        response["EMessage"] = sap_response_message

        response["messages_combo"] = soap_response_messages;
        response["vehicle_down"] = vehicle_down

        #print(response)
        return response
    
    except Exception as e:
        print(e)
        return {"retCode":"500", "message": e}
    
    
@app.route('/clear', methods=['POST'])
def limpiar():
    
    try:
        tg = TruckStructureGenerator()
        og = OrdersStructureGenerator()

        #el event
        #event = {"codigo_fecha":["20200506"], "codigo_planta":["1208"]}
        event = request.json
        
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        #sap_url = os.environ['SAP_URL']
        sap_url = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
        #'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'

        dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
        dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
        
        
        input_order = {"fecha_pedido":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}
        input_truck = {"fecha_plan":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}

        fechaSoap = event["codigo_fecha"][0:4] + "-" + event["codigo_fecha"][4:6] + "-" + event["codigo_fecha"][6:8] 
        #soap_service = soapService()
        soapService_zeep = soapServiceZeep()
        #NO HARDCODEAR USER PASS. PASARLO cOMO PARAMETRO
        
        #soap_service.createClient(user,pwd,sap_url)
        soapService_zeep.createClient(sap_user,sap_pwd,sap_url)
        
        #soap_response = soap_service.get_plan("2020-05-06",input["codigo_planta"])
        #soap_response = soap_service.clear(fechaSoap,event["codigo_planta"])
        
        print(fechaSoap)
        print(event["codigo_fecha"])
        print("befho.")
        
        soap_response = soapService_zeep.clear(fechaSoap,[event["codigo_planta"]])
        soap_vehicles_cab = soap_response["EtVehicles"]
        soap_pedidos = soap_response["EtPedidos"]
        soap_pedidos_det = soap_response["EtPedidoDet"]
        
        sap_response_code = soap_response["ERc"]
        sap_response_message = soap_response["EMessage"]

        input_truck["soap_vehicles_cab"] = soap_vehicles_cab
        input_order["soap_pedidos"] = soap_pedidos
        input_order["soap_pedidos_det"] = soap_pedidos_det
        input_order["merge_dynamo"] = True
        input_order["clean"] = True
        input_order["pedidos_opt_dynamo"] = dyna_pedidos_opt
        input_order["pedidos_usr_dynamo"] = dyna_pedidos_usr
        response_truck = tg.generate_structure(input_truck)
        print("DDD")
        response = og.generate_structure(input_order)
        print("EEE")
        response["truck"] = response_truck   
        
        response["ERc"] = sap_response_code
        response["EMessage"] = sap_response_message
        print("DEBUG RESPONSE")
        print(response)
        return response
    
    except Exception as e:
        #print(e)
        return {"retCode":"500", "message": e}
     
@app.route('/register_shift_truck', methods=['POST'])
def shift_truck():
    try:
        #dyna_table = os.environ['DYNA_TABLE']
        dyna_table = "Registro_Turnos_Camiones"
        
        #recibir planta,fecha,camion,motivo,codigo motivo
        event = request.json
        planta = event["plant_code"]
        fecha = event["date"]
        camion = event["vehicle"]
        motivo = event["reason"]
        motivo_code = event["reason_code"]
        tipo_operacion = event["tipo_operacion"]
        comentarios = event["comentarios"]
        
        #tomar timestamp
        
        #generar input pa dynamo
        #se guardan las dadas de baja de los camiones
        input = {}
        #Key: planta-fecha-camion-timestamp
        timestamp = int(time.time())
        
        input["planta-fecha-camion-timestamp"] = planta + "-" + fecha + "-" + camion + "-" + str(timestamp)
        input["timestamp"] =  timestamp
        input["motivo"] = motivo
        input["motivo_code"] = motivo_code
        input["tipo_operacion"] = tipo_operacion
        input["comentarios"] = comentarios
        #escribir en dynamo
        table_dyna_shift_truck = dyna_table
        service_dynamo = DynamoService(table_dyna_shift_truck)
        dynamo_response = service_dynamo.put_item(input)
        
        #print(dynamo_response)
        if dynamo_response["code"] == "ok":
            return {"retCode":"200"}
        else:
            return {"retCode":"500", "message":dynamo_response["msg"]}
    
    except Exception as e:
        return {"retCode":"500", "message": e}


@app.route('/lista_pedidos', methods=['POST'])
def lista_pedidos():
    try:
        
        #event = {"codigo_fecha":["20200506"], "codigo_planta":["1208"]}
        
        event = request.json
        
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        #sap_url = os.environ['SAP_URL']
        #?wsdl
        sap_url = "http://coqabroker1.copec.cl:7812/programador/zws_bdp_lista_pedidos?wsdl"
        dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
        dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
        
        
        input = {"truck":event["truck"], "fecha_ini":event["fecha_ini"], "fecha_fin":event["fecha_fin"], "hora_ini":event["hora_ini"], "hora_fin":event["hora_fin"], "codigo_planta":event["codigo_planta"]  }
        
        soap_service = soapService()
        
        #NO HARDCODEAR USER PASS. PASARLO cOMO PARAMETRO
        soap_service.createClient(sap_user,sap_pwd,sap_url)
        response = soap_service.lista_pedidos(input)
        return {"retCode":"200", "message": "ok", "data":response}
    except Exception as e:
        print(e)
        return {"retCode":"500", "message": e}

@app.route('/confirmar_turno', methods=['POST'])
def confirmar_turno():
    print("LOCAL SERVICE.. CONFIRMAR TURNO LLAMADO")
    try:
        #event.plan debe recibir los pedidos a confirmar en el estado en el que vienen del programador. 
        #El servicio tendra que filtrar que guardar 
        event = request.json
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        #sap_url = os.environ['SAP_URL']
        sap_url = "http://coqabroker1.copec.cl:7812/programador/zws_pr_prog_sap_confirmar?wsdl"
        
        #sap_url_get_plan = os.environ['SAP_URL_GET_PLAN']
        #sap_url_get_plan = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
        
        dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
        dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
        
        input = {"items": event["plan"],"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
        #1 GUARDAMOS EN SAP
        service_sap = soapService()
        service_sap.createClient(sap_user,sap_pwd,sap_url)
        sap_response = service_sap.confirmCambioTurno(input)
        #cuando confirmamos y no hay problemam ERc es 0. El mensaje es "espere un momento hasta que se confirme"
        #vamos a consultar sap hasta que veamos que la lista de pedidos este confirmada
        response = {}
        response["retCode"]="200"
        if sap_response.ERc != 0:
            response["retCode"]="500"
            
        #if sap_response.ERc == 0:
            #TODO TERMINAR ESTO sap serv esta listo    
            #return {"retCode":"200","progStatus":"ok","message":"Se confirman pedidos de la lista en SAP y se confirma en Dynamo","lista_confirmados":id_pedidos_confirmados, "ERc":sap_response["ERc"],"EMessage":sap_response["EMessage"]}
         
        response["ERc"] = sap_response["ERc"]
        response["EMessage"] = sap_response["EMessage"]
        return response

        
    except Exception as e:
        print("exception")
        print(e)
        return {"retCode":"500", "message":e}

        
@app.route('/get_comentarios',methods=['POST'])
def get_comentarios():
    try:
        event = request.json
        print("VENT..")
        print(event)
        
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        
        sap_url_xtras = os.environ['SAP_URL_XTRAS']
        #lista_id_pedidos debe ser un arr con jsons, en el que venga VBELN y le agregamos el campo COMENTARIOS y lo retornamos al front
        lista_pedidos = event['lista_pedidos']
        ret_list = {}
        ###
        soap_service_comentarios_zeep = soapServiceZeep()
        soap_service_comentarios_zeep.createClient(sap_user,sap_pwd,sap_url_xtras)
        
        
        for p in lista_pedidos:
            messages = soap_service_comentarios_zeep.get_message(p["VBELN"])
            if(messages["T_TEXTOS"]==None or messages["T_TEXTOS"]==""):
                p["COMENTARIOS"] = ""
                ret_list[p["VBELN"]] = p["COMENTARIOS"]
            else:
                comentario = ""
                if type(messages["T_TEXTOS"]["item"]).__name__ == "dict":
                    textLine = messages["T_TEXTOS"]["item"]
                    separador = " "
                    if textLine["TDFORMAT"] == "*" and textLine["TDFORMAT"] != None:
                        separador = "\n"
                    nextToken=""
                    if textLine["TDLINE"] != None:
                            nextToken = textLine["TDLINE"]
                    comentario = comentario + separador + nextToken
                    
                elif type(messages["T_TEXTOS"]["item"]).__name__ == "list":
                    for textLine in messages["T_TEXTOS"]["item"]:
                        separador = " "
                        if textLine["TDFORMAT"] == "*" and textLine["TDFORMAT"] != None:
                            separador = "\n"
                        nextToken=""
                        if textLine["TDLINE"] != None:
                            nextToken = textLine["TDLINE"]
                        comentario = comentario + separador + nextToken
                p["COMENTARIOS"] = comentario
                ret_list[p["VBELN"]] = p["COMENTARIOS"]
                
        #print(lista_pedidos)
        return {"retCode":"200", "data":ret_list}    
    except Exception as e:    
        return {"retCode":"500", "message": e}
        
        
@app.route('/sap-integ-get-single-message',methods=['POST'])
def get_single_message():
    try:
        event = request.json
        
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        
        sap_url_xtras = os.environ['SAP_URL_XTRAS']
        vbeln = event['vbeln']
        ret_list = {}
        ###
        soap_service_comentarios_zeep = soapServiceZeep()
        soap_service_comentarios_zeep.createClient(sap_user,sap_pwd,sap_url_xtras)
        
        comentarios = ""
        messages = soap_service_comentarios_zeep.get_message(vbeln)
        if(messages["T_TEXTOS"]==None or messages["T_TEXTOS"]==""):
            comentarios = ""
            ret_list[vbeln] = "ada d"#comentarios
        else:
            comentario = ""
            if type(messages["T_TEXTOS"]["item"]).__name__ == "dict":
                textLine = messages["T_TEXTOS"]["item"]
                separador = " "
                if textLine["TDFORMAT"] == "*" and textLine["TDFORMAT"] != None:
                    separador = "\n"
                nextToken=""
                if textLine["TDLINE"] != None:
                        nextToken = textLine["TDLINE"]
                comentario = comentario + separador + nextToken
                
            elif type(messages["T_TEXTOS"]["item"]).__name__ == "list":
                for textLine in messages["T_TEXTOS"]["item"]:
                    separador = " "
                    if textLine["TDFORMAT"] == "*" and textLine["TDFORMAT"] != None:
                        separador = "\n"
                    nextToken=""
                    if textLine["TDLINE"] != None:
                        nextToken = textLine["TDLINE"]
                    comentario = comentario + separador + nextToken
            comentarios = comentario
            ret_list[vbeln] = comentarios
                
        #print(lista_pedidos)
        return {"retCode":"200", "data":ret_list}    
    except Exception as e:    
        return {"retCode":"500", "message": e}
        
@app.route('/sap-integ-get-all-message',methods=['POST'])
def get_all_message():
    #Necesito ver la 
    try:
        event = request.json
        
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        
        sap_url_all_mesasges = os.environ['SAP_URL_ALL_MESSAGE']
        
        date = event['date']#yyyy-mm-dd
        plant_code = event['plant_code']
        
        ret_list = []
        ###
        soap_service_comentarios_zeep = soapServiceZeep()
        soap_service_comentarios_zeep.createClient(sap_user,sap_pwd,sap_url_all_mesasges)
        
        comentarios = ""
        messages = soap_service_comentarios_zeep.get_all_message(plant_code,date)
        #print("messages")
        #print(messages)
        #print("EOF")
        
        if(messages["TEXTOS"]==None or messages["TEXTOS"]==""):
            comentarios = ""
           
        else:
            comentario = ""
            if type(messages["TEXTOS"]["item"]).__name__ == "dict":
                ret_list.append(messages["TEXTOS"]["item"])
                
            elif type(messages["TEXTOS"]["item"]).__name__ == "list":
                ret_list = messages["TEXTOS"]["item"]
            
        """
        else:
            comentario = ""
            if type(messages["T_TEXTOS"]["item"]).__name__ == "dict":
                textLine = messages["T_TEXTOS"]["item"]
                separador = " "
                if textLine["TDFORMAT"] == "*" and textLine["TDFORMAT"] != None:
                    separador = "\n"
                nextToken=""
                if textLine["TDLINE"] != None:
                        nextToken = textLine["TDLINE"]
                comentario = comentario + separador + nextToken
                
            elif type(messages["T_TEXTOS"]["item"]).__name__ == "list":
                for textLine in messages["T_TEXTOS"]["item"]:
                    separador = " "
                    if textLine["TDFORMAT"] == "*" and textLine["TDFORMAT"] != None:
                        separador = "\n"
                    nextToken=""
                    if textLine["TDLINE"] != None:
                        nextToken = textLine["TDLINE"]
                    comentario = comentario + separador + nextToken
            comentarios = comentario
            ret_list[vbeln] = comentarios
        """                
        #print(lista_pedidos)
        return {"retCode":"200", "data":ret_list}    
    except Exception as e:    
        return {"retCode":"500", "message": e}
    
if __name__ == '__main__':
    app.run()