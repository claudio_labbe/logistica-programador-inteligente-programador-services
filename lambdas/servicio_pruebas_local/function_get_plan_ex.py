def get_plan():
    tg = TruckStructureGenerator()
    og = OrdersStructureGenerator()

    #el event
    #event = {"codigo_fecha":["20200506"], "codigo_planta":["1208"]}
    event = request.json
    
    user = event["sap_user"]
    pwd = event["sap_pwd"]
    
    
    input_order = {"fecha_pedido":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}
    input_truck = {"fecha_plan":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}

    fechaSoap = event["codigo_fecha"][0][0:4] + "-" + event["codigo_fecha"][0][4:6] + "-" + event["codigo_fecha"][0][6:8] 
    soap_service = soapService()
    #NO HARDCODEAR USER PASS. PASARLO cOMO PARAMETRO
    
    soap_service.createClient(usr,pwd,'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl')
    
    soap_service_comentarios = soapService()
    soap_service_comentarios.createClient(usr,pwd,'http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl')
    #obtener messages con get_message para cada vbeln y pasarlo 
    
    #soap_response = soap_service.get_plan("2020-05-06",input["codigo_planta"])
    soap_response = soap_service.get_plan(fechaSoap,event["codigo_planta"])
    soap_vehicles_cab = soap_response["EtVehicles"]
    soap_vehicles_det = soap_response["EtVehicleDet"]
    soap_pedidos = soap_response["EtPedidos"]
    soap_pedidos_det = soap_response["EtPedidoDet"]

    """
    print("soap_pedidos")
    print(len(soap_pedidos))
    print(soap_vehicles_cab)
    print(soap_vehicles_det)
    print(soap_pedidos)
    print(soap_pedidos_det)
    print("--------------")    
    """
    if len(soap_pedidos) > 0:
        for p in soap_pedidos["item"]:
            print(p["Vbeln"])
            #Este vbeln tiene comentarios para probar 0404358046
            messages = soap_service_comentarios.get_message(p["Vbeln"])
            if(messages["T_TEXTOS"]==""):
                p["COMENTARIOS"] = ""
            else:
                #print(messages["T_TEXTOS"])
                comentario = ""
                for textLine in messages["T_TEXTOS"]["item"]:
                    separador = " "
                    if textLine["TDFORMAT"] == "*":
                        separador = "\n"
                    comentario = comentario + separador + textLine["TDLINE"]
                #print(comentario)
                p["COMENTARIOS"] = comentario
                
        
    
    input_truck["soap_vehicles_cab"] = soap_vehicles_cab
    input_truck["soap_vehicles_det"] = soap_vehicles_det
    input_order["soap_pedidos"] = soap_pedidos
    input_order["soap_pedidos_det"] = soap_pedidos_det
    input_order["merge_dynamo"] = True
    input_order["clean"] = False
    input_order["pedidos_opt_dynamo"] = dyna_pedidos_opt
    input_order["pedidos_usr_dynamo"] = dyna_pedidos_usr
    
    response_truck = tg.generate_structure(input_truck)
    response_truck_cab_optimizer = tg.generate_cab_structure_optimizer(input_truck)
    response_truck_det_optimizer = tg.generate_det_structure_optimizer(input_truck)
    
    #generamos un response en caso que no hayan pedidos. Esto para poder agregar un camion.
    response = {"pedidos": [], "duraciones":[]}
    if len(soap_pedidos) > 0:
        response = og.generate_structure(input_order)
    
    print("AACAAAAAAAAAAAAAAAAAAAAAAAAA")
    print(response_truck)
    
    response["truck"] = response_truck
    
    response["truck_cab_optimizer"] = response_truck_cab_optimizer
    response["truck_det_optimizer"] = response_truck_det_optimizer
    return response