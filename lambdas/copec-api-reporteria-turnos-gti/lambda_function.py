﻿import boto3
import csv
import io
from datetime import datetime, timedelta
import requests 
import json
import os

def get_api_data(api_url, method, body):
    
    url = api_url + method
    response = requests.post(url, json=body)
    
    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None
        

output_bucket = os.environ['OUTPUT_BUCKET']
output_folder = os.environ['OUTPUT_FOLDER']


s3 = boto3.resource('s3')

api_url = 'https://copecgti.owlchile.cl/Api/deloitte/v1'
method = '/getAvailability?date=%s'

def lambda_handler(event, context):
    """ Obtener data desde API y almacenar en bucket S3.
    
    Iterar sobre los registros de turnos, retorna la data de interés.
    :param event:  
    :param context:
    """       
    #required_date = event['required_date']   # '20200701'
    required_date = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d').replace("-","")
    year = required_date[:4]
    month = required_date[4:6]

    FILENAME =  'turnos' + required_date + '.csv'
    output_file = io.StringIO()    
    writer = csv.writer(output_file, delimiter='|')

    # Escribir encabezados
    headers = ["turnos_esperados_fecha_consulta|turnos_efectivos|id_equipo|planta|nombre_transportista|rut_transportista|porcentaje_disponibilidad|turnos_esperados_mes_actual|fecha"]
    writer.writerow(headers)  

    # Respuestas API GTI
    getAvailability = get_api_data(api_url = api_url, method = method % (required_date), body = {})
    

    # Iterar sobre los registros de turnos:
    if getAvailability is not None:
        for item in getAvailability['eventos']:
                writer.writerow(list(item.values())+ [required_date])       
            
    # Subir archivo temporal a S3
    object = s3.Object(output_bucket, output_folder +"/year="+year +"/month="+month+"/"+FILENAME)    
    object.put(Body=output_file.getvalue())