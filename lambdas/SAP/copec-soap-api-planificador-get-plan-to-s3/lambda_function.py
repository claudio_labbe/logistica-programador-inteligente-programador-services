import csv
import os
from lib.soap_service import soapService
import boto3
from datetime import datetime
from datetime import timedelta
import pytz
import io

# dest_bucket = os.environ['dest_bucket']
# dest_path = os.environ['dest_path']
# sap_url = os.environ['SAP_URL']


def lambda_handler(event, context):
    s3 = boto3.resource('s3')

    dest_bucket = os.environ['dest_bucket']
    dest_path = os.environ['dest_path']

    sap_url = os.environ['SAP_URL']

    santiago = pytz.timezone('America/Santiago')
    if event["night_plan"] == "_A":
        santiago_dt = pytz.utc.localize(
            datetime.utcnow() + timedelta(days=-1), is_dst=None).astimezone(santiago)
    else:
        santiago_dt = pytz.utc.localize(
            datetime.utcnow() + timedelta(days=1), is_dst=None).astimezone(santiago)
    santiago_fmt = santiago_dt.strftime('%Y%m%d')

    event["codigo_fecha"] = [santiago_fmt]

    # print(event["codigo_fecha"])

    user = event["sap_user"]
    pwd = event["sap_pwd"]

    fechaSoap = event["codigo_fecha"][0][0:4] + "-" + event["codigo_fecha"][0][4:6] + "-" + event["codigo_fecha"][0][
                                                                                            6:8]
    soap_service = soapService()

    soap_service.createClient(user, pwd, sap_url)

    csv_columns = {
        'EtPedidos': [
            'Agrupador',
            'Auart',
            'BezeiAuart',
            'BezeiDelco',
            'Bsark',
            'CamionDedicado',
            'Cmgst',
            'CuentaLitros',
            'Delco',
            'Detalle',
            'Duracion',
            'DuracionCalcula',
            'DuracionClientes',
            'DuracionGrupo',
            'DuracionTramo1',
            'EntBegda',
            'EntBegti',
            'EntEndda',
            'EntEndti',
            'Especial',
            'Estado',
            'Groupname',
            'Grouptext',
            'Kunag',
            'Name1Kunag',
            'PosAgrupador',
            'ProBegda',
            'ProBegti',
            'ProEndda',
            'ProEndti',
            'UtcError',
            'Vbeln',
            'Vbtyp',
            'Vdatu',
            'Vehicle',
            'Vtext',
            'VtextBsark',
            'Zone1'],
        'EtPedidoDet': [
            'Kwmeng',
            'Maktx',
            'Matnr',
            'Meins',
            'Vbeln'],
        'EtVehicles': [
            'CantCompar',
            'CombinaCompar',
            'CuentaLitros',
            'Detalle',
            'EndDate',
            'EndTime',
            'Especial',
            'Groupname',
            'Grouptext',
            'StartDate',
            'StartTime',
            'Vehicle'],
        'EtVehicleDet': [
            'CantCompar',
            'CmpMaxvol',
            'CmpMinvol',
            'ComNumberDet',
            'Groupname',
            'Vehicle'],
        'ComNumberDet': [
            'CmpMaxvol',
            'CmpMaxvolEnt',
            'CmpMaxvolEntKg',
            'CmpMaxvolKg',
            'CmpMinvol',
            'CmpMinvolEnt',
            'CmpMinvolEntKg',
            'CmpMinvolKg',
            'ComNumber',
            'TuNumber']
    }

    # print(event["codigo_planta"])
    # print(type(event["codigo_planta"]))

    for codigo_planta in event["codigo_planta"]:
        print('procesando planta %s' % codigo_planta)

        csv_files = {'EtPedidos':
                         'Pedidos_Cab_%s_%s%s.txt' % (codigo_planta,
                                                      event["codigo_fecha"][0],
                                                      event["night_plan"]),
                     'EtPedidoDet':
                         'Pedidos_Det_%s_%s%s.txt' % (codigo_planta,
                                                      event["codigo_fecha"][0],
                                                      event["night_plan"]),
                     'EtVehicles':
                         'Vehiculos_Cab_%s_%s%s.txt' % (codigo_planta,
                                                        event["codigo_fecha"][0],
                                                        event["night_plan"]),
                     'EtVehicleDet':
                         'Vehiculos_Det_%s_%s%s.txt' % (codigo_planta,
                                                        event["codigo_fecha"][0],
                                                        event["night_plan"]),
                     'ComNumberDet':
                         'Vehiculos_Det_Comp_%s_%s%s.txt' % (codigo_planta,
                                                             event["codigo_fecha"][0],
                                                             event["night_plan"])
                     }

        soap_response = soap_service.get_plan(fechaSoap, [codigo_planta])

        for service_name in csv_columns.keys():
            try:
                output = io.StringIO()
                writer = csv.DictWriter(output, fieldnames=csv_columns[service_name], delimiter='|')
                writer.writeheader()
                if service_name != 'ComNumberDet':
                    for item in soap_response[service_name]['item']:
                        writer.writerow(item)

                elif service_name == 'ComNumberDet':
                    for item in soap_response['EtVehicleDet']['item']:
                        for item_comp in item[service_name]['item']:
                            writer.writerow(item_comp)

                # print(dest_path % ('_'.join(csv_files[service_name].split('_')[:2]).lower(),
                #                    '_'.join(csv_files[service_name].split('_')[:2]).lower() +
                #                    event["night_plan"].lower(),
                #                    codigo_planta,
                #                    event["codigo_fecha"][0][0:4],
                #                    event["codigo_fecha"][0][4:6],
                #                    csv_files[service_name]))

                if service_name != 'ComNumberDet':
                    filename = '_'.join(csv_files[service_name].split('_')[:2]).lower()
                else:
                    filename = '_'.join(csv_files[service_name].split('_')[:3]).lower()

                object = s3.Object(dest_bucket, dest_path
                                   % (filename,
                                      filename + (event["night_plan"].lower()),
                                      codigo_planta,
                                      event["codigo_fecha"][0][0:4],
                                      event["codigo_fecha"][0][4:6],
                                      csv_files[service_name]))

                object.put(Body=output.getvalue())

            except Exception as e:
                # return {"message": e}
                print(e)
                pass

    return {"message": "proceso finalizo"}