import boto3
import sys
import json
from boto3.dynamodb.conditions import And, Attr
from functools import reduce
from operator import and_, or_

class DynamoService:

    # {items:[{...},{...}..]
    def put_items(self, items):
        with self.table.batch_writer() as batch:
            idx = 1
            for it in items["items"]:
                batch.put_item(Item=it)
                #print("up " + str(idx) + " elements...")
                #print(it)
                idx = idx + 1
        return "put items finished"

    # Method to retrieve data from vehiculos_cab table
    def scan_items(self, request_items):
        query_params = self.build_query_params(request_items)
        #response = self.table.scan(self.table, **query_params)
        ##print("QUERPARAMS")
        ##print(query_params)
        response = self.table.scan(**query_params)
        
        items = []
        items = items + response["Items"]
        
        while "LastEvaluatedKey" in response:
            ##print("ojo con el scan cuando deployee a lambda..")
            #response = self.table.scan(self.table,**query_params,ExclusiveStartKey=response["LastEvaluatedKey"])
            response = self.table.scan(**query_params,ExclusiveStartKey=response["LastEvaluatedKey"])
            items = items + response["Items"]    
        
        return {"Items":items}

    # vechiculo es un json que debe tener un atributo id_vehiculo de tipo string
    def put_item(self, item):
        response = ""
        try:
            response = self.table.put_item(
                Item=item
            )
            # si no falla, se inserta
            response = {'code': 'ok'}
        except Exception as e:
            response = {'code': 'error','msg':str(e)}
        return response

    # jsonRequest debe traer el json con los ids a buscar
    def get_item(self, jsonRequest):
        response = ""
        try:
            response = self.table.get_item(
                Key=jsonRequest
            )

            if ('Item' in response):
                item = response['Item']
                return {'row': item, 'msg': 'ok'}
            else:
                return {'row': {}, 'msg': 'no item to show'}
                #print('no item to show')
        except:
            response = {'code': 'error'}

        return {'row': {}, 'msg': 'no item to show'}

    def __init__(self, tableName,runlocal=False):
        #print("calling constructor dynamo for table: " + tableName)
        if not runlocal :
            dynamodb = boto3.resource('dynamodb')
        else:
            dynamodb = boto3.resource('dynamodb', region_name='local', aws_access_key_id='local', aws_secret_access_key='local', endpoint_url="http://localhost:8000")
        self.table = dynamodb.Table(tableName)

    def build_query_params(self, request_items: dict):
        
        #print("build_query_params")
        #print("request_items")
        #print(request_items)
        """
        it = request_items['VBELN']['item']
        it = it[0:10]
        request_items['VBELN']['item'] = it
        #print("after...")
        #print(it)
        #request_items = request_items[0:30]
        #VER COMO SECCIONAR REQUEST dYNA max 131...
        ##print("VER COMO SECCIONAR REQUEST dYNA max 131...")
        """
        query_params = {}
        if len(request_items) > 0:
            query_params["FilterExpression"] = self.add_expressions(request_items)
        return query_params


    def add_expressions(self, request_items: dict):
        #{'ENT_BEGDA': ['20200317'], 'CODIGO_PLANTA': ['1216'], 'OPTIMIZED': ['1', '0']}
        #{'FECHA_PLAN': '0317_N', 'CODIGO_PLANTA': ['1216']}
        
        #{'ENT_BEGDA': {'item':['20200317'],'condition':'eq'}, 'CODIGO_PLANTA': {'item':['1216'],'condition':'eq'}, 'OPTIMIZED': {'item':['1', '0']}
        #{'FECHA_PLAN': {'item':'0317_N','condition':'eq'}, 'CODIGO_PLANTA': {'item':['1216'],'condition':'eq'}}
        if request_items:
            conditions = []
            #dynamo suppports till 100 element in "is in" operation
            #if, ther is more elements, we have to split the query
            has_to_split_condition = False
            for key, value in request_items.items():
                if isinstance(value["item"], str):
                    if value["condition"]=="eq":
                        conditions.append(Attr(key).eq(value["item"]))
                    if value["condition"]=="lte":
                        conditions.append(Attr(key).lte(value["item"]))
                    if value["condition"]=="gte":
                        conditions.append(Attr(key).gte(value["item"]))
                    if value["condition"]=="contains":
                        conditions.append(Attr(key).contains(value["item"]))
                    else:
                        conditions.append(Attr(key).eq(value["item"]))
                        
                if isinstance(value["item"], list):
                    if len(value["item"]) > 100:
                        has_to_split_condition = True
                        i = 0
                        step = 50
                        while i < len(value["item"]):
                            section = value["item"][i:i+step]
                            conditions.append(Attr(key).is_in([v for v in section]))
                            i+=step;
                    
                    else:
                        conditions.append(Attr(key).is_in([v for v in value["item"]]))
                    
            if has_to_split_condition:
                return reduce(or_, conditions)
            
            return reduce(and_, conditions)


def main():
    flotaService = FlotaService()
    # flotaService.put_item({'id_vehiculo':'2222'})
    # flotaService.get_item('2a222')


if __name__ == "__main__":
    main()
