#zeep
from requests import Session
from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.
from zeep import Client
from zeep.transports import Transport
import logging
import xmltodict
#from xmltodict import xmltodict
from json import loads, dumps
import time
#dynamo
from lib.ms_dynamo_services import DynamoService

#test
import unittest
import boto3

#para generar variables de ambiente
import os
"""


import json
from boto3.dynamodb.conditions import And, Attr
from functools import reduce
from operator import and_, or_
"""
import sys
#sys.path.append("C:\projects\COPEC\planificador\programador_services\lambdas\SAP\lambda_foo")
sys.path.append("C:\projects\COPEC\planificador\programador_services\lambdas\SAP\copec_api_planificador_get_plan_sap")
#from lambdafoo import handler_get
from function import handler_get



user="aelgueda"
pwd ="Qprograma360"

#lista_pedidos_qa = ["0404363702","0404363703","0404363704","0404363705","0404363706"]
lista_pedidos_qa = ["0404813351"]
fecha_test_qa = "2020-08-13"
planta_test_qa = "1208"

#NEVER USE PROD NAMES
dyna_opt = "Pedido_dev_opt"
dyna_usr = "Pedido_dev_usr"

input_test_pedido_simple = []
ped = {}
ped["Agrupador"]="1"
ped["PosAgrupador"]="0"
ped["Vbeln"]="0404363703"
ped["Vbtyp"]="C"
ped["Auart"]="ZCES"
ped["Bsark"]=""
ped["Vehicle"]="8730"
ped["StartDate"]="2020-07-01"
ped["StartTime"]="10:00:00"
ped["EndDate"]="2020-07-01"
ped["EndTime"]="11:24:00"
ped["Duracion"]="01:24:00"
ped["DuracionGrupo"]=""
ped["Volumen"]=""
input_test_pedido_simple.append(ped)

input_test_pedido_combinado = []
ped = {}
ped["Agrupador"]="1"
ped["PosAgrupador"]="0"
ped["Vbeln"]="0404363703"
ped["Vbtyp"]="C"
ped["Auart"]="ZCES"
ped["Bsark"]=""
ped["Vehicle"]="8730"
ped["StartDate"]="2020-07-01"
ped["StartTime"]="10:00:00"
ped["EndDate"]="2020-07-01"
ped["EndTime"]="13:00:00"
ped["Duracion"]="01:24:00"
ped["DuracionGrupo"]="03:00:00"
ped["Volumen"]=""

ped2 = {}
ped2["Agrupador"]="1"
ped2["PosAgrupador"]="0"
ped2["Vbeln"]="0404363704"
ped2["Vbtyp"]="C"
ped2["Auart"]="ZCES"
ped2["Bsark"]=""
ped2["Vehicle"]="8730"
ped2["StartDate"]="2020-07-01"
ped2["StartTime"]="10:00:00"
ped2["EndDate"]="2020-07-01"
ped2["EndTime"]="13:00:00"
ped2["Duracion"]="01:24:00"
ped2["DuracionGrupo"]="03:00:00"
ped2["Volumen"]=""
input_test_pedido_combinado.append(ped)
input_test_pedido_combinado.append(ped2)

input_test_pedido_simple_DYNAMO = {"items":[]}
ped_dyna = {
  "AGRUPADOR": "",
  "AUART": "ZCES",
  "BEZEI_AUART": "Venta / Repos. a E/S",
  "BEZEI_DELCO": "00:00-23:59 Cualquie",
  "BSARK": "ZSTD",
  "CAMION_DEDICADO": "0",
  "CMGST": "B",
  "COMENTARIOS": "",
  "CUENTA_LITROS": "S",
  "DELCO": "004",
  "DETALLE": "24.000,000 = 20.000,000-4.000,000",
  "DETALLE_PEDIDO": [
    {
      "KWMENG": "20000.0",
      "MAKTX": "GASOLINA SP 95 OCTANOS  NU 1203",
      "MATNR": "000000000000000003",
      "MEINS": "L",
      "VBELN": "0404363703"
    },
    {
      "KWMENG": "4000.0",
      "MAKTX": "GASOLINA SP 97 OCTANOS PREMIUM NU 1203",
      "MATNR": "000000000000000005",
      "MEINS": "L",
      "VBELN": "0404363703"
    }
  ],
  "DURACION": "020127",
  "DURACION_CALCULA": "014900",
  "DURACION_CLIENTES": "112400",
  "DURACION_GRUPO": "000000",
  "DURACION_TRAMO1": "005730",
  "ENT_BEGDA": "20200701",
  "ENT_BEGTI": "000000",
  "ENT_ENDDA": "20200701",
  "ENT_ENDTI": "235900",
  "ERROR_MESSAGE": " El pedido 0404363703 se encuentra bloqueado.",
  "ERRORS": "S",
  "ESPECIAL": "00",
  "ESTADO": "1",
  "GROUPNAME": "00001100",
  "GROUPTEXT": "Livianos Normal",
  "HIGHLIGHTED": "N",
  "KUNAG": "0000040128",
  "NAME1_KUNAG": "COMERCIAL MAHANA Y COMPANIA LT",
  "POS_AGRUPADOR": "000000",
  "PRO_BEGDA": "20200701",
  "PRO_BEGTI": "100000",
  "PRO_ENDDA": "20200701",
  "PRO_ENDTI": "112400",
  "UNCONFIRM_CODE": "",
  "UNCONFIRM_COMMENT": "",
  "UNSUBSCRIBE_CODE": "",
  "UNSUBSCRIBE_COMMENT": "",
  "VBELN": "0404363703",
  "VBTYP": "C",
  "VDATU": "2020-07-01",
  "VEHICLE": "0000008730",
  "VTEXT": "VINA DEL MAR",
  "VTEXT_BSARK": "Estándar",
  "WARNING_MESSAGE": " ",
  "WARNINGS": "N",
  "WRITE_TIMESTAMP": "100",
  "ZONE1": "Z000000364"
}
input_test_pedido_simple_DYNAMO["items"].append(ped_dyna)
# lista_pedidos -de la forma->{items:[{...},{...}..]

input_test_pedido_combinado_DYNAMO = {"items":[]}
ped_dyna = {
  "AGRUPADOR": "1",
  "AUART": "ZCES",
  "BEZEI_AUART": "Venta / Repos. a E/S",
  "BEZEI_DELCO": "00:00-23:59 Cualquie",
  "BSARK": "ZSTD",
  "CAMION_DEDICADO": "0",
  "CMGST": "B",
  "COMENTARIOS": "",
  "CUENTA_LITROS": "S",
  "DELCO": "004",
  "DETALLE": "24.000,000 = 20.000,000-4.000,000",
  "DETALLE_PEDIDO": [
    {
      "KWMENG": "20000.0",
      "MAKTX": "GASOLINA SP 95 OCTANOS  NU 1203",
      "MATNR": "000000000000000003",
      "MEINS": "L",
      "VBELN": "0404363703"
    },
    {
      "KWMENG": "4000.0",
      "MAKTX": "GASOLINA SP 97 OCTANOS PREMIUM NU 1203",
      "MATNR": "000000000000000005",
      "MEINS": "L",
      "VBELN": "0404363703"
    }
  ],
  "DURACION": "020127",
  "DURACION_CALCULA": "014900",
  "DURACION_CLIENTES": "112400",
  "DURACION_GRUPO": "050000",
  "DURACION_TRAMO1": "005730",
  "ENT_BEGDA": "20200701",
  "ENT_BEGTI": "000000",
  "ENT_ENDDA": "20200701",
  "ENT_ENDTI": "235900",
  "ERROR_MESSAGE": " El pedido 0404363703 se encuentra bloqueado.",
  "ERRORS": "S",
  "ESPECIAL": "00",
  "ESTADO": "1",
  "GROUPNAME": "00001100",
  "GROUPTEXT": "Livianos Normal",
  "HIGHLIGHTED": "N",
  "KUNAG": "0000040128",
  "NAME1_KUNAG": "COMERCIAL MAHANA Y COMPANIA LT",
  "POS_AGRUPADOR": "000000",
  "PRO_BEGDA": "20200701",
  "PRO_BEGTI": "100000",
  "PRO_ENDDA": "20200701",
  "PRO_ENDTI": "150000",
  "UNCONFIRM_CODE": "",
  "UNCONFIRM_COMMENT": "",
  "UNSUBSCRIBE_CODE": "",
  "UNSUBSCRIBE_COMMENT": "",
  "VBELN": "0404363703",
  "VBTYP": "C",
  "VDATU": "2020-07-01",
  "VEHICLE": "0000008730",
  "VTEXT": "VINA DEL MAR",
  "VTEXT_BSARK": "Estándar",
  "WARNING_MESSAGE": " ",
  "WARNINGS": "N",
  "WRITE_TIMESTAMP": "100",
  "ZONE1": "Z000000364"
}
ped_dyna2 = {
  "AGRUPADOR": "1",
  "AUART": "ZCES",
  "BEZEI_AUART": "Venta / Repos. a E/S",
  "BEZEI_DELCO": "00:00-23:59 Cualquie",
  "BSARK": "ZSTD",
  "CAMION_DEDICADO": "0",
  "CMGST": "B",
  "COMENTARIOS": "",
  "CUENTA_LITROS": "S",
  "DELCO": "004",
  "DETALLE": "24.000,000 = 20.000,000-4.000,000",
  "DETALLE_PEDIDO": [
    {
      "KWMENG": "20000.0",
      "MAKTX": "GASOLINA SP 95 OCTANOS  NU 1203",
      "MATNR": "000000000000000003",
      "MEINS": "L",
      "VBELN": "0404363704"
    },
    {
      "KWMENG": "4000.0",
      "MAKTX": "GASOLINA SP 97 OCTANOS PREMIUM NU 1203",
      "MATNR": "000000000000000005",
      "MEINS": "L",
      "VBELN": "0404363704"
    }
  ],
  "DURACION": "020127",
  "DURACION_CALCULA": "014900",
  "DURACION_CLIENTES": "112400",
  "DURACION_GRUPO": "050000",
  "DURACION_TRAMO1": "005730",
  "ENT_BEGDA": "20200701",
  "ENT_BEGTI": "000000",
  "ENT_ENDDA": "20200701",
  "ENT_ENDTI": "235900",
  "ERROR_MESSAGE": " El pedido 0404363704 se encuentra bloqueado.",
  "ERRORS": "S",
  "ESPECIAL": "00",
  "ESTADO": "1",
  "GROUPNAME": "00001100",
  "GROUPTEXT": "Livianos Normal",
  "HIGHLIGHTED": "N",
  "KUNAG": "0000040128",
  "NAME1_KUNAG": "COMERCIAL MAHANA Y COMPANIA LT",
  "POS_AGRUPADOR": "000000",
  "PRO_BEGDA": "20200701",
  "PRO_BEGTI": "100000",
  "PRO_ENDDA": "20200701",
  "PRO_ENDTI": "150000",
  "UNCONFIRM_CODE": "",
  "UNCONFIRM_COMMENT": "",
  "UNSUBSCRIBE_CODE": "",
  "UNSUBSCRIBE_COMMENT": "",
  "VBELN": "0404363704",
  "VBTYP": "C",
  "VDATU": "2020-07-01",
  "VEHICLE": "0000008730",
  "VTEXT": "VINA DEL MAR",
  "VTEXT_BSARK": "Estándar",
  "WARNING_MESSAGE": " ",
  "WARNINGS": "N",
  "WRITE_TIMESTAMP": "100",
  "ZONE1": "Z000000364"
}
input_test_pedido_combinado_DYNAMO["items"].append(ped_dyna)
input_test_pedido_combinado_DYNAMO["items"].append(ped_dyna2)

input_test_pedido_simple_DYNAMO_usr = {"items":[]}
ped_dyna = {
  "AGRUPADOR": "",
  "AUART": "ZCES",
  "BEZEI_AUART": "Venta / Repos. a E/S",
  "BEZEI_DELCO": "00:00-23:59 Cualquie",
  "BSARK": "ZSTD",
  "CAMION_DEDICADO": "0",
  "CMGST": "B",
  "COMENTARIOS": "",
  "CUENTA_LITROS": "S",
  "DELCO": "004",
  "DETALLE": "24.000,000 = 20.000,000-4.000,000",
  "DETALLE_PEDIDO": [
    {
      "KWMENG": "20000.0",
      "MAKTX": "GASOLINA SP 95 OCTANOS  NU 1203",
      "MATNR": "000000000000000003",
      "MEINS": "L",
      "VBELN": "0404363703"
    },
    {
      "KWMENG": "4000.0",
      "MAKTX": "GASOLINA SP 97 OCTANOS PREMIUM NU 1203",
      "MATNR": "000000000000000005",
      "MEINS": "L",
      "VBELN": "0404363703"
    }
  ],
  "DURACION": "020127",
  "DURACION_CALCULA": "014900",
  "DURACION_CLIENTES": "112400",
  "DURACION_GRUPO": "000000",
  "DURACION_TRAMO1": "005730",
  "ENT_BEGDA": "20200701",
  "ENT_BEGTI": "000000",
  "ENT_ENDDA": "20200701",
  "ENT_ENDTI": "235900",
  "ERROR_MESSAGE": " El pedido 0404363703 se encuentra bloqueado.",
  "ERRORS": "S",
  "ESPECIAL": "00",
  "ESTADO": "1",
  "GROUPNAME": "00001100",
  "GROUPTEXT": "Livianos Normal",
  "HIGHLIGHTED": "N",
  "KUNAG": "0000040128",
  "NAME1_KUNAG": "COMERCIAL MAHANA Y COMPANIA LT",
  "POS_AGRUPADOR": "000000",
  "PRO_BEGDA": "20200701",
  "PRO_BEGTI": "110000",
  "PRO_ENDDA": "20200701",
  "PRO_ENDTI": "122400",
  "UNCONFIRM_CODE": "",
  "UNCONFIRM_COMMENT": "",
  "UNSUBSCRIBE_CODE": "",
  "UNSUBSCRIBE_COMMENT": "",
  "VBELN": "0404363703",
  "VBTYP": "C",
  "VDATU": "2020-07-01",
  "VEHICLE": "0000008731",
  "VTEXT": "VINA DEL MAR",
  "VTEXT_BSARK": "Estándar",
  "WARNING_MESSAGE": " ",
  "WARNINGS": "N",
  "WRITE_TIMESTAMP": "200",
  "ZONE1": "Z000000364"
}
input_test_pedido_simple_DYNAMO_usr["items"].append(ped_dyna)

input_test_pedido_combinado_DYNAMO_usr = {"items":[]}
ped_dyna = {
  "AGRUPADOR": "1",
  "AUART": "ZCES",
  "BEZEI_AUART": "Venta / Repos. a E/S",
  "BEZEI_DELCO": "00:00-23:59 Cualquie",
  "BSARK": "ZSTD",
  "CAMION_DEDICADO": "0",
  "CMGST": "B",
  "COMENTARIOS": "",
  "CUENTA_LITROS": "S",
  "DELCO": "004",
  "DETALLE": "24.000,000 = 20.000,000-4.000,000",
  "DETALLE_PEDIDO": [
    {
      "KWMENG": "20000.0",
      "MAKTX": "GASOLINA SP 95 OCTANOS  NU 1203",
      "MATNR": "000000000000000003",
      "MEINS": "L",
      "VBELN": "0404363703"
    },
    {
      "KWMENG": "4000.0",
      "MAKTX": "GASOLINA SP 97 OCTANOS PREMIUM NU 1203",
      "MATNR": "000000000000000005",
      "MEINS": "L",
      "VBELN": "0404363703"
    }
  ],
  "DURACION": "020127",
  "DURACION_CALCULA": "014900",
  "DURACION_CLIENTES": "112400",
  "DURACION_GRUPO": "050000",
  "DURACION_TRAMO1": "005730",
  "ENT_BEGDA": "20200701",
  "ENT_BEGTI": "000000",
  "ENT_ENDDA": "20200701",
  "ENT_ENDTI": "235900",
  "ERROR_MESSAGE": " El pedido 0404363703 se encuentra bloqueado.",
  "ERRORS": "S",
  "ESPECIAL": "00",
  "ESTADO": "1",
  "GROUPNAME": "00001100",
  "GROUPTEXT": "Livianos Normal",
  "HIGHLIGHTED": "N",
  "KUNAG": "0000040128",
  "NAME1_KUNAG": "COMERCIAL MAHANA Y COMPANIA LT",
  "POS_AGRUPADOR": "000000",
  "PRO_BEGDA": "20200701",
  "PRO_BEGTI": "110000",
  "PRO_ENDDA": "20200701",
  "PRO_ENDTI": "160000",
  "UNCONFIRM_CODE": "",
  "UNCONFIRM_COMMENT": "",
  "UNSUBSCRIBE_CODE": "",
  "UNSUBSCRIBE_COMMENT": "",
  "VBELN": "0404363703",
  "VBTYP": "C",
  "VDATU": "2020-07-01",
  "VEHICLE": "0000008731",
  "VTEXT": "VINA DEL MAR",
  "VTEXT_BSARK": "Estándar",
  "WARNING_MESSAGE": " ",
  "WARNINGS": "N",
  "WRITE_TIMESTAMP": "200",
  "ZONE1": "Z000000364"
}
ped_dyna2 = {
  "AGRUPADOR": "1",
  "AUART": "ZCES",
  "BEZEI_AUART": "Venta / Repos. a E/S",
  "BEZEI_DELCO": "00:00-23:59 Cualquie",
  "BSARK": "ZSTD",
  "CAMION_DEDICADO": "0",
  "CMGST": "B",
  "COMENTARIOS": "",
  "CUENTA_LITROS": "S",
  "DELCO": "004",
  "DETALLE": "24.000,000 = 20.000,000-4.000,000",
  "DETALLE_PEDIDO": [
    {
      "KWMENG": "20000.0",
      "MAKTX": "GASOLINA SP 95 OCTANOS  NU 1203",
      "MATNR": "000000000000000003",
      "MEINS": "L",
      "VBELN": "0404363704"
    },
    {
      "KWMENG": "4000.0",
      "MAKTX": "GASOLINA SP 97 OCTANOS PREMIUM NU 1203",
      "MATNR": "000000000000000005",
      "MEINS": "L",
      "VBELN": "0404363704"
    }
  ],
  "DURACION": "020127",
  "DURACION_CALCULA": "014900",
  "DURACION_CLIENTES": "112400",
  "DURACION_GRUPO": "050000",
  "DURACION_TRAMO1": "005730",
  "ENT_BEGDA": "20200701",
  "ENT_BEGTI": "000000",
  "ENT_ENDDA": "20200701",
  "ENT_ENDTI": "235900",
  "ERROR_MESSAGE": " El pedido 0404363704 se encuentra bloqueado.",
  "ERRORS": "S",
  "ESPECIAL": "00",
  "ESTADO": "1",
  "GROUPNAME": "00001100",
  "GROUPTEXT": "Livianos Normal",
  "HIGHLIGHTED": "N",
  "KUNAG": "0000040128",
  "NAME1_KUNAG": "COMERCIAL MAHANA Y COMPANIA LT",
  "POS_AGRUPADOR": "000000",
  "PRO_BEGDA": "20200701",
  "PRO_BEGTI": "110000",
  "PRO_ENDDA": "20200701",
  "PRO_ENDTI": "160000",
  "UNCONFIRM_CODE": "",
  "UNCONFIRM_COMMENT": "",
  "UNSUBSCRIBE_CODE": "",
  "UNSUBSCRIBE_COMMENT": "",
  "VBELN": "0404363704",
  "VBTYP": "C",
  "VDATU": "2020-07-01",
  "VEHICLE": "0000008731",
  "VTEXT": "VINA DEL MAR",
  "VTEXT_BSARK": "Estándar",
  "WARNING_MESSAGE": " ",
  "WARNINGS": "N",
  "WRITE_TIMESTAMP": "200",
  "ZONE1": "Z000000364"
}
input_test_pedido_combinado_DYNAMO_usr["items"].append(ped_dyna)
input_test_pedido_combinado_DYNAMO_usr["items"].append(ped_dyna2)

def to_dict(input_ordered_dict):
    return loads(dumps(input_ordered_dict))

#desconfirma todos los pedidos de lista ids
def confirmar(listapedidos_id,planta,fecha):
    session = Session()
    session.auth = HTTPBasicAuth(user, pwd)
    urlConn = "http://coqabroker1.copec.cl:7812/programador/zws_pr_prog_sap_confirmar?wsdl"
    client = Client(urlConn, transport=Transport(session=session))
  
    factory = client.type_factory('ns1')
    ItPedidos = factory.ZprogTtConfirmarProg()
    
    for id_pedido in listapedidos_id:
        ped = {}
        ped["Agrupador"]="1"
        ped["PosAgrupador"]="0"
        ped["Vbeln"]=id_pedido
        ped["Vbtyp"]="C"
        ped["Auart"]="ZCES"
        ped["Bsark"]=""
        ped["Vehicle"]="8730"
        ped["StartDate"]="2020-07-01"
        ped["StartTime"]="11:00:00"
        ped["EndDate"]=""
        ped["EndTime"]="11:24:00"
        ped["Duracion"]="01:24:00"
        ped["DuracionGrupo"]=""
        ped["Volumen"]=""

        ItPedidos.item.append(ped)
    
    response = client.service.ZprProgSapConfirmar(fecha, planta, [], ItPedidos)
    return response

def desprogramar_lista(listapedidos_id,planta,fecha):
    session = Session()
    session.auth = HTTPBasicAuth(user, pwd)
    urlConn = "http://coqabroker1.copec.cl:7812/programador/zpr_prog_sap_grabar?wsdl"
    client = Client(urlConn, transport=Transport(session=session))
    factory = client.type_factory('ns1')
    
    ItCambioCamion=factory.ZttdfAudCami()
    ItPedidos=factory.ZprogTtConfirmarProg()
    
    for id_pedido in listapedidos_id:
        ped = {}
        ped["Agrupador"]=""
        ped["PosAgrupador"]=""
        ped["Vbeln"]=id_pedido
        ped["Vbtyp"]="C"
        ped["Auart"]="ZCES"
        ped["Bsark"]=""
        ped["Vehicle"]=""
        ped["StartDate"]=""
        ped["StartTime"]=""
        ped["EndDate"]=""
        ped["EndTime"]=""
        ped["Duracion"]=""
        ped["DuracionGrupo"]=""
        ped["Volumen"]=""

        ItPedidos.item.append(ped)
    print(fecha)
    print(planta)
    print(ItCambioCamion)
    print(ItPedidos)
    
    response = client.service.ZprProgSapGrabar(fecha, planta,ItCambioCamion, ItPedidos )
    return response
    
 
def programar(listapedidos_obj,planta,fecha):
    session = Session()
    session.auth = HTTPBasicAuth(user, pwd)
    urlConn = "http://coqabroker1.copec.cl:7812/programador/zpr_prog_sap_grabar?wsdl"
    client = Client(urlConn, transport=Transport(session=session))
    factory = client.type_factory('ns1')
    
    ItCambioCamion=factory.ZttdfAudCami()
    ItPedidos=factory.ZprogTtConfirmarProg()
    
    for pedido_obj in listapedidos_obj:
        ped = {}
        ped["Agrupador"]=pedido_obj["Agrupador"]
        ped["PosAgrupador"]=pedido_obj["PosAgrupador"]
        ped["Vbeln"]=pedido_obj["Vbeln"]
        ped["Vbtyp"]=pedido_obj["Vbtyp"]
        ped["Auart"]=pedido_obj["Auart"]
        ped["Bsark"]=pedido_obj["Bsark"]
        ped["Vehicle"]=pedido_obj["Vehicle"]
        ped["StartDate"]=pedido_obj["StartDate"]
        ped["StartTime"]=pedido_obj["StartTime"]
        ped["EndDate"]=pedido_obj["EndDate"]
        ped["EndTime"]=pedido_obj["EndTime"]
        ped["Duracion"]=pedido_obj["Duracion"]
        ped["DuracionGrupo"]=pedido_obj["DuracionGrupo"]
        ped["Volumen"]=pedido_obj["Volumen"]

        ItPedidos.item.append(ped)
    
    response = client.service.ZprProgSapGrabar(fecha, planta,ItCambioCamion, ItPedidos )
    return response
    

def desconfirmar_lista(lista_pedidos,planta,fecha):
    session = Session()
    session.auth = HTTPBasicAuth(user, pwd)
    urlConn = "http://coqabroker1.copec.cl:7812/programador/zws_pr_prog_sap_desconfirmar?wsdl"
    client = Client(urlConn, transport=Transport(session=session))
  
    factory = client.type_factory('ns1')
    CtPedidos = factory.ZprogTtDesconfirmarProg()
    IFechaFin = factory.date10("")
    IFechaIni = factory.date10("")
    IHoraFin = factory.time("")
    IHoraIni = factory.time("")
    
    for pedido_id in lista_pedidos:
        ped = {}
        ped["Vbeln"] = pedido_id
        ped["Vbtyp"] = "C"
        ped["Motivo"] = ""
        ped["Subrc"] = ""
        ped["Message"] = ""
        ped["DuracionCalcula"] = ""
        ped["Tipo"] = "P"
        ped["Observacion"] = ""
        CtPedidos.item.append(ped)
    
    response = client.service.ZprProgSapDesconfirmar(CtPedidos,IFechaFin, IFechaIni, IHoraFin, IHoraIni )
    #print(response)

def get_programacion(planta,fecha):

    session = Session()
    session.auth = HTTPBasicAuth(user, pwd)
    urlConn = "http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl"
    client = Client(urlConn, transport=Transport(session=session))
    
    factory = client.type_factory('ns0')
    ItClienteFecha = factory.ZprogTtKunnrDatum()
    sapCode=2
    with client.settings(strict=False,raw_response=True):
        response = client.service.ZbdpObtieneProgramacion(fecha,sapCode,planta,ItClienteFecha)
    string_response_xml = response.content#.decode("utf-8")
    response_json=to_dict(xmltodict.parse(string_response_xml))
    
    base = response_json["soap-env:Envelope"]["soap-env:Body"]["n0:ZbdpObtieneProgramacionResponse"]
            
    res={}
    res["ERc"]=base["ERc"] if base["ERc"] != None else " "
    res["EMessage"]=base["EMessage"] if base["EMessage"] != None else " "
    res["EtVehicles"]=base["EtVehicles"] if base["EtVehicles"] != None else " "
    res["EtVehicleDet"]=base["EtVehicleDet"] if base["EtVehicleDet"] != None else " "
    res["EtPedidos"]=base["EtPedidos"] if base["EtPedidos"] != None else " "
    res["EtPedidoDet"]=base["EtPedidoDet"] if base["EtPedidoDet"] != None else " "
    
    return res
    
def cleanDynamo(table):
    
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(table)
    
    items = table.scan()
    for item in items["Items"]:
        #print(item["VBELN"])
        #print(item["WRITE_TIMESTAMP"])
        table.delete_item(
            Key={
                "VBELN": item["VBELN"],
                "WRITE_TIMESTAMP": item["WRITE_TIMESTAMP"]
            }
        )
        #print("=====")
    
    


def get_dynamo(table,vbeln_list):
    dyna_service = DynamoService(table)
    input = {"VBELN": {"item":vbeln_list}}
    ret = dyna_service.scan_items(input)
    return ret["Items"]

def write_dynamo(table,lista_pedidos):
    # lista_pedidos -de la forma->{items:[{...},{...}..]
    dyna_service = DynamoService(table)
    dyna_service.put_items(lista_pedidos)

def prepare_simple():
    # vbeln   "404363703"
    # vehicle "8730"
    print("desconfirmando todos los pedidos")
    desconfirmar_lista(lista_pedidos_qa,planta_test_qa,fecha_test_qa)
    print("desprogramando todos los pedidos")
    desprogramar_lista(lista_pedidos_qa,planta_test_qa,fecha_test_qa)
    print("limpuando dynamo" + dyna_usr)
    cleanDynamo(dyna_usr)
    print("limpuando dynamo" + dyna_opt)
    cleanDynamo(dyna_opt)
    print("ready prepare")

def test_1():
    #T1_GET_PLAN 
    #Condicion :
    #SAP CON UN PEDIDO
    #Pedidos opt SIN DATA
    #pedidos usr SIN DATA

    #Esperable:
    #Comparar Pedido sap, con tiempos de athena vs el input.
    
    #preparamos
    prepare_simple()
    print("programando pedido simple")
    programar(input_test_pedido_simple,planta_test_qa,fecha_test_qa)
    
    #variables de entorno para la funcion
    os.environ["SAP_URL"] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl"
    os.environ['SAP_URL_XTRAS'] = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
    os.environ['SAP_URL_MESSAGES'] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_info_prog_sap?wsdl"
    
    os.environ['DYNA_PEDIDOS_USR'] = "Pedido_dev_usr"
    os.environ['DYNA_PEDIDOS_OPT'] = "Pedido_dev_opt"
    
    #handler_get de get plan
    input = {
        "codigo_planta": [
            "1208"
        ],
        "codigo_fecha": ["20200701"],
        "sap_user": "aelgueda",
        "sap_pwd": "Qprograma360",
        "night_plan": ""
    }
    res = handler_get(input,True)
    #print(res)
    
    
    vbeln_programado = input_test_pedido_simple[0]["Vbeln"]  #"404363703"
    #obenemos de la respuesta de sap todos los pedidos cuyos vehiculos esten asignados y chequeamos ESTADO, PEDIDO TIEMPOS ETC
    f = filter(lambda x: x["VEHICLE"].strip() != "", res["pedidos"])
    
    resultado_a_evaluar = list(f)
    
    assert len(input_test_pedido_simple) == len(resultado_a_evaluar)
    
    #no se compara duracion porque la duracion que viene del servicio,viene de athena y no va  a ser igual a la de SAP
    compare_keys = [{"nameInput":"Vbeln","nameSap":"VBELN","type":"int"},  
                    {"nameInput":"Vbtyp","nameSap":"VBTYP","type":"str"},
                    {"nameInput":"Auart","nameSap":"AUART","type":"str"},
                    {"nameInput":"Vehicle","nameSap":"VEHICLE","type":"int"},
                    {"nameInput":"StartDate","nameSap":"PRO_BEGDA","type":"date"},
                    {"nameInput":"StartTime","nameSap":"PRO_BEGTI","type":"time"},
                    {"nameInput":"EndDate","nameSap":"PRO_ENDDA","type":"date"},
                    {"nameInput":"EndTime","nameSap":"PRO_ENDTI","type":"time"},
                    #{"nameInput":"Duracion","nameSap":"DURACION","type":"time"}
                    ]
    
    
    print(input_test_pedido_simple[0])
    print("----------")
    print(resultado_a_evaluar[0])
    for key in compare_keys:
        if key["type"]=="int":
            assert int(input_test_pedido_simple[0][key["nameInput"]]) ==  int(resultado_a_evaluar[0][key["nameSap"]])
        if key["type"]=="str":
            assert input_test_pedido_simple[0][key["nameInput"]] ==  resultado_a_evaluar[0][key["nameSap"]]
        if key["type"]=="date":
            assert input_test_pedido_simple[0][key["nameInput"]].replace("-","") ==  resultado_a_evaluar[0][key["nameSap"]]
        if key["type"]=="time":
            assert input_test_pedido_simple[0][key["nameInput"]].replace(":","") ==  resultado_a_evaluar[0][key["nameSap"]] 
    
    
def test_2():
    #T2_GET_PLAN
    #Condicion :
    #SAP CON UN PEDIDO
    #Pedidos opt DATA DE UN PEDIDO
    #pedidos usr SIN DATA    
    
    #Esperable:
    #Comparar Pedido sap vs Dynamo.
    
    prepare_simple()
    print("programando pedido simple")
    programar(input_test_pedido_simple,planta_test_qa,fecha_test_qa)
    print("escribiendo en tabla opt de dynamo")
    write_dynamo(dyna_opt,input_test_pedido_simple_DYNAMO)
    
    #variables de entorno para la funcion
    os.environ["SAP_URL"] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl"
    os.environ['SAP_URL_XTRAS'] = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
    os.environ['SAP_URL_MESSAGES'] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_info_prog_sap?wsdl"
    
    os.environ['DYNA_PEDIDOS_USR'] = "Pedido_dev_usr"
    os.environ['DYNA_PEDIDOS_OPT'] = "Pedido_dev_opt"
    
    #handler_get de get plan
    input = {
        "codigo_planta": [
            "1208"
        ],
        "codigo_fecha": ["20200701"],
        "sap_user": "aelgueda",
        "sap_pwd": "Qprograma360",
        "night_plan": ""
    }
    res = handler_get(input,True)
    
    vbeln_programado = input_test_pedido_simple[0]["Vbeln"]  #"0404363703"
    #obenemos de la respuesta de sap todos los pedidos cuyos vehiculos esten asignados y chequeamos ESTADO, PEDIDO TIEMPOS ETC
    f = filter(lambda x: x["VEHICLE"].strip() != "", res["pedidos"])
    
    resultado_a_evaluar = list(f)
    
    dyna_result = get_dynamo(dyna_opt,[vbeln_programado])
    
    assert len(resultado_a_evaluar) == len(dyna_result)
    
    #print("resultado_a_evaluar")
    #print(resultado_a_evaluar)
    
    compare_keys = [{"nameInput":"VBELN","nameSap":"VBELN","type":"int"},  
                    {"nameInput":"VBTYP","nameSap":"VBTYP","type":"str"},
                    {"nameInput":"AUART","nameSap":"AUART","type":"str"},
                    {"nameInput":"VEHICLE","nameSap":"VEHICLE","type":"int"},
                    {"nameInput":"PRO_BEGDA","nameSap":"PRO_BEGDA","type":"date"},
                    {"nameInput":"PRO_BEGTI","nameSap":"PRO_BEGTI","type":"time"},
                    {"nameInput":"PRO_ENDDA","nameSap":"PRO_ENDDA","type":"date"},
                    {"nameInput":"PRO_ENDTI","nameSap":"PRO_ENDTI","type":"time"},
                    #{"nameInput":"Duracion","nameSap":"DURACION","type":"time"}
                    ]
    
    for key in compare_keys:
        if key["type"]=="int":
            assert int(resultado_a_evaluar[0][key["nameInput"]]) ==  int(dyna_result[0][key["nameSap"]])
        if key["type"]=="str":
            assert resultado_a_evaluar[0][key["nameInput"]] ==  dyna_result[0][key["nameSap"]]
        if key["type"]=="date":
            assert resultado_a_evaluar[0][key["nameInput"]].replace("-","") ==  dyna_result[0][key["nameSap"]]
        if key["type"]=="time":
            assert resultado_a_evaluar[0][key["nameInput"]].replace(":","") ==  dyna_result[0][key["nameSap"]] 
    #for result in dyna_result:
        
    
def test_3():
    #T3_GET_PLAN
    #Condicion :
    #SAP CON UN PEDIDO
    #Pedidos opt DATA DE UN PEDIDO
    #pedidos usr DATA DE UN PEDIDO con timestamp mayor
    
    #Esperable:
    #Comparar Pedido sap vs Dynamo. Debe dominar el pedido de timestamp mayor (usR)
    
    prepare_simple()
    print("programando pedido simple")
    programar(input_test_pedido_simple,planta_test_qa,fecha_test_qa)
    print("escribiendo en tabla opt de dynamo")
    write_dynamo(dyna_opt,input_test_pedido_simple_DYNAMO)
    print("escribiendo en tabla usr de dynamo")
    write_dynamo(dyna_usr,input_test_pedido_simple_DYNAMO_usr)
    
    #variables de entorno para la funcion
    os.environ["SAP_URL"] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl"
    os.environ['SAP_URL_XTRAS'] = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
    os.environ['SAP_URL_MESSAGES'] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_info_prog_sap?wsdl"
    
    os.environ['DYNA_PEDIDOS_USR'] = "Pedido_dev_usr"
    os.environ['DYNA_PEDIDOS_OPT'] = "Pedido_dev_opt"
    
    #handler_get de get plan
    input = {
        "codigo_planta": [
            "1208"
        ],
        "codigo_fecha": ["20200701"],
        "sap_user": "aelgueda",
        "sap_pwd": "Qprograma360",
        "night_plan": ""
    }
    res = handler_get(input,True)
    
    vbeln_programado = input_test_pedido_simple[0]["Vbeln"]  #"0404363703"
    #obenemos de la respuesta de sap todos los pedidos cuyos vehiculos esten asignados y chequeamos ESTADO, PEDIDO TIEMPOS ETC
    f = filter(lambda x: x["VEHICLE"].strip() != "", res["pedidos"])
    
    resultado_a_evaluar = list(f)
    dyna_result = get_dynamo(dyna_usr,[vbeln_programado]) #con opt debe fallar
    assert len(resultado_a_evaluar) == len(dyna_result)
    
    compare_keys = [{"nameInput":"VBELN","nameSap":"VBELN","type":"int"},  
                    {"nameInput":"VBTYP","nameSap":"VBTYP","type":"str"},
                    {"nameInput":"AUART","nameSap":"AUART","type":"str"},
                    {"nameInput":"VEHICLE","nameSap":"VEHICLE","type":"int"},
                    {"nameInput":"PRO_BEGDA","nameSap":"PRO_BEGDA","type":"date"},
                    {"nameInput":"PRO_BEGTI","nameSap":"PRO_BEGTI","type":"time"},
                    {"nameInput":"PRO_ENDDA","nameSap":"PRO_ENDDA","type":"date"},
                    {"nameInput":"PRO_ENDTI","nameSap":"PRO_ENDTI","type":"time"},
                    #{"nameInput":"Duracion","nameSap":"DURACION","type":"time"}
                    ]
    
    for key in compare_keys:
        if key["type"]=="int":
            assert int(resultado_a_evaluar[0][key["nameInput"]]) ==  int(dyna_result[0][key["nameSap"]])
        if key["type"]=="str":
            assert resultado_a_evaluar[0][key["nameInput"]] ==  dyna_result[0][key["nameSap"]]
        if key["type"]=="date":
            assert resultado_a_evaluar[0][key["nameInput"]].replace("-","") ==  dyna_result[0][key["nameSap"]]
        if key["type"]=="time":
            assert resultado_a_evaluar[0][key["nameInput"]].replace(":","") ==  dyna_result[0][key["nameSap"]] 

def test_4():
    #T4_GET_PLAN
    #Condicion :
    #SAP SIN PEDIDO
    #Pedidos opt DATA DE UN PEDIDO
    #pedidos usr DATA DE UN PEDIDO con timestamp mayor

    #Esperable:
    #Sin pedidos porque no hay pedido en sap, pese a que haya data en opt y usr
    prepare_simple()
    print("NO PROGRAMAMOS pedido simple en SAP")
    print("escribiendo en tabla opt de dynamo")
    #write_dynamo(dyna_opt,input_test_pedido_simple_DYNAMO)
    print("escribiendo en tabla usr de dynamo")
    #write_dynamo(dyna_usr,input_test_pedido_simple_DYNAMO_usr)
    
    #variables de entorno para la funcion
    os.environ["SAP_URL"] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl"
    os.environ['SAP_URL_XTRAS'] = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
    os.environ['SAP_URL_MESSAGES'] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_info_prog_sap?wsdl"
    
    os.environ['DYNA_PEDIDOS_USR'] = "Pedido_dev_usr"
    os.environ['DYNA_PEDIDOS_OPT'] = "Pedido_dev_opt"
    
    #handler_get de get plan
    input = {
        "codigo_planta": [
            "1208"
        ],
        "codigo_fecha": ["20200701"],
        "sap_user": "aelgueda",
        "sap_pwd": "Qprograma360",
        "night_plan": ""
    }
    res = handler_get(input,True)
    
    #print(res)
    
    #vbeln_programado = input_test_pedido_simple[0]["Vbeln"]  #"0404363703"
    #obenemos de la respuesta de sap todos los pedidos cuyos vehiculos esten asignados y chequeamos ESTADO, PEDIDO TIEMPOS ETC
    f = filter(lambda x: x["VEHICLE"].strip() != "", res["pedidos"])
    
    resultado_a_evaluar = list(f)
    #print(len(resultado_a_evaluar))
    
    assert len(resultado_a_evaluar) == 0
  

#### REPETIMOS PERO COMBINADOS   

def test_1_combinado():
    #T1_GET_PLAN_combinado
    #Condicion :
    #SAP CON UN PEDIDO
    #Pedidos opt SIN DATA
    #pedidos usr SIN DATA

    #Esperable:
    #Comparar Pedido sap, con tiempos de athena vs el input.
    
    #preparamos
    prepare_simple()
    print("programando pedido combinado")
    programar(input_test_pedido_combinado,planta_test_qa,fecha_test_qa)
    
    #variables de entorno para la funcion
    os.environ["SAP_URL"] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl"
    os.environ['SAP_URL_XTRAS'] = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
    os.environ['SAP_URL_MESSAGES'] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_info_prog_sap?wsdl"
    
    os.environ['DYNA_PEDIDOS_USR'] = "Pedido_dev_usr"
    os.environ['DYNA_PEDIDOS_OPT'] = "Pedido_dev_opt"
    
    #handler_get de get plan
    input = {
        "codigo_planta": [
            "1208"
        ],
        "codigo_fecha": ["20200701"],
        "sap_user": "aelgueda",
        "sap_pwd": "Qprograma360",
        "night_plan": ""
    }
    res = handler_get(input,True)
    #print(res)
    
    
    vbeln_programado = input_test_pedido_simple[0]["Vbeln"]  #"404363703"
    #obenemos de la respuesta de sap todos los pedidos cuyos vehiculos esten asignados y chequeamos ESTADO, PEDIDO TIEMPOS ETC
    f = filter(lambda x: x["VEHICLE"].strip() != "", res["pedidos"])
    
    resultado_a_evaluar = list(f)
    
    assert len(input_test_pedido_combinado) == len(resultado_a_evaluar)
    
    #no se compara duracion porque la duracion que viene del servicio,viene de athena y no va  a ser igual a la de SAP
    compare_keys = [{"nameInput":"Vbeln","nameSap":"VBELN","type":"int"},  
                    {"nameInput":"Vbtyp","nameSap":"VBTYP","type":"str"},
                    {"nameInput":"Auart","nameSap":"AUART","type":"str"},
                    {"nameInput":"Vehicle","nameSap":"VEHICLE","type":"int"},
                    {"nameInput":"StartDate","nameSap":"PRO_BEGDA","type":"date"},
                    {"nameInput":"StartTime","nameSap":"PRO_BEGTI","type":"time"},
                    {"nameInput":"EndDate","nameSap":"PRO_ENDDA","type":"date"},
                    {"nameInput":"EndTime","nameSap":"PRO_ENDTI","type":"time"},
                    {"nameInput":"DuracionGrupo","nameSap":"DURACION_GRUPO","type":"time"} #NO se considera pq sap no guarda la duracion grupo tampoco
                    ]
    
    
    
    for key in compare_keys:
        if key["type"]=="int":
            assert int(input_test_pedido_simple[0][key["nameInput"]]) ==  int(resultado_a_evaluar[0][key["nameSap"]])
        if key["type"]=="str":
            assert input_test_pedido_simple[0][key["nameInput"]] ==  resultado_a_evaluar[0][key["nameSap"]]
        if key["type"]=="date":
            assert input_test_pedido_simple[0][key["nameInput"]].replace("-","") ==  resultado_a_evaluar[0][key["nameSap"]]
        if key["type"]=="time":
            assert input_test_pedido_simple[0][key["nameInput"]].replace(":","") ==  resultado_a_evaluar[0][key["nameSap"]]  

def test_2_combinado():
    #T2_GET_PLAN
    #Condicion :
    #SAP CON UN PEDIDO COMBINADO
    #Pedidos opt DATA DE UN PEDIDO COMBINADO
    #pedidos usr SIN DATA    
    
    #Esperable:
    #Comparar Pedido sap vs Dynamo.
    
    prepare_simple()
    print("programando pedido combinado")
    programar(input_test_pedido_combinado,planta_test_qa,fecha_test_qa)
    print("escribiendo en tabla opt de dynamo")
    write_dynamo(dyna_opt,input_test_pedido_combinado_DYNAMO)
    
    #variables de entorno para la funcion
    os.environ["SAP_URL"] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl"
    os.environ['SAP_URL_XTRAS'] = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
    os.environ['SAP_URL_MESSAGES'] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_info_prog_sap?wsdl"
    
    os.environ['DYNA_PEDIDOS_USR'] = "Pedido_dev_usr"
    os.environ['DYNA_PEDIDOS_OPT'] = "Pedido_dev_opt"
    
    #handler_get de get plan
    input = {
        "codigo_planta": [
            "1208"
        ],
        "codigo_fecha": ["20200701"],
        "sap_user": "aelgueda",
        "sap_pwd": "Qprograma360",
        "night_plan": ""
    }
    res = handler_get(input,True)
    
    vbeln_programado_1 = input_test_pedido_combinado[0]["Vbeln"]  #"0404363703"
    vbeln_programado_2 = input_test_pedido_combinado[1]["Vbeln"]  #"0404363704"
    #obenemos de la respuesta de sap todos los pedidos cuyos vehiculos esten asignados y chequeamos ESTADO, PEDIDO TIEMPOS ETC
    f = filter(lambda x: x["VEHICLE"].strip() != "", res["pedidos"])
    
    resultado_a_evaluar = list(f)
    
    dyna_result = get_dynamo(dyna_opt,[vbeln_programado_1,vbeln_programado_2])
    
    assert len(resultado_a_evaluar) == len(dyna_result)
    
    #print("resultado_a_evaluar")
    #print(resultado_a_evaluar)
    
    
    compare_keys = [{"nameInput":"VBELN","nameSap":"VBELN","type":"int"},  
                    {"nameInput":"VBTYP","nameSap":"VBTYP","type":"str"},
                    {"nameInput":"AUART","nameSap":"AUART","type":"str"},
                    {"nameInput":"VEHICLE","nameSap":"VEHICLE","type":"int"},
                    {"nameInput":"PRO_BEGDA","nameSap":"PRO_BEGDA","type":"date"},
                    {"nameInput":"PRO_BEGTI","nameSap":"PRO_BEGTI","type":"time"},
                    {"nameInput":"PRO_ENDDA","nameSap":"PRO_ENDDA","type":"date"},
                    {"nameInput":"PRO_ENDTI","nameSap":"PRO_ENDTI","type":"time"},
                    {"nameInput":"DURACION_GRUPO","nameSap":"DURACION_GRUPO","type":"time"},
                    {"nameInput":"AGRUPADOR","nameSap":"AGRUPADOR","type":"int"}
                    
                    ]
    for i in range(0,len(resultado_a_evaluar)):
        for key in compare_keys:
            if key["type"]=="int":
                assert int(resultado_a_evaluar[i][key["nameInput"]]) ==  int(dyna_result[i][key["nameSap"]])
            if key["type"]=="str":
                assert resultado_a_evaluar[i][key["nameInput"]] ==  dyna_result[i][key["nameSap"]]
            if key["type"]=="date":
                assert resultado_a_evaluar[i][key["nameInput"]].replace("-","") ==  dyna_result[i][key["nameSap"]]
            if key["type"]=="time":
                assert resultado_a_evaluar[i][key["nameInput"]].replace(":","") ==  dyna_result[i][key["nameSap"]] 
    #for result in dyna_result:
    
def test_3_combinado():
    #T3_GET_PLAN_COMBINADO
    #Condicion :
    #SAP CON UN PEDIDO
    #Pedidos opt DATA DE UN PEDIDO
    #pedidos usr DATA DE UN PEDIDO con timestamp mayor
    
    #Esperable:
    #Comparar Pedido sap vs Dynamo. Debe dominar el pedido de timestamp mayor (usR)
    
    prepare_simple()
    print("programando pedido combinado")
    programar(input_test_pedido_combinado,planta_test_qa,fecha_test_qa)
    print("escribiendo en tabla opt de dynamo")
    write_dynamo(dyna_opt,input_test_pedido_combinado_DYNAMO)
    print("escribiendo en tabla usr de dynamo")
    write_dynamo(dyna_usr,input_test_pedido_combinado_DYNAMO_usr)
    
    
    
    #variables de entorno para la funcion
    os.environ["SAP_URL"] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl"
    os.environ['SAP_URL_XTRAS'] = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
    os.environ['SAP_URL_MESSAGES'] = "http://coqabroker1.copec.cl:7812/programador/zws_pr_info_prog_sap?wsdl"
    
    os.environ['DYNA_PEDIDOS_USR'] = "Pedido_dev_usr"
    os.environ['DYNA_PEDIDOS_OPT'] = "Pedido_dev_opt"
    
    #handler_get de get plan
    input = {
        "codigo_planta": [
            "1208"
        ],
        "codigo_fecha": ["20200701"],
        "sap_user": "aelgueda",
        "sap_pwd": "Qprograma360",
        "night_plan": ""
    }
    res = handler_get(input,True)
    
    vbeln_programado_1 = input_test_pedido_combinado[0]["Vbeln"]  #"0404363703"
    vbeln_programado_2 = input_test_pedido_combinado[1]["Vbeln"]  #"0404363704"
    
    #obenemos de la respuesta de sap todos los pedidos cuyos vehiculos esten asignados y chequeamos ESTADO, PEDIDO TIEMPOS ETC
    f = filter(lambda x: x["VEHICLE"].strip() != "", res["pedidos"])
    
    resultado_a_evaluar = list(f)
    dyna_result = get_dynamo(dyna_usr,[vbeln_programado_1,vbeln_programado_2])
    
    assert len(resultado_a_evaluar) == len(dyna_result)
    
    compare_keys = [{"nameInput":"VBELN","nameSap":"VBELN","type":"int"},  
                    {"nameInput":"VBTYP","nameSap":"VBTYP","type":"str"},
                    {"nameInput":"AUART","nameSap":"AUART","type":"str"},
                    {"nameInput":"VEHICLE","nameSap":"VEHICLE","type":"int"},
                    {"nameInput":"PRO_BEGDA","nameSap":"PRO_BEGDA","type":"date"},
                    {"nameInput":"PRO_BEGTI","nameSap":"PRO_BEGTI","type":"time"},
                    {"nameInput":"PRO_ENDDA","nameSap":"PRO_ENDDA","type":"date"},
                    {"nameInput":"PRO_ENDTI","nameSap":"PRO_ENDTI","type":"time"},
                    {"nameInput":"DURACION_GRUPO","nameSap":"DURACION_GRUPO","type":"time"},
                    {"nameInput":"AGRUPADOR","nameSap":"AGRUPADOR","type":"int"}
                    
                    ]
    for i in range(0,len(resultado_a_evaluar)):
        for key in compare_keys:
            if key["type"]=="int":
                assert int(resultado_a_evaluar[i][key["nameInput"]]) ==  int(dyna_result[i][key["nameSap"]])
            if key["type"]=="str":
                assert resultado_a_evaluar[i][key["nameInput"]] ==  dyna_result[i][key["nameSap"]]
            if key["type"]=="date":
                assert resultado_a_evaluar[i][key["nameInput"]].replace("-","") ==  dyna_result[i][key["nameSap"]]
            if key["type"]=="time":
                assert resultado_a_evaluar[i][key["nameInput"]].replace(":","") ==  dyna_result[i][key["nameSap"]] 
                
            
if sys.argv[1] == "clean":
    prepare_simple()