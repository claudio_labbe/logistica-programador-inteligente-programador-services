from suds.client import Client
from suds import WebFault
from suds.transport.http import HttpAuthenticated
from suds.sudsobject import asdict
import logging
#import xmltodict
#para el lambda
from xmltodict import xmltodict
from json import loads, dumps
import time

class soapService:
    def to_dict(self,input_ordered_dict):
        return loads(dumps(input_ordered_dict))
    
    def build_response(self,item_json):   
        response = []
        #print(item_json["item"])
        
        for elem in item_json:
            if elem == "item":
                return self.build_response(item_json[elem])
            
            else:
                hash = {}
                for item in elem:
                    if type(elem[item]).__name__ == "dict":
                        hash[item] = self.build_response(elem[item])
                    else:
                        hash[item] = elem[item]
                response.append(hash)    
        return response     

    def recursive_asdict(self,d):
        """Convert Suds object into serializable format."""
        out = {}
        for k, v in asdict(d).items():
            if hasattr(v, '__keylist__'):
                out[k] = self.recursive_asdict(v)
            elif isinstance(v, list):
                out[k] = []
                for item in v:
                    if hasattr(item, '__keylist__'):
                        out[k].append(self.recursive_asdict(item))
                    else:
                        out[k].append(item)
            else:
                out[k] = v
        return out

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        #logging.getLogger('suds.client').setLevel(logging.DEBUG)
        #logging.getLogger('suds.transport').setLevel(logging.DEBUG)
        #logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)

    def client_info(self):
        print(self.client)
        
    def createClient(self,user,pwd,urlConn):
        t = HttpAuthenticated(username=user, password=pwd)
        self.client = Client(url=urlConn,transport=t)
    
    #entrega los mensajes del programador (usados para los combobox cuando das de baja pedidos o camiones)
    def get_combo_messages(self):
        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            response = self.client.service[0].ZBDP_INFO_PROGRAMACION()
            return self.recursive_asdict(response)
        except WebFault as e:
            print(e)
            return false
    
    
    def get_plan(self,fecha,plantCode): 
        #cliente tiene que tener la URL de get plan: zws_pr_obtiene_prog_sap
        #fecha yyyy-mm-dd
        #plantCode xxxx
        ItClienteFecha = self.client.factory.create('ItClienteFecha')

        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios.
            #print("LLAMANDO A SERVICIO SAP DE ZbdpObtieneProgramacion")
            #t1 = time.time()
            response = self.client.service[0].ZbdpObtieneProgramacion(fecha,2,plantCode,ItClienteFecha)
            #t2 = time.time()
            #print("llamada a sap en secs: " + str(t2-t1))
            return self.recursive_asdict(response)
        except WebFault as e:
            print(e)
            return false
    
    #entrega los comentarios dado un pedido
    def get_message(self,vbeln):
        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            response = self.client.service[0].ZBDP_EXTRAS_PEDIDOS(vbeln)
            return self.recursive_asdict(response)
        except WebFault as e:
            print(e)
            return false
    
    #entrega todos los pedidos como dados de baja, excepto los confirmados
    #se asocia a la funcionalidad LIMPIAR del programador
    def clear(self,fecha,plantCode): 
        #cliente tiene que tener la URL de get plan: zws_pr_obtiene_prog_sap
        #fecha yyyy-mm-dd
        #plantCode xxxx
        ItClienteFecha = self.client.factory.create('ItClienteFecha')

        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            parametro_dar_de_baja = 3
            response = self.client.service[0].ZbdpObtieneProgramacion(fecha,parametro_dar_de_baja,plantCode,ItClienteFecha)
            return self.recursive_asdict(response)
        except WebFault as e:
            print(e)
            return false
    
    def yyyymmddToDateSap(self,yyyymmdd):
        year = yyyymmdd[0:4]
        month = yyyymmdd[4:6]
        day = yyyymmdd[6:8]
        ret = year+"-"+month+"-"+day
        return ret
    
    def hhmmssToSap(self,hhmmss):
        #print("hhmmss")
        #print(hhmmss)
        #print(self.hhmmmmToHour(hhmmss))
        #print(int(self.hhmmmmToHour(hhmmss)))
        h =  self.hhmmmmToHour(hhmmss)
        m =  self.hhmmmmToMinute(hhmmss)
        s =  self.hhmmmmToSeconds(hhmmss)
        return h+":"+m+":"+s;
  
    def hhmmmmToHour(self,hhmmss):
        h = hhmmss[0:2]
        return h
  
    def hhmmmmToMinute(self,hhmmss):
        m = hhmmss[2:4]
        return m
  
    def hhmmmmToSeconds(self,hhmmss):
        s = hhmmss[4:6]
        return s
    
    def confirmCambioTurno(self,input):
        werks = input["items"]["Werks"]
        hora = "10:00:00" #deconfirm_truck_data["Hora"] #hora actual hh:mm:ss
        vehicle = input["items"]["Vehicle"]
        turnoFechaDesde = input["items"]["TurnoFechaDesde"]
        turnoHoraDesde = input["items"]["TurnoHoraDesde"]
        turnoFechaHasta = input["items"]["TurnoFechaHasta"]
        turnoHoraHasta = input["items"]["TurnoHoraHasta"]
        motivos = input["items"]["Motivos"]
        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            IDatum = input["fecha"]    #"2020-05-06"
            IWerks = input["codigo_planta"]    #"1208"
            
            #la lista de camiones. Esto no es tarea del programador, pero se podria hacer
            ItCambioCamion = self.client.factory.create('ZttdfAudCami')
            #la lista de pedidos
            ItPedidos = self.client.factory.create('ZprogTtConfirmarProg')
            
            item = self.client.factory.create('item')  
            data_truck = input["items"]
            
            obj = {}
            obj["Werks"] = werks
            obj["Hora"] = hora
            obj["Vehicle"] = vehicle
            obj["TurnoFechaDesde"] = turnoFechaDesde
            obj["TurnoHoraDesde"] = turnoHoraDesde
            obj["TurnoFechaHasta"] = turnoFechaHasta
            obj["TurnoHoraHasta"] = turnoHoraHasta
            obj["Motivos"] = motivos
            
            ItCambioCamion.item.append(obj)
            
        
            #debo agregar pedido vacio para hacer un confirm de camion. Si no lo hago, no me deja confirmar camion SAP
            item_pedido = self.client.factory.create('item')  
            
            ped = {}
            ped["Agrupador"] = " "
            ped["PosAgrupador"] = ""
            ped["Vbeln"] = " "
            ped["Vbtyp"] = " "
            ped["Auart"] = " "
            ped["Bsark"] = " "
            ped["Vehicle"] = " "
            ped["StartDate"] = ""
            ped["StartTime"] = ""
            ped["EndDate"] = ""
            ped["EndTime"] = ""
            ped["Duracion"] = ""
            ped["DuracionGrupo"] = ""
            ped["Volumen"] = " "
            
            ItPedidos.item.append(ped)
            
            print(ItPedidos)
            print(ItCambioCamion)
            
            response = self.client.service[0].ZprProgSapConfirmar(IDatum, IWerks,ItCambioCamion, ItPedidos)
            return response
        except WebFault as e:
            #print(e)
            return e
    
    
    def confirm(self,input):
        #try:
        #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
        IDatum = input["fecha"]    #"2020-05-06"
        IWerks = input["codigo_planta"]    #"1208"
        
        #la lista de camiones. Esto no es tarea del programador, pero se podria hacer
        ItCambioCamion = self.client.factory.create('ZttdfAudCami')
        #la lista de pedidos
        ItPedidos = self.client.factory.create('ZprogTtConfirmarProg')
        
        item = self.client.factory.create('item')  
        
        for pedido in input["items"]:
            ped = {}
            ped["Agrupador"] = pedido["AGRUPADOR"]
            ped["PosAgrupador"] = pedido["POS_AGRUPADOR"]
            ped["Vbeln"] = pedido["VBELN"]
            ped["Vbtyp"] = pedido["VBTYP"]
            ped["Auart"] = pedido["AUART"]
            ped["Bsark"] = pedido["BSARK"]
            ped["Vehicle"] = pedido["VEHICLE"]
            
            ped["StartDate"] = self.yyyymmddToDateSap(pedido["PRO_BEGDA"])
            ped["StartTime"] = self.hhmmssToSap(pedido["PRO_BEGTI"])
            ped["EndDate"] = self.yyyymmddToDateSap(pedido["PRO_ENDDA"])
            ped["EndTime"] = self.hhmmssToSap(pedido["PRO_ENDTI"])
            ped["Duracion"] = self.hhmmssToSap(pedido["DURACION"])
            if pedido["DURACION_GRUPO"].strip() != "":
                ped["DuracionGrupo"] = self.hhmmssToSap(pedido["DURACION_GRUPO"])
            
            ped["Volumen"] = ""#pedido["VOLUMEN"]
            #AGREGAMOS PARA LA CORRECCION DE CONFIRMAR
            print("AGREGAMOS nuevos parametros confirmar")
            print(pedido["TPO_CARGA"])
            #ped["Tpo_carga"] = pedido["TPO_CARGA"]
            #ped["Tpo_desca"] = pedido["TPO_DESCA"]
            #ped["Ruta"] = pedido["DURACION"] #recordar que duracion es tiempo ida y vuelta sin considerar los tiempos de carga y descarga.
            #ped["Tpo_combi"] = pedido["TPO_COMBI"]
            
            ItPedidos.item.append(ped)
        
        print(ItPedidos)
        
        print(self.client)
        print("AAAA")
        #print(self.client.wsdl.services[0].ports[0].types )
        #print(self.client.wsdl.types )
        print(ItPedidos )
        print("BBB")
        #print(self.client[0]["ports"])
        response = self.client.service[0].ZprProgSapConfirmar(IDatum, IWerks,ItCambioCamion, ItPedidos)
        print("AFTER RESPONSE")
        return response
        #except WebFault as e:
        #    #print(e)
        #    return e
    
    def deconfirm(self,input):
        #input is an array. 
        CtPedidos = self.client.factory.create('ZprogTtDesconfirmarProg')
        IFechaFin = self.client.factory.create('date10')
        IFechaIni = self.client.factory.create('date10')
        IHoraFin = self.client.factory.create('time')
        IHoraIni = self.client.factory.create('time')
        try:
            
            for pedido in  input["items"]:
                #pedido = input["items"]
            
                ped = {}
                ped["Vbeln"] = pedido["VBELN"]
                ped["Vbtyp"] = pedido["VBTYP"]
                ped["Motivo"] = pedido["MOTIVO"]
                ped["Subrc"] = pedido["SUBRC"]
                ped["Message"] = pedido["MESSAGE"]
                #ped["DuracionCalcula"] = pedido[""]
                ped["Tipo"] = pedido["TIPO"]
                ped["Observacion"] = pedido["OBSERVACION"]
                
                CtPedidos.item.append(ped)
            print(CtPedidos)
            #ZprProgSapDesconfirmar(ZprogTtDesconfirmarProg CtPedidos, date10 IFechaFin, date10 IFechaIni, time IHoraFin, time IHoraIni, )
            response = self.client.service[0].ZprProgSapDesconfirmar(CtPedidos, IFechaFin, IFechaIni, IHoraFin, IHoraIni )
            #print("SERVICIO DECONFR RESPONSE")
            return response
            
        except WebFault as e:
            print(e)
            return e
            
            
            
    def save_plan(self,input):
        #input = {"items": event["plan"],"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
        #ZprProgSapGrabar(ns0:date10 IDatum, ns0:char4 IWerks, ZttdfAudCami ItCambioCamion, ZprogTtConfirmarProg ItPedidos, )
        
        try:
            #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios. 
            IDatum = input["fecha"]    #"2020-05-06"
            IWerks = input["codigo_planta"]    #"1208"
            
            #la lista de camiones. Esto no es tarea del programador, pero se podria hacer
            ItCambioCamion = self.client.factory.create('ZttdfAudCami')
            #la lista de pedidos
            ItPedidos = self.client.factory.create('ZprogTtConfirmarProg')
            
            #item = self.client.factory.create('item')  
            
            for pedido in input["items"]:
                #item = self.client.factory.create('item')    
                #FORMATEAR FECHAS QUE VIENEN DE PEDIDO YYYYDDMM TO - NOTATION AND FOR HOUR TOO
                #if pedido["AGRUPADOR"].strip() != "" :
                ped = {}
                ped["Agrupador"] = pedido["AGRUPADOR"]
                ped["PosAgrupador"] = pedido["POS_AGRUPADOR"]
                ped["Vbeln"] = pedido["VBELN"]
                ped["Vbtyp"] = pedido["VBTYP"]
                ped["Auart"] = pedido["AUART"]
                ped["Bsark"] = pedido["BSARK"]
                ped["Vehicle"] = pedido["VEHICLE"]
                ped["StartDate"] = pedido["PRO_BEGDA"]
                ped["StartTime"] = pedido["PRO_BEGTI"]
                ped["EndDate"] = pedido["PRO_ENDDA"]
                ped["EndTime"] = pedido["PRO_ENDTI"]
                ped["Duracion"] = pedido["DURACION"]
                ped["DuracionGrupo"] = pedido["DURACION_GRUPO"]
                #ped["Volumen"] = pedido["VOLUMEN"]
                
        
                
                ItPedidos.item.append(ped)
            
            print("PEDIDOS A GUARDAR")
            print(ItPedidos)
            
            response = self.client.service[0].ZprProgSapGrabar(IDatum, IWerks,ItCambioCamion, ItPedidos)
            return response
        except WebFault as e:
            #print(e)
            return e
    
    
    
    def login(self,user,pwd,url):
        try:
            t = HttpAuthenticated(username=user, password=pwd)
            self.client = Client(url=url,transport=t,retxml=True)
            response = self.client.service[0].ZprProgSapAutoriza()
            string_response_xml = response.decode("utf-8")
            json=self.to_dict(xmltodict.parse(string_response_xml))
            
            print("RESPONSE")
            print(json)
            
            if "soapenv:Envelope" in json:
                if json["soapenv:Envelope"]["soapenv:Body"]["NS1:ZprProgSapAutorizaResponse"]["ERc"] == "1":
                    return {"retCode":401, "message":json["soapenv:Envelope"]["soapenv:Body"]["NS1:ZprProgSapAutorizaResponse"]["EMessage"]}
            try:
                #    (json["soap-env:Envelope"]["soap-env:Body"]["soap-env:Fault"]["faultstring"]["#text"])
                msg = json["soap-env:Envelope"]["soap-env:Body"]["soap-env:Fault"]["faultstring"]["#text"]
                if msg.strip() != "":
                    return {"retCode":401, "message":"El usuario no tiene acceso al programador."}
            except:
                pass
            
            base = json["soap-env:Envelope"]["soap-env:Body"]["n0:ZprProgSapAutorizaResponse"]
            """
            message = base["EMessage"]
            erc = base["ERc"]
            autoriza = base["EtAutoriza"]   
            counc = base["EtCounc"]
            groupname = base["EtGroupname"]
            """
            autoriza = base["EtAutoriza"]  
            
            return {"retCode":200, "message":"ok", "data": self.build_response(autoriza)}
            #return response
        except Exception as e:
            print(e)
            return {}
            
    def lista_pedidos(self,input):
        #ZBDP_LISTA_PEDIDOS(date10 I_FECHA_FIN, date10 I_FECHA_INI, time I_HORA_FIN, time I_HORA_INI, char10 I_VEHICULO, char4 I_WERKS, )
        I_FECHA_FIN=input["fecha_fin"]
        I_FECHA_INI=input["fecha_ini"]
        I_HORA_FIN=input["hora_fin"]
        I_HORA_INI=input["hora_ini"]
        I_VEHICULO=input["truck"]
        I_WERKS=input["codigo_planta"]
        response = self.client.service[0].ZBDP_LISTA_PEDIDOS(I_FECHA_FIN, I_FECHA_INI, I_HORA_FIN, I_HORA_INI, I_VEHICULO, I_WERKS)
        return self.recursive_asdict(response)

