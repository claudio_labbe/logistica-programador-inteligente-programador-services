from lib.truck_structure_generator import TruckStructureGenerator
from lib.orders_structure_generator import OrdersStructureGenerator
from lib.orders_structure_store import storeOrdersDynamo
from lib.soap_service import soapService
import time
import math

import os

from datetime import datetime
from datetime import timedelta

def lambda_handler(event, context):
    #event = {"plan": [{pedido-ith formato programador para confirmar},{}], "codigo_planta":["1208"], "fecha":...}
    
    sap_user = event["sap_user"]
    sap_pwd = event["sap_pwd"]
    sap_url = os.environ['SAP_URL']
    #"http://coqabroker1.copec.cl:7812/programador/zpr_prog_sap_grabar?wsdl
    
    dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
    dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
    
    #event = request.json
    input = {"items": event["plan"],"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
    table_dynamo_pedidos_user = dyna_pedidos_usr
    #poner la tabla en ambiente...
    
    #1 GUARDAMOS EN SAP
    service_sap = soapService()
    service_sap.createClient(sap_user,sap_pwd,sap_url)
    sap_response = service_sap.save_plan(input)
    
    sap_response_code = sap_response["ERc"]
    sap_response_message = sap_response["EMessage"]
    
    """
    table_dynamo_pedidos_user = "Pedido_dev_usr"
    service_dynamo = storeOrdersDynamo(table_dynamo_pedidos_user)
    dynamo_store_succes = service_dynamo.store(input)
    """
    #DynamoService(pedidos_opt_table)

    #generando timestamp para creación y ttl
    current_time =  datetime.utcnow()
    if(event["timestamp"] != -1 and event["timestamp"] != None):
        timestamp = str(event["timestamp"])
        print("UTILIZANDO TIMESTAMP DESDE FRONT EN GUARDAR")
    else:
        print("UTILIZANDO TIMESTAMP GENERADO EN LAMBDA")
        timestamp = str(int(float(current_time.timestamp())))

    print(timestamp)

    ttl = int(float((current_time + timedelta(days=5)).timestamp()))
    
    #2 Si guarda en sap, vemos store en dynamo
    try:
        
        if sap_response.ERc != 0:
            response = {}
            response["retCode"] = "200";
            response["ERc"] = sap_response_code
            response["EMessage"] = sap_response_message
            return response;
    
        #si guarda ok en sap
        if sap_response.ERc == 0:
            service_dynamo = storeOrdersDynamo(table_dynamo_pedidos_user)
            #print("saving input in dynamo")
            #print(input)
            dynamo_store_succes = service_dynamo.store(input,timestamp,ttl)
            print("save, dynamo_store_succes:" + str(dynamo_store_succes))
            #en caso que falle, intentamos un par de veces
            tries = 3
            count = 0
            while (dynamo_store_succes != True) and count != tries :
                dynamo_store_succes = service_dynamo.store(input,timestamp,ttl)
                count = count + 1
                time.sleep(0.5)
            
            #si no guarda en dynamo pero si en sap, igual retornamos ok
            if dynamo_store_succes != True:
                response = {"retCode":"200", "message":"Planificacion guardada en SAP, pero no pudo ser guardada en DynamoDB"}
                response["ERc"] = sap_response_code
                response["EMessage"] = sap_response_message
                return response
            
            response = {"retCode":"200","message":"Planificacion Guardada de manera exitosa"}
            response["ERc"] = sap_response_code
            response["EMessage"] = sap_response_message
            return response
        #si no se guardo en sap, no guardo en dyna y retorno error
        
        #return {"retCode":"500", "message":"no se pudo guardar plan en Sap.1 "}
    except Exception as e:
        print(e)
        return {"retCode":"500", "message":"no se pudo guardar plan en Sap.2 " + e}