from .ms_dynamo_services import DynamoService
import datetime
#from datetime import datetime

class TruckStructureGenerator:

    def __init__(self): 
        pass

    #detalleType de la forma 24=6-5-4-8/9 por ejemplo o 24=6-5-4-8
    def get_capacity(self,detalleType):
        detalles = detalleType.split("=") #separo detalle de la suma
        
        compartimientos = detalles[1].strip().split("-")
        return compartimientos
    
    def get_countLiter(self,value):
        if value == "C":
            return True
        return False

    def format_date(self,yyyymmdd):
        ret = yyyymmdd[6:8] + "-" + yyyymmdd[4:6] + "-" +  yyyymmdd[:4]
        return ret
        #datetime_str = str(yyyymmdd[:4] + "-" + yyyymmdd[4:6] + "-" + yyyymmdd[6:8])
        #datetime_object = datetime.strptime(datetime_str, '%Y-%m-%d')
        #return datetime.strftime(datetime_object, '%d-%m-%Y')

    def soap_time_to_hhmmss(self,soap_time):
        return soap_time.replace(":","")
    
    def soap_date_to_yyyymmdd(self,soap_date):
        return soap_date.replace("-","")

    def get_time(self,time,unit):
        if unit == "h":
            return int(time[:2])
        if unit == "m":
            return int(time[2:4])
        if unit == "s":
            return int(time[4:6])

    def get_date_from_shift(self,shift):
        
        year = int(shift["dayInit"].split("-")[2])
        month = int(shift["dayInit"].split("-")[1])
        day = int(shift["dayInit"].split("-")[0])
        
        
        hour=int(shift["hourInit"])
        minutes=int(shift["minutesInit"])
        seconds=int(shift["secondsInit"])
        return datetime.datetime(year,month,day,hour=hour,minute=minutes,second=seconds).timestamp() 
    
    def append_shift(self,new_shift,shift_array):
        shift_array.append(new_shift)
    
    
    def order_shifts(self,shift_array):
        self.quickSort(shift_array,0,len(shift_array)-1)
        
    def quickSort(self,arr,low,high):    
        if low < high:
            pi = self.partition(arr,low,high)
            self.quickSort(arr,low,pi-1)
            self.quickSort(arr,pi+1,high)
            
    def partition(self,arr,low,high):
        i = low -1
        pivot = arr[high]
        
        for j in range(low,high):
            if self.get_date_from_shift(arr[j]) <= self.get_date_from_shift(pivot):
                i = i + 1
                arr[i],arr[j] = arr[j],arr[i]
                
        arr[i+1],arr[high] = arr[high],arr[i+1]
        return i + 1
        
        
    
    #takes a yyyymmdd and return a mmdd_N
    def from_date_to_fecha_code(self,yyyymmdd):
        ret = yyyymmdd[0][4:6] + yyyymmdd[0][6:8] + "_N"
        return ret
    
        
    def generate_structure(self,input_search):
        soap_vehiculos_cab = input_search["soap_vehicles_cab"]
        print("INPUT SEARCH")
        print(input_search)
        #service_vehiculos_comp = DynamoService("Vehiculos_det")

        #input_search["fecha_plan"] viene en formato YYYYMMDD
        #Criterio 1 obtener camiones cuyos turnos partan en START_DATE y terminen en END_DATE para la fecha_plan dada. 
        #input = {"START_DATE": input_search["fecha_plan"],"END_DATE": input_search["fecha_plan"],"CODIGO_PLANTA": input_search["codigo_planta"]}
        
        #Criterio 2. Pasar fecha_plan a formato mmdd (que deberia cambiar pronto porque no  soporta agno)  y buscar por FECHA_PLAN=MMDD_N
        #input = {"FECHA_PLAN": self.from_date_to_fecha_code(input_search["fecha_plan"]),"CODIGO_PLANTA": input_search["codigo_planta"]}
        #input = {"FECHA_PLAN": {"item":self.from_date_to_fecha_code(input_search["fecha_plan"]),"condition":"eq"},"CODIGO_PLANTA": {"item":input_search["codigo_planta"],"condition":"eq"}}
        
        #vehiculos_cab = service_vehiculos_cab.scan_items(input)
        #vehiculos_comp = service_vehiculos_comp.scan_items(input)
        
        array_trucks = []
        trucks_by_id = {}
        
        if type(soap_vehiculos_cab).__name__ == "str":
            if len(soap_vehiculos_cab.strip())==0:
                return array_trucks
        
        
        if len(soap_vehiculos_cab)==0:
            return array_trucks
        
        print('soap_vehiculos_cab["item"]')
        print(soap_vehiculos_cab["item"])
        print(type(soap_vehiculos_cab["item"]).__name__)
        
        
        if type(soap_vehiculos_cab["item"]).__name__ == "dict":
            print("UN SOLO CAMION")
            #Viene un solo camion
            vcab = soap_vehiculos_cab["item"]
            if not vcab["Vehicle"] in trucks_by_id:
                trucks_by_id[vcab["Vehicle"]] = {}            
            
            print("t1")
            current_vehicle =  trucks_by_id[vcab["Vehicle"]]
            current_vehicle["truckId"] = vcab["Vehicle"]
            current_vehicle["capacity"] = self.get_capacity(vcab["Detalle"])
            current_vehicle["countLiter"] = self.get_countLiter(vcab["CuentaLitros"])
            current_vehicle["fuelTypes"] = vcab["Grouptext"]
            current_vehicle["fuelTypeCode"] = vcab["Groupname"]
            if not "shifts" in current_vehicle:
                current_vehicle["shifts"] = []
            #current_vehicle["shifts"]
            new_shift={}        
            new_shift["name"]="Txxx" #hardcoded turn #determinar como poblarlo
            new_shift["dayInit"] = self.format_date(self.soap_date_to_yyyymmdd(vcab["StartDate"]))
            new_shift["hourInit"] = self.get_time(self.soap_time_to_hhmmss(vcab["StartTime"]),"h")
            new_shift["minutesInit"] = self.get_time(self.soap_time_to_hhmmss(vcab["StartTime"]),"m")
            new_shift["secondsInit"] = self.get_time(self.soap_time_to_hhmmss(vcab["StartTime"]),"s")         
            new_shift["dayEnd"] = self.format_date(self.soap_date_to_yyyymmdd(vcab["EndDate"]))
            new_shift["hourEnd"] = self.get_time(self.soap_time_to_hhmmss(vcab["EndTime"]),"h")
            new_shift["minutesEnd"] = self.get_time(self.soap_time_to_hhmmss(vcab["EndTime"]),"m")
            new_shift["secondsEnd"] = self.get_time(self.soap_time_to_hhmmss(vcab["EndTime"]),"s") 
            new_shift["active"] = True
            self.append_shift(new_shift,current_vehicle["shifts"])
            #self.append_shift(new_shift,current_vehicle["shifts"],vcab["VEHICLE"])
            self.order_shifts(current_vehicle["shifts"])
            print("T2")
            for t in trucks_by_id:
                array_trucks.append(trucks_by_id[t])
            print("t3")    
        
        elif type(soap_vehiculos_cab["item"]).__name__ == "list":
            print("MAS DE UN CAMION")
            #vienen mas camiones
            #we use now values from sap format
            for vcab in soap_vehiculos_cab["item"]:
                #print("VCAB")
                #print(vcab)
                if not vcab["Vehicle"] in trucks_by_id:
                    trucks_by_id[vcab["Vehicle"]] = {}            
                current_vehicle =  trucks_by_id[vcab["Vehicle"]]
                current_vehicle["truckId"] = vcab["Vehicle"]
                current_vehicle["capacity"] = self.get_capacity(vcab["Detalle"])
                current_vehicle["countLiter"] = self.get_countLiter(vcab["CuentaLitros"])
                current_vehicle["fuelTypes"] = vcab["Grouptext"]
                current_vehicle["fuelTypeCode"] = vcab["Groupname"]
                if not "shifts" in current_vehicle:
                    current_vehicle["shifts"] = []
                #current_vehicle["shifts"]
                new_shift={}        
                new_shift["name"]="Txxx" #hardcoded turn #determinar como poblarlo
                new_shift["dayInit"] = self.format_date(self.soap_date_to_yyyymmdd(vcab["StartDate"]))
                new_shift["hourInit"] = self.get_time(self.soap_time_to_hhmmss(vcab["StartTime"]),"h")
                new_shift["minutesInit"] = self.get_time(self.soap_time_to_hhmmss(vcab["StartTime"]),"m")
                new_shift["secondsInit"] = self.get_time(self.soap_time_to_hhmmss(vcab["StartTime"]),"s")         
                new_shift["dayEnd"] = self.format_date(self.soap_date_to_yyyymmdd(vcab["EndDate"]))
                new_shift["hourEnd"] = self.get_time(self.soap_time_to_hhmmss(vcab["EndTime"]),"h")
                new_shift["minutesEnd"] = self.get_time(self.soap_time_to_hhmmss(vcab["EndTime"]),"m")
                new_shift["secondsEnd"] = self.get_time(self.soap_time_to_hhmmss(vcab["EndTime"]),"s") 
                new_shift["active"] = True
                self.append_shift(new_shift,current_vehicle["shifts"])
                #self.append_shift(new_shift,current_vehicle["shifts"],vcab["VEHICLE"])
                self.order_shifts(current_vehicle["shifts"])
                #current_vehicle["shifts"] 
                #print(vcab["VEHICLE"])
            #print(trucks_by_id)
            for t in trucks_by_id:
                array_trucks.append(trucks_by_id[t])
            
        else:
            #algo paso.. retornamos vacio
            pass
        
        
            
        print(array_trucks)
        return array_trucks

    def generate_cab_structure_optimizer(self, input_search):
        soap_vehiculos_cab = input_search["soap_vehicles_cab"]
        
        array_trucks = []
        
        if type(soap_vehiculos_cab).__name__ == "str":
            if len(soap_vehiculos_cab.strip())==0:
                return array_trucks

        if len(soap_vehiculos_cab)==0:
            return array_trucks

        if type(soap_vehiculos_cab["item"]).__name__ == "list":
            #Si vienen varios pedidos
            for vcab in soap_vehiculos_cab["item"]:
                truck = {}
                truck["VEHICLE"] = vcab["Vehicle"]
                truck["DETALLE"] = vcab["Detalle"]
                truck["CUENTA_LITROS"] = vcab["CuentaLitros"]
                truck["GROUPTEXT"] = vcab["Grouptext"]
                truck["GROUPNAME"] = vcab["Groupname"]
                truck["START_DATE"] = vcab["StartDate"]
                truck["START_TIME"] = vcab["StartTime"]
                truck["END_DATE"] = vcab["EndDate"]
                truck["END_TIME"] = vcab["EndTime"]
                truck["ESPECIAL"] = vcab["Especial"]
                truck["CANT_COMPAR"] = vcab["CantCompar"]
                array_trucks.append(truck)
            
        elif type(soap_vehiculos_cab["item"]).__name__ == "dict":
            #si viene un solo pedido
            vcab = soap_vehiculos_cab["item"]
            truck = {}
            truck["VEHICLE"] = vcab["Vehicle"]
            truck["DETALLE"] = vcab["Detalle"]
            truck["CUENTA_LITROS"] = vcab["CuentaLitros"]
            truck["GROUPTEXT"] = vcab["Grouptext"]
            truck["GROUPNAME"] = vcab["Groupname"]
            truck["START_DATE"] = vcab["StartDate"]
            truck["START_TIME"] = vcab["StartTime"]
            truck["END_DATE"] = vcab["EndDate"]
            truck["END_TIME"] = vcab["EndTime"]
            truck["ESPECIAL"] = vcab["Especial"]
            truck["CANT_COMPAR"] = vcab["CantCompar"]
            array_trucks.append(truck)
            
        else:
            #si no viene ni dict ni arr..puede ser que no venga nada
            pass
        
    
        

        return array_trucks

    def generate_det_structure_optimizer(self, input_search):
        #try:
        soap_vehiculos_det = input_search["soap_vehicles_det"]
        
        array_trucks = []
        
        if type(soap_vehiculos_det).__name__ == "str":
            if len(soap_vehiculos_det.strip())==0:
                return array_trucks
        
        if len(soap_vehiculos_det)==0:
            return array_trucks
        
        vdetArr = []
        if type(soap_vehiculos_det["item"]).__name__ == "dict":
            #un solo vehicle
            vdetArr.append(soap_vehiculos_det["item"])
            
        if type(soap_vehiculos_det["item"]).__name__ == "list":
            #mas de uno...
            vdetArr = soap_vehiculos_det["item"]
        else:
            #??
            pass
        
         
        
        #for vdet in soap_vehiculos_det["item"]:
        for vdet in vdetArr:
            truck = {}
            truck["VEHICLE"] = vdet["Vehicle"]
            truck["GROUPNAME"] = vdet["Groupname"]
            truck["CANT_COMPAR"] = vdet["CantCompar"]
            truck["CMP_MAXVOL"] = vdet["CmpMaxvol"]
            truck["CMP_MINVOL"] = vdet["CmpMinvol"]
            truck["DETALLE_COMPARTIMIENTO"] = []
            
            detalle_comp_arr = []
            if type(vdet["ComNumberDet"]["item"]).__name__ == "dict":
                detalle_comp_arr.append(vdet["ComNumberDet"]["item"])
            elif type(vdet["ComNumberDet"]["item"]).__name__ == "list":
                detalle_comp_arr = vdet["ComNumberDet"]["item"]
            
            for detalle_compartimiento in detalle_comp_arr:
                det = {}

                # det["CHA_DATE"] =
                # det["CHA_NAME"] =
                # det["CLIENT"] =
                det["CMP_MAXVOL"] = detalle_compartimiento["CmpMaxvol"]
                det["CMP_MINVOL"] = detalle_compartimiento["CmpMinvol"]
                # det["COM_IDTEXT"] =
                det["COM_NUMBER"] = detalle_compartimiento["ComNumber"]
                # det["CRE_DATE"] =
                # det["CRE_NAME"] =
                # det["DOUB_HULL"] =
                # det["GROUPNAME"] =
                # det["LOAD_SEQ"] =
                # det["SEQ_NMBR"] =
                det["TU_NUMBER"] = detalle_compartimiento["TuNumber"]
                truck["DETALLE_COMPARTIMIENTO"].append(det)

            array_trucks.append(truck)

        return array_trucks
        #except Exception as e:
        #    print("generate_det_structure_optimizer ERROR ============>")
        #    print(e)
        #    return []
