from lib.soap_service_zeep import soapServiceZeep
import time
import math
import boto3
import json
import zlib
import base64

import os

def lambda_handler(event, context):
    try:
        #print("EVENT")
        #print(event)
        message_id = event["message_id"]
        sap_user = event["body"]["sap_user"]
        sap_pwd = event["body"]["sap_pwd"]
        sqs_table = os.environ['SQS_DYNAMO_TABLE']
        sap_url_all_mesasges = os.environ['SAP_URL_ALL_MESSAGE']
        
        date = event["body"]['date']#yyyy-mm-dd
        plant_code = event["body"]['plant_code']
        
        ret_list = []
        ###
        soap_service_comentarios_zeep = soapServiceZeep()
        soap_service_comentarios_zeep.createClient(sap_user,sap_pwd,sap_url_all_mesasges)
        
        comentarios = ""
        messages = soap_service_comentarios_zeep.get_all_message(plant_code,date)
        
        if(messages["TEXTOS"]==None or messages["TEXTOS"]==""):
            pass
           
        else:
            comentario = ""
            if type(messages["TEXTOS"]["item"]).__name__ == "dict":
                ret_list.append(messages["TEXTOS"]["item"])
                
            elif type(messages["TEXTOS"]["item"]).__name__ == "list":
                ret_list = messages["TEXTOS"]["item"]
                    
        #compress response
        #byteArr = bytes(json.dumps(response),'utf-8')
        byteArr = bytes(json.dumps(ret_list),'utf-8')
        #byteArr = bytes(ret_list),'utf-8')
        compressed = zlib.compress(byteArr)
        
        
        s3_bucket = os.environ['S3_SQS_ROOT']
        s3_key = "sqs_stored/" +  message_id
        s3_path = s3_bucket + '/' + s3_key
        
        s3 = boto3.resource('s3')
        object = s3.Object(s3_bucket, s3_key)
        object.put(Body=compressed)
        
        #store response in DB
        #item = {"message_id":message_id, "message_body":response}
        usuario = event["body"]["sap_user"]  #  request["sap_user"]
        planta = event["body"]["plant_code"]   #   request["codigo_planta"][0]
        timestamp = int(time.time())
        
        item = {"message_id":message_id, "s3_bucket":s3_bucket, "s3_key":s3_key, "s3_reference_body":s3_path ,"function":"copec_api_planificador_get_all_messages_sqs_sap", "user":usuario, "planta":planta, "timestamp":timestamp}
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(sqs_table)
        
        db_response = table.put_item(
            Item = item
        )
        return db_response
        
        
    except Exception as e:    
        return {"retCode":"500", "message": e}