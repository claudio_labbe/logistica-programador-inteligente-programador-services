#from requests import requests
import boto3
import time
import math
import json
import zlib
import base64

import os

import requests


import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#from botocore.vendored import requests

def lambda_handler(event, context):
    logger.info('## EVENT')
    logger.info(event)
    try:
        url = os.environ['API_LB']

        message_id = event["message_id"]
        data = event["body"]
        data["sqs_msg_id"] = message_id
        data["bucket"] = os.environ['S3_SQS_ROOT']
        #data["pedidos"] = []
        logger.info('## MessageId')
        logger.info(message_id)
        
        logger.info('## Data Request')
        logger.info(data)
        headers = { "content-type":"application/json"}
        x = requests.post(url, json = data, headers = headers)
        logger.info('## Se realizo llamada a API FLASK')
        
    except Exception as e:
        
        sqs_table = os.environ['SQS_DYNAMO_TABLE']
        
        item = {"message_id":message_id,  "error":str(e) , "retCode":500 , "msg":str(e)} 
        
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(sqs_table)
        db_response = table.put_item(
            Item = item
        )