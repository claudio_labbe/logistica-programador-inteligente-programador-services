from lib.soap_service import soapService
import time
import math

import os

def lambda_handler(event, context):
    try:
        #event = {"codigo_fecha":["20200506"], "codigo_planta":["1208"]}
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        
        #sap_url = "http://coqabroker1.copec.cl:7812/programador/zws_bdp_lista_pedidos?wsdl"
        sap_url = os.environ['SAP_URL']
        dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
        dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
        #dyna_pedidos_usr =  "Pedido_dev_usr" #"Pedidos_Usuario"
        #dyna_pedidos_opt =  "Pedido_dev_opt" #"Pedidos_Optimizador"
        
        input = {"truck":event["truck"], "fecha_ini":event["fecha_ini"], "fecha_fin":event["fecha_fin"], "hora_ini":event["hora_ini"], "hora_fin":event["hora_fin"], "codigo_planta":event["codigo_planta"]  }
        
        soap_service = soapService()
        
        #NO HARDCODEAR USER PASS. PASARLO cOMO PARAMETRO
        soap_service.createClient(sap_user,sap_pwd,sap_url)
        response = soap_service.lista_pedidos(input)
        return {"retCode":"200", "message": "ok", "data":response}
    except Exception as e:
        print(e)
        return {"retCode":"500", "message": e}