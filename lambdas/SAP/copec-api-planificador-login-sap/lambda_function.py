from lib.truck_structure_generator import TruckStructureGenerator
from lib.orders_structure_generator import OrdersStructureGenerator
from lib.ms_dynamo_services import DynamoService
from lib.soap_service import soapService
import time
import math
import json


import requests
#from requests import requests
#from requests.urllib3.request import requests

import os

def lambda_handler(event, context):

    user = event["user"]
    pwd = event["pwd"]
    sap_url = os.environ['SAP_URL']
    
    service_sap = soapService()
    response = service_sap.login(user,pwd,sap_url)
    
    #antes de obtener el auth.. llamo a dyna 
    table_plant_access = os.environ['DYNA_ACCESO_PLANTAS']
    service_dynamo = DynamoService(table_plant_access)
    dynamo_response = service_dynamo.get_item({"rule":1})
    response["enablePlantRules"] = dynamo_response["row"]["enablePlantRules"]
    response["whitelist"] = dynamo_response["row"]["whitelist"]
    
    
    #add auth 0...
    data = {"client_id":os.environ["cid"],"client_secret":os.environ["cse"],"audience":os.environ["deploy_url_api_gw"],"grant_type":"client_credentials"}

    response_a0 = requests.post(os.environ["auth0_request_url_proxy"], json=data)

    res = response_a0._content.decode('utf8')
    y = json.loads(res)
    response["bearer"] = y['access_token']
    
    #response["bearer"] = "thetoken"
    return response