from lib.soap_service_zeep import soapServiceZeep
import time
import math

import os

def lambda_handler(event, context):
    try:
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        
        sap_url_xtras = os.environ['SAP_URL_XTRAS']
        #sap_url_xtras = 'http://10.1.60.41:7812/programador/zws_pr_extras_prog_sap?wsdl'
            
        #lista_id_pedidos debe ser un arr con jsons, en el que venga VBELN y le agregamos el campo COMENTARIOS y lo retornamos al front
        lista_pedidos = event['lista_pedidos']
        ret_list = {}
        ###
        soap_service_comentarios_zeep = soapServiceZeep()
        soap_service_comentarios_zeep.createClient(sap_user,sap_pwd,sap_url_xtras)
        
        
        for p in lista_pedidos:
            messages = soap_service_comentarios_zeep.get_message(p["VBELN"])
            if(messages["T_TEXTOS"]==None or messages["T_TEXTOS"]==""):
                p["COMENTARIOS"] = ""
                ret_list[p["VBELN"]] = p["COMENTARIOS"]
            else:
                comentario = ""
                if type(messages["T_TEXTOS"]["item"]).__name__ == "dict":
                    textLine = messages["T_TEXTOS"]["item"]
                    separador = " "
                    if textLine["TDFORMAT"] == "*" and textLine["TDFORMAT"] != None:
                        separador = "\n"
                    nextToken=""
                    if textLine["TDLINE"] != None:
                            nextToken = textLine["TDLINE"]
                    comentario = comentario + separador + nextToken
                    
                elif type(messages["T_TEXTOS"]["item"]).__name__ == "list":
                    for textLine in messages["T_TEXTOS"]["item"]:
                        separador = " "
                        if textLine["TDFORMAT"] == "*" and textLine["TDFORMAT"] != None:
                            separador = "\n"
                        nextToken=""
                        if textLine["TDLINE"] != None:
                            nextToken = textLine["TDLINE"]
                        comentario = comentario + separador + nextToken
                p["COMENTARIOS"] = comentario
                ret_list[p["VBELN"]] = p["COMENTARIOS"]
                
        #print(lista_pedidos)
        return {"retCode":"200", "data":ret_list}    
    except Exception as e:    
        return {"retCode":"500", "message": e}