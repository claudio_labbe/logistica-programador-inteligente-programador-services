from lib.soap_service_zeep import soapServiceZeep
import time
import math
import boto3
import json
import zlib
import base64

import os

def lambda_handler(event, context):
    try:
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        
        sap_url_xtras = os.environ['SAP_URL_XTRAS']
        vbeln = event['vbeln']
        ret_list = {}
        ###
        soap_service_comentarios_zeep = soapServiceZeep()
        soap_service_comentarios_zeep.createClient(sap_user,sap_pwd,sap_url_xtras)
        
        comentarios = ""
        messages = soap_service_comentarios_zeep.get_message(vbeln)
        if(messages["T_TEXTOS"]==None or messages["T_TEXTOS"]==""):
            comentarios = ""
            ret_list[vbeln] = ""#comentarios
        else:
            comentario = ""
            if type(messages["T_TEXTOS"]["item"]).__name__ == "dict":
                textLine = messages["T_TEXTOS"]["item"]
                separador = " "
                if textLine["TDFORMAT"] == "*" and textLine["TDFORMAT"] != None:
                    separador = "\n"
                nextToken=""
                if textLine["TDLINE"] != None:
                        nextToken = textLine["TDLINE"]
                comentario = comentario + separador + nextToken
                
            elif type(messages["T_TEXTOS"]["item"]).__name__ == "list":
                for textLine in messages["T_TEXTOS"]["item"]:
                    separador = " "
                    if textLine["TDFORMAT"] == "*" and textLine["TDFORMAT"] != None:
                        separador = "\n"
                    nextToken=""
                    if textLine["TDLINE"] != None:
                        nextToken = textLine["TDLINE"]
                    comentario = comentario + separador + nextToken
            comentarios = comentario
            ret_list[vbeln] = comentarios
                
        #print(lista_pedidos)
        return {"retCode":"200", "data":ret_list}    
    except Exception as e:    
        return {"retCode":"500", "message": e}