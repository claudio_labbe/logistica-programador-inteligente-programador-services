import boto3
import json
import os
import time

def lambda_handler(event, context):
   
    sqs = boto3.client('sqs')
    queue_url = os.environ['SQS_URL']

    lambda_start = time.time()
    print("RECEIVER START AT TIMESTAMP: "+str(lambda_start))
    
    #store request in s3
    s3_bucket_output = os.environ['S3_SQS_ROOT']
    ts = str(int(time.time()))
    s3_key_output = "sqs_data_to_dispatcher/"+ts+"-data-"+event["function"]
    
    s3_client = boto3.client('s3')
    
    body = {
      "function":event["function"],
      "s3_bucket": s3_bucket_output,
      "s3_key":s3_key_output
    }
    
    str_msg = json.dumps(event)
    
    s3_client.put_object(Body=str_msg, Bucket=s3_bucket_output, Key=s3_key_output)
    
    msg_body = json.dumps(body)
    
    print("msg_body type")
    print(type(msg_body))
    
    # Send message to SQS queue
    response = sqs.send_message(
        QueueUrl=queue_url,
        DelaySeconds=10,
        MessageBody=(msg_body)
    )
        
    lambda_end = time.time()
    print("RECEIVER END AT TIMESTAMP: "+str(lambda_end))

    return {"msg_queue_id":response['MessageId']}


    