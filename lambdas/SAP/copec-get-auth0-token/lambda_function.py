import json
import os

#fix for aws botocore deprecated function
#from requests import requests
from botocore.vendored import requests

def lambda_handler(event, context):
    data = {"client_id":os.environ["cid"],"client_secret":os.environ["cse"],"audience":os.environ["deploy_url_api_gw"],"grant_type":"client_credentials"}
    response = {}
    response_a0 = requests.post(os.environ["auth0_request_url_proxy"], json=data)

    res = response_a0._content.decode('utf8')
    y = json.loads(res)
    response["bearer"] = y['access_token']
    
    #response["bearer"] = "thetoken"
    return response