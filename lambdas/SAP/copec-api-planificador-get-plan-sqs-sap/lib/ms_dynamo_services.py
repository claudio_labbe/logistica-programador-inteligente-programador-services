import boto3
import sys
import json
from boto3.dynamodb.conditions import And, Attr, Key
from functools import reduce
from operator import and_, or_

class DynamoService:

    # {items:[{...},{...}..]
    def put_items(self, items):
        with self.table.batch_writer() as batch:
            idx = 1
            for it in items["items"]:
                batch.put_item(Item=it)
                idx = idx + 1
        return "put items finished"

    # Method to retrieve data from vehiculos_cab table
    def scan_items(self, request_items):
        query_params = self.build_query_params(request_items)
        #response = self.table.scan(self.table, **query_params)
        response = self.table.scan(**query_params)
        
        items = []
        items = items + response["Items"]
        
        while "LastEvaluatedKey" in response:
            #response = self.table.scan(self.table,**query_params,ExclusiveStartKey=response["LastEvaluatedKey"])
            response = self.table.scan(**query_params,ExclusiveStartKey=response["LastEvaluatedKey"])
            items = items + response["Items"]    
        
        return {"Items":items}



    # ***********************************************
    # **************** Query Section ****************
    # ***********************************************
    
    def generate_query_params(self,plant,date):
        #print("en funcion de generar query")
        params = {}
        params['IndexName'] = "PLANT_CODE-WRITE_TIMESTAMP-index"
        if(self.timestamp != -1):
            #print("generando params para pedidos")
            params['KeyConditionExpression'] = Key('PLANT_CODE').eq(plant) & Key('WRITE_TIMESTAMP').eq(self.timestamp)
        else:
            #print("generando params para obtener timestamp")
            params['KeyConditionExpression'] = Key('PLANT_CODE').eq(plant)
            params['FilterExpression'] = Attr('ENT_BEGDA').eq(date)
            params['ScanIndexForward'] = False
            params['Limit'] = 500
        #print("retornando parametros de query")
        #print(params)
        return params

    def getTimestamp(self,query_params):
        resp = self.table.query(**query_params)
        if resp['Count'] > 0:
            msg = "OK: seteando timestamp: "
            self.timestamp = resp['Items'][0]['WRITE_TIMESTAMP']
            print(msg+str(self.timestamp))
            #return resp['Items'][0]['WRITE_TIMESTAMP']
        elif 'LastEvaluatedKey' in resp:
            #print("no trae objetos, se continua con ultima llave evaluada")
            query_params['ExclusiveStartKey'] = resp['LastEvaluatedKey']
            self.getTimestamp(query_params)
        else:
            self.timestamp = -1
            print("Error, no se encontraron objetos")

    def queryPedidos(self,plant,date):
        query_params = self.generate_query_params(plant,date)

        response = self.table.query(**query_params)
        
        items = []
        items = items + response["Items"]

        while "LastEvaluatedKey" in response:
            #print("Más pedidos de los que trae la query, continuando con ultima llave evaluada")
            #response = self.table.scan(self.table,**query_params,ExclusiveStartKey=response["LastEvaluatedKey"])
            response = self.table.query(**query_params,ExclusiveStartKey=response["LastEvaluatedKey"])
            items = items + response["Items"] 

        return {"Items":items}


    async def getPedidosQuery(self,plant,date):
        print("Realizando Query")
        query_params = self.generate_query_params(plant,date)
        self.getTimestamp(query_params)
        if(self.timestamp !=-1):
            print("Obteniendo pedidos")
            pedidos = self.queryPedidos(plant, date)
            print("OK: pedidos obtenidos")
            return pedidos
        else:
            print("WARN: sin pedidos o no se pudo realizar query")
            return -1   
    
    def getPedidosQuery2(self,plant,date):
        print("Realizando query para timestamp")
        query_params = self.generate_query_params(plant,date)
        print("seteando timestamp")
        self.getTimestamp(query_params)
        if(self.timestamp !=-1):
            print("Relizando query para pedidos")
            pedidos = self.queryPedidos(plant, date)
            print("Pedidos obtenidos exitosamente")
            return pedidos
        else:
            print("No se pudo realizar Query :(")
            return -1

        


    # vechiculo es un json que debe tener un atributo id_vehiculo de tipo string
    def put_item(self, item):
        response = ""
        try:
            response = self.table.put_item(
                Item=item
            )
            # si no falla, se inserta
            response = {'code': 'ok'}
        except Exception as e:
            response = {'code': 'error','msg':str(e)}
        return response

    # jsonRequest debe traer el json con los ids a buscar
    def get_item(self, jsonRequest):
        response = ""
        try:
            response = self.table.get_item(
                Key=jsonRequest
            )

            if ('Item' in response):
                item = response['Item']
                return {'row': item, 'msg': 'ok'}
            else:
                return {'row': {}, 'msg': 'no item to show'}
                #print('no item to show')
        except:
            response = {'code': 'error'}

        return {'row': {}, 'msg': 'no item to show'}

    def __init__(self, tableName,runlocal=False):
        #print("calling constructor dynamo for table: " + tableName)
        if not runlocal :
            dynamodb = boto3.resource('dynamodb')
        else:
            dynamodb = boto3.resource('dynamodb', region_name='local', aws_access_key_id='local', aws_secret_access_key='local', endpoint_url="http://localhost:8000")
        self.table = dynamodb.Table(tableName)
        self.timestamp = -1

    def build_query_params(self, request_items: dict):
        query_params = {}
        if len(request_items) > 0:
            query_params["FilterExpression"] = self.add_expressions(request_items)
        return query_params


    def add_expressions(self, request_items: dict):
        #{'ENT_BEGDA': ['20200317'], 'CODIGO_PLANTA': ['1216'], 'OPTIMIZED': ['1', '0']}
        #{'FECHA_PLAN': '0317_N', 'CODIGO_PLANTA': ['1216']}
        
        #{'ENT_BEGDA': {'item':['20200317'],'condition':'eq'}, 'CODIGO_PLANTA': {'item':['1216'],'condition':'eq'}, 'OPTIMIZED': {'item':['1', '0']}
        #{'FECHA_PLAN': {'item':'0317_N','condition':'eq'}, 'CODIGO_PLANTA': {'item':['1216'],'condition':'eq'}}
        if request_items:
            conditions = []
            #dynamo suppports till 100 element in "is in" operation
            #if, ther is more elements, we have to split the query
            has_to_split_condition = False
            for key, value in request_items.items():
                if isinstance(value["item"], str):
                    if value["condition"]=="eq":
                        conditions.append(Attr(key).eq(value["item"]))
                    if value["condition"]=="lte":
                        conditions.append(Attr(key).lte(value["item"]))
                    if value["condition"]=="gte":
                        conditions.append(Attr(key).gte(value["item"]))
                    if value["condition"]=="contains":
                        conditions.append(Attr(key).contains(value["item"]))
                    else:
                        conditions.append(Attr(key).eq(value["item"]))
                        
                if isinstance(value["item"], list):
                    if len(value["item"]) > 100:
                        has_to_split_condition = True
                        i = 0
                        step = 50
                        while i < len(value["item"]):
                            section = value["item"][i:i+step]
                            conditions.append(Attr(key).is_in([v for v in section]))
                            i+=step;
                    
                    else:
                        conditions.append(Attr(key).is_in([v for v in value["item"]]))
                    
            if has_to_split_condition:
                return reduce(or_, conditions)
            
            return reduce(and_, conditions)


def main():
    flotaService = FlotaService()
    # flotaService.put_item({'id_vehiculo':'2222'})
    # flotaService.get_item('2a222')


if __name__ == "__main__":
    main()
