import boto3
from lib.truck_structure_generator import TruckStructureGenerator
from lib.orders_structure_generator import OrdersStructureGenerator
from lib.orders_structure_store import storeOrdersDynamo
from lib.soap_service import soapService
from lib.soap_service_zeep import soapServiceZeep
from lib.ms_dynamo_services import DynamoService
import time
import math
import json
import zlib
import base64

import os

from requests import Session
from requests.auth import HTTPBasicAuth
from zeep import Client
from zeep.transports import Transport
import logging

from lib.xmltodict import xmltodict
#import xmltodict

from json import loads, dumps
#import time

import asyncio

import io
import pandas as pd
import numpy as np


def get_hhmmss(minutes_float):
    #transform hhmmss
    h = math.floor(minutes_float/60)
    m = math.floor(minutes_float%60)
    minutes_remaining = (minutes_float%60) - m
    s = math.ceil(60*minutes_remaining)

    hh = str(h) 
    if h < 10:
        hh = "0"+hh

    mm = str(m) 
    if m < 10:
        mm = "0"+mm

    ss = str(s) 
    if s < 10:
        ss = "0"+ss

    return str(hh)+str(mm)+str(ss)

async def getTimmesData(plant):
    #print("entro en func")
    s3 = boto3.resource('s3')
    #print("set s3")
    bucket_name = os.environ['BUCKET_CSV_TIMES']
    #print(bucket_name)
    path_to_athena = os.environ['PATH_CSV_TIMES']
    #print(path_to_athena)
    path_name_load_times = os.environ['LOAD_TIMES_CSV_PATH_NAME']
    #print(path_name_load_times)
    #print("nombres de variables de entorno listas")
    
    load_times_obj = s3.Object(bucket_name,path_name_load_times)
    load_times_stream = io.BytesIO(load_times_obj.get()['Body'].read())
    load_times_df = pd.read_csv(load_times_stream,sep=';')
    #print("se obtuvo primer csv")
    
    bucket = s3.Bucket(bucket_name)
    #print(bucket)
    dir_name = path_to_athena+'plant='+str(plant)
    #print(dir_name)
    csv_files = []
    #print(csv_files)
    for obj in bucket.objects.filter(Prefix=dir_name): csv_files.append(obj)
    #print(csv_files)
    if(len(csv_files)==1):
        csv_obj = csv_files[0].get()['Body'].read()
    else:
        #aquí ver caso en que hubiese más de un archivo, por ahora se sigue tomando el primero
        csv_obj = csv_files[0].get()['Body'].read()
    #print("se obtuvo csv tiempos athena")
    athena_times_stream = io.BytesIO(csv_obj)
    athena_times_df = pd.read_csv(athena_times_stream,names=['origin','destination','travel_time_estimated','travel_time_minutes','stddev','count'])
    #print("cargado en df")
    tiempo_carga = int(load_times_df.loc[load_times_df['plant_code']==int(plant)]['tiempo_carga'])
    tiempo_descarga = 45
    
    #print("se setean tiempos de carga y carga")
    
    athena_times_df = athena_times_df.rename(columns = {'travel_time_minutes':'minutes'}, inplace = False)
    #print("DF: rename")
    athena_times_df['duracion'] = athena_times_df['minutes'].apply(lambda x: x*2).apply(get_hhmmss)
    #print("DF: get_hhmmss")
    athena_times_df['duracion_calculada'] = athena_times_df['minutes'].apply(lambda x: float(x)*2 + tiempo_carga+tiempo_descarga).apply(get_hhmmss)
    #print("DF: lambda x")
    athena_times_df['tiempo_carga_default'] = tiempo_carga
    athena_times_df['tiempo_descarga_default'] = tiempo_descarga
    print("OK: nuevo df obtenido")
    print(athena_times_df.head())
    
    return athena_times_df
    

async def getTimesCsv(plant):
    print("Obteniendo CSV Times")
    s3 = boto3.resource('s3')
    
    bucket_name = os.environ['BUCKET_CSV_TIMES'] 
    path_to_csv = os.environ['PATH_CSV_TIMES'] 
    
    bucket = s3.Bucket(bucket_name)
    #print(bucket)
    dir_name = path_to_csv+'plant='+str(plant)
    #print(dir_name)
    csv_files = []

    for obj in bucket.objects.filter(Prefix=dir_name): csv_files.append(obj)
    #print(csv_files)
    
    if(len(csv_files)==1):
        csv_obj = csv_files[0].get()['Body'].read()
    else:
        #aquí ver caso en que hubiese más de un archivo, por ahora se sigue tomando el primero
        csv_obj = csv_files[0].get()['Body'].read()
    
    stream_csv_obj = io.BytesIO(csv_obj)
    #df_csv = pd.read_csv(stream_csv_obj,names=['origin','destination','travel_time_estimated','travel_time_minutes','stddev','count'])
    df_csv = pd.read_csv(stream_csv_obj)
    print("OK: tiempso obtenidos en Dataframe")
    
    return df_csv

async def zeepTest(sap_user,sap_pwd,sap_url,sap_date, plant):
    print("Realizando pedidos a SAP")
    logging.basicConfig(level=logging.INFO)
    
    #print("logging ok, comienza session")
    session = Session()
    session.auth = HTTPBasicAuth(sap_user, sap_pwd)
    client = Client(sap_url, transport= Transport(session=session))
    #print("Cliente creado")
    
    try:
        factory = client.type_factory('ns0')
        ItClienteFecha = factory.ZprogTtKunnrDatum()
        #print("intentando obtener data de SAP")
        #el 2do parametro 1,2, 3 o 4 se le da a zws_pr_obtiene_prog_sap. 2 es solo ver. 1 creo que es para hacer cambios.
        with client.settings(strict=False,raw_response=True):
            #response = client.service.ZbdpObtieneProgramacion('2021-03-11',2,'1208',ItClienteFecha)
            response = client.service.ZbdpObtieneProgramacion(sap_date,2,plant,ItClienteFecha)
        print("OK: Pedidos de sap obtenidos")
        #string_response_xml = response.content#.decode("utf-8")
        #response_json=self.to_dict(xmltodict.parse(string_response_xml))
        response_json = loads(dumps(xmltodict.parse(response.content)))
        
        base = response_json["soap-env:Envelope"]["soap-env:Body"]["n0:ZbdpObtieneProgramacionResponse"]
        
        res={}
        res["ERc"]=base["ERc"] if base["ERc"] != None else " "
        res["EMessage"]=base["EMessage"] if base["EMessage"] != None else " "
        res["EtVehicles"]=base["EtVehicles"] if base["EtVehicles"] != None else " "
        res["EtVehicleDet"]=base["EtVehicleDet"] if base["EtVehicleDet"] != None else " "
        res["EtPedidos"]=base["EtPedidos"] if base["EtPedidos"] != None else " "
        res["EtPedidoDet"]=base["EtPedidoDet"] if base["EtPedidoDet"] != None else " "
        res["EtCambioCamion"]=base["EtCambioCamion"] #if base["EtCambioCamion"] != None else " "
       
        #print(res)
        return res
    except Exception as e:
        print("Err")
        print(e)
        return False
        
async def asyncCalls(sap_user,sap_pwd,sap_url,date,sap_date,plant):
    #print("Entró en la función")
    service_pedidos_dynamo_optimizador = DynamoService(os.environ['DYNA_PEDIDOS_OPT'])
    #print(os.environ['DYNA_PEDIDOS_OPT'])
    #print("Parámetros queries")
    #print(date)
    #print(type(date))
    #print(plant)
    #print(type(plant))
    service_pedidos_dynamo_usuario = DynamoService(os.environ['DYNA_PEDIDOS_USR'])
    #print("se setearon las tablas")
    loop = asyncio.get_running_loop()
    res = await asyncio.gather(
        #zeepTest(sap_user,sap_pwd,sap_url,sap_date,plant),
        service_pedidos_dynamo_usuario.getPedidosQuery(int(plant),date),
        service_pedidos_dynamo_optimizador.getPedidosQuery(int(plant),date),
        #getTimesCsv(plant),
        getTimesCsv(plant)
    )
    return res

def lambda_handler(event, context):
    print("llamando a limpiar sap sqs")
    message_id = event["message_id"]
    sqs_table = os.environ['SQS_DYNAMO_TABLE']
    global_user = event["body"]["sap_user"]
    global_plant = event["body"]["codigo_planta"]
    #print(global_user)
    #print(global_plant)
    try:
        
        request = event["body"]
        
        tg = TruckStructureGenerator()
        og = OrdersStructureGenerator()

        sap_user = request["sap_user"]
        sap_pwd = request["sap_pwd"]
        sap_url = os.environ['SAP_URL']
        
        dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
        dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
        
        
        input_order = {"fecha_pedido":request["codigo_fecha"], "codigo_planta":request["codigo_planta"]}
        input_truck = {"fecha_plan":request["codigo_fecha"], "codigo_planta":request["codigo_planta"]}

        fechaSoap = request["codigo_fecha"][0:4] + "-" + request["codigo_fecha"][4:6] + "-" + request["codigo_fecha"][6:8] 
        soapService_zeep = soapServiceZeep()
        
        soapService_zeep.createClient(sap_user,sap_pwd,sap_url)
        
        soap_response = soapService_zeep.clear(fechaSoap,[request["codigo_planta"]])
        soap_vehicles_cab = soap_response["EtVehicles"]
        soap_pedidos = soap_response["EtPedidos"]
        soap_pedidos_det = soap_response["EtPedidoDet"]
        
        sap_response_code = soap_response["ERc"]
        sap_response_message = soap_response["EMessage"]
        
        query_usr_resp = {}
        query_opt_resp = {}
        times_csv_df = []

        available_parallel_plants = ['1202','1210','1207','1228','1208','1216','1211','1205','1215','1336','1254','1245','1204','1201','1206','1213','1214','1212','1203','4201']

        if(request["codigo_planta"] in available_parallel_plants):
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            async_dayna_usr,async_dyna_opt,get_data = loop.run_until_complete(asyncCalls(sap_user,sap_pwd,sap_url,request["codigo_fecha"],fechaSoap,request["codigo_planta"]))
            loop.close()
        

            #print(async_dayna_usr)
            print(type(async_dayna_usr))
            #print(async_dyna_opt)
            #print(type(async_dyna_opt))
            #print(type(async_csv_df))
            #print(len(async_csv_df))
            print(type(get_data))
            print(len(get_data))
            #service_pedidos_dynamo_usuario = DynamoService(os.environ['DYNA_PEDIDOS_USR'])
            #resp = service_pedidos_dynamo_usuario.getPedidosQuery2(1208,'20210311')
            #print(resp)
            query_usr_resp = async_dayna_usr
            query_opt_resp = async_dyna_opt
            times_csv_df = get_data
        else:
            print("SE REALIZA EJECUCION SECUENCIAL")
            

        input_truck["soap_vehicles_cab"] = soap_vehicles_cab
        input_order["soap_pedidos"] = soap_pedidos
        input_order["soap_pedidos_det"] = soap_pedidos_det
        input_order["merge_dynamo"] = True
        input_order["clean"] = True
        input_order["pedidos_opt_dynamo"] = dyna_pedidos_opt
        input_order["pedidos_usr_dynamo"] = dyna_pedidos_usr
        
        #NUEVOS ELEMENTOS AGREGADOS POR QUERIES
        input_order["query_opt"] = query_opt_resp
        input_order["query_usr"] = query_usr_resp

        #AGREGANDO DATAFRAME CON TIEMPOS
        input_order['times_df'] =  times_csv_df
        #input_order['csv_times_df'] = get_data
        
        response_truck = tg.generate_structure(input_truck)
        response = og.generate_structure(input_order)
        response["truck"] = response_truck   
        
        response["ERc"] = sap_response_code
        response["EMessage"] = sap_response_message
        #return response
        byteArr = bytes(json.dumps(response),'utf-8')
        compressed = zlib.compress(byteArr)
        
        s3_bucket = os.environ['S3_SQS_ROOT']
        s3_key = "sqs_stored/" +  message_id
        s3_path = s3_bucket + '/' + s3_key
        
        s3 = boto3.resource('s3')
        object = s3.Object(s3_bucket, s3_key)
        object.put(Body=compressed)
        
        #store response in DB
        #item = {"message_id":message_id, "message_body":response}
        usuario = request["sap_user"]
        planta = request["codigo_planta"][0]
        timestamp = int(time.time())
        
        item = {"message_id":message_id, "s3_bucket":s3_bucket, "s3_key":s3_key, "s3_reference_body":s3_path ,"function":"copec_api_planificador_limpiar_sqs_sap", "user":usuario, "planta":planta, "timestamp":timestamp}
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(sqs_table)
        
        db_response = table.put_item(
            Item = item
        )
        print("FINAL RESPONSE:")
        print(db_response)
        return db_response
        
        
    except Exception as e:
        #store response in DB
        timestamp = int(time.time())
        #item = {"message_id":message_id, "error":str(e), "function":"copec_api_planificador_limpiar_sqs_sap"}
        item = {"message_id":message_id, "error":str(e), "function":"copec_api_planificador_limpiar_sqs_sap", "user":global_user, "planta":global_plant, "timestamp":timestamp}
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(sqs_table)
        db_response = table.put_item(
            Item = item
        )
        return db_response