from lib.truck_structure_generator import TruckStructureGenerator
from lib.orders_structure_generator import OrdersStructureGenerator
from lib.orders_structure_store import storeOrdersDynamo
from lib.soap_service import soapService
from lib.soap_service_zeep import soapServiceZeep
import time
import math

import os

def lambda_handler(event, context):
    try:
        start = time.time()
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        sap_url = os.environ['SAP_URL']
        #'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
        sap_url_xtras = os.environ['SAP_URL_XTRAS']
        #'http://coqabroker1.copec.cl:7812/programador/zws_pr_extras_prog_sap?wsdl'
        sap_url_messages = os.environ['SAP_URL_MESSAGES'] 

        dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
        dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
        
        tg = TruckStructureGenerator()
        og = OrdersStructureGenerator()
        
        input_order = {"fecha_pedido":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}
        input_truck = {"fecha_plan":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}

        fechaSoap = event["codigo_fecha"][0][0:4] + "-" + event["codigo_fecha"][0][4:6] + "-" + event["codigo_fecha"][0][6:8] 

        #para obtener los mensajes usados en combobox del programador al momento de dar de baja camiones o pedidos
        soap_service_messages = soapService()
        soap_service_messages.createClient(sap_user,sap_pwd,sap_url_messages)

        soap_response_messages = soap_service_messages.get_combo_messages()
        soapService_zeep = soapServiceZeep()
        soapService_zeep.createClient(sap_user,sap_pwd,sap_url)

        soap_response = soapService_zeep.get_plan(fechaSoap,event["codigo_planta"])

        sap_response_code = soap_response["ERc"]
        sap_response_message = soap_response["EMessage"]

        soap_vehicles_cab = soap_response["EtVehicles"]
        soap_vehicles_det = soap_response["EtVehicleDet"]
        soap_pedidos = soap_response["EtPedidos"]
        soap_pedidos_det = soap_response["EtPedidoDet"]

        soap_update_vehiculos = soap_response["EtCambioCamion"]

        #entregamos los vehiculos que tengan turno abajo
        vehicle_down = []
        if soap_update_vehiculos!=None:
            print(type(soap_update_vehiculos["item"]).__name__)
            if type(soap_update_vehiculos["item"]).__name__ == "list":
                for it in soap_update_vehiculos["item"]:
                    vehicle_item = {}
                    #vehicle = it["Vehicle"]
                    #motivos = it["Motivos"]
                    vehicle_item["Vehicle"] = it["Vehicle"]
                    vehicle_item["Motivos"] = it["Motivos"]
                    vehicle_item["TurnoFechaDesde"] = it["TurnoFechaDesde"]
                    vehicle_item["TurnoHoraDesde"] = it["TurnoHoraDesde"]
                    vehicle_item["TurnoFechaHasta"] = it["TurnoFechaHasta"]
                    vehicle_item["TurnoHoraHasta"] = it["TurnoHoraHasta"]
                    
                    
                    #print(motivos)
                    tipo_motivo = list(filter(lambda x: x["MOTIVO"] == vehicle_item["Motivos"], soap_response_messages["ET_MOTIVOS"]["item"]))
                    #si largo es distinto que 1 hay algo malo.. No puede haber mas de un elemento por motivo
                    if len(tipo_motivo)==1:
                        if tipo_motivo[0]["TIPO"] == "B":
                            vehicle_down.append(vehicle_item)
                            
            elif type(soap_update_vehiculos["item"]).__name__ == "dict":
                it = soap_update_vehiculos["item"]
                vehicle_item = {}
                #vehicle = it["Vehicle"]
                #motivos = it["Motivos"]
                vehicle_item["Vehicle"] = it["Vehicle"]
                vehicle_item["Motivos"] = it["Motivos"]
                vehicle_item["TurnoFechaDesde"] = it["TurnoFechaDesde"]
                vehicle_item["TurnoHoraDesde"] = it["TurnoHoraDesde"]
                vehicle_item["TurnoFechaHasta"] = it["TurnoFechaHasta"]
                vehicle_item["TurnoHoraHasta"] = it["TurnoHoraHasta"]
                
                
                #print(motivos)
                tipo_motivo = list(filter(lambda x: x["MOTIVO"] == vehicle_item["Motivos"], soap_response_messages["ET_MOTIVOS"]["item"]))
                #si largo es distinto que 1 hay algo malo.. No puede haber mas de un elemento por motivo
                if len(tipo_motivo)==1:
                    if tipo_motivo[0]["TIPO"] == "B":
                        vehicle_down.append(vehicle_item)
                        
                        
        #probamos 
        if type(soap_vehicles_cab).__name__ == "str":
            soap_vehicles_cab = []
        if type(soap_vehicles_det).__name__ == "str":
            soap_vehicles_det = []
        if type(soap_pedidos).__name__ == "str":
            soap_pedidos = []
        if type(soap_pedidos_det).__name__ == "str":
            soap_pedidos_det = []


        input_truck["soap_vehicles_cab"] = soap_vehicles_cab
        input_truck["soap_vehicles_det"] = soap_vehicles_det
        input_order["soap_pedidos"] = soap_pedidos
        input_order["soap_pedidos_det"] = soap_pedidos_det
        input_order["merge_dynamo"] = True
        input_order["clean"] = False
        input_order["pedidos_opt_dynamo"] = dyna_pedidos_opt
        input_order["pedidos_usr_dynamo"] = dyna_pedidos_usr

        response_truck = tg.generate_structure(input_truck)
        response_truck_cab_optimizer = tg.generate_cab_structure_optimizer(input_truck)
        response_truck_det_optimizer = tg.generate_det_structure_optimizer(input_truck)
        #generamos un response en caso que no hayan pedidos. Esto para poder agregar un camion.
        response = {"pedidos": [], "duraciones":[]}
        if len(soap_pedidos) > 0:
            response = og.generate_structure(input_order)
        response["truck"] = response_truck

        response["truck_cab_optimizer"] = response_truck_cab_optimizer
        response["truck_det_optimizer"] = response_truck_det_optimizer

        response["ERc"] = sap_response_code
        response["EMessage"] = sap_response_message

        response["messages_combo"] = soap_response_messages;
        response["vehicle_down"] = vehicle_down

        #print(response)
        return response
        
    except Exception as e:
        print(e)
        return {"retCode":"500", "message": e}