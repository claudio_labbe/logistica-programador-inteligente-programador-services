from lib.truck_structure_generator import TruckStructureGenerator
from lib.orders_structure_generator import OrdersStructureGenerator
from lib.orders_structure_store import storeOrdersDynamo
from lib.soap_service_zeep import soapServiceZeep
import time
import math

import os

from datetime import datetime
from datetime import timedelta

def lambda_handler(event, context):
    try:
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        sap_url = os.environ['SAP_URL']
        
        dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
        dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
        
        
        input = {"items": event["plan"],"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
        
        print(event["codigo_planta"])
        
        soapService_zeep = soapServiceZeep()
        soapService_zeep.createClient(sap_user,sap_pwd,sap_url)
        sap_response = soapService_zeep.confirm(input)
        
        print('### SAP RESPONSE:')
        print(sap_response)

        #cuando confirmamos y no hay problemam ERc es 0. El mensaje es "espere un momento hasta que se confirme"
        #vamos a consultar sap hasta que veamos que la lista de pedidos este confirmada
        if sap_response['ERc'] != '0':
            response = {}
            response["retCode"]="200"
            response["ERc"] = sap_response["ERc"]
            response["EMessage"] = sap_response["EMessage"]
            return response

        current_time =  datetime.utcnow()
        if(event["timestamp"] != -1 and event["timestamp"] != None):
            timestamp = str(event["timestamp"])
            print("UTILIZANDO TIMESTAMP DESDE FRONT CONFIRMAR")
        else:
            print("UTILIZANDO TIMESTAMP GENERADO AHORA")
            timestamp = str(int(float(current_time.timestamp())))

        print(timestamp)
        ttl = int(float((current_time + timedelta(days=5)).timestamp()))

        
        if sap_response['ERc'] == '0':
            print("Sap ERc = 0")
            id_pedidos_confirmados =[]
            for  ped in event["plan"]:
                id_pedidos_confirmados.append(ped["VBELN"])
                
            
                
            pedidos_confirmados = event["plan"]
            
            #se guarda en dynamo asumiendo que el o los pedidos van a confirmarse en SAP sin problema. El agrupador, se lo dejamos como un agrupador generado por el programador
            for pc in pedidos_confirmados:
                pc["ESTADO"] = "3"
                #pc["AGRUPADOR"] = agrupador_pedidos_by_id[pc["VBELN"]]
                
            
            input_dyna = {"items": pedidos_confirmados,"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
            #dejarlo arriba, como env. 
            table_dynamo_pedidos_user = dyna_pedidos_usr
            service_dynamo = storeOrdersDynamo(table_dynamo_pedidos_user)
            dynamo_store_succes = service_dynamo.store(input_dyna,timestamp,ttl)
            tries_dyna = 3
            count_dyna = 0
            while (dynamo_store_succes != True) and count_dyna != tries_dyna :
                dynamo_store_succes = service_dynamo.store(input_dyna,timestamp,ttl)
                count_dyna = count_dyna + 1
                time.sleep(0.5)
            
            if dynamo_store_succes != True:
                return {"retCode":"200","message":"NO se pudo guardar en dynamo","lista_confirmados":id_pedidos_confirmados, "ERc":sap_response["ERc"],"EMessage":sap_response["EMessage"]}
                
            return {"retCode":"200","message":"se pudo guardar en dynamo","lista_confirmados":id_pedidos_confirmados, "ERc":sap_response["ERc"],"EMessage":sap_response["EMessage"]}
    
    except Exception as e:
        #print("exception")
        #print(e)
        return {"retCode":"500", "message":e}