from .ms_dynamo_services import DynamoService
import datetime
import boto3
import time
import math 
import os

class OrdersStructureGenerator:

    def __init__(self): 
        self.clean = False
        pass
        
    #detalleType de la forma 24=6-5-4-8/9 por ejemplo o 24=6-5-4-8
    def get_countLiter(self,value):
        if value == "S":
            return True
        return False

    def format_date(self,yyyymmdd):
        return yyyymmdd[:4] + "-" + yyyymmdd[4:6] + "-" + yyyymmdd[6:8] 
        
    #A veces las horas en soap vienen sin el cero por delante
    #si vviene con : se separa. Si no se entrega tal cual
    def soap_time_to_hhmmss(self,soap_time):
        l = soap_time.split(":")
        if len(l)>1:
            hh = l[0]
            mm = l[1]
            ss = l[2]
            if int(hh) < 10:
                hh = "0"+str(int(hh))
            if int(mm) < 10:
                mm = "0"+str(int(mm))
            if int(ss) < 10:
                ss = "0"+str(int(ss))
            return hh+mm+ss
        return soap_time
    
    def soap_date_to_yyyymmdd(self,soap_date):
        return soap_date.replace("-","")

    def get_time(self,time,unit):
        if unit == "h":
            return time[:2]
        if unit == "m":
            return time[2:4]
        if unit == "s":
            return time[4:6]

    def get_date_from_shift(self,shift):
        year = int(shift["dayInit"].split("-")[0])
        month = int(shift["dayInit"].split("-")[1])
        day = int(shift["dayInit"].split("-")[2])
        hour=int(shift["hourInit"])
        minutes=int(shift["minutesInit"])
        seconds=int(shift["secondsInit"])
        ##print(str(year) + "-" + str(month) + "-" + str(day) + "-" + str(hour)  + ":" + str(minutes) + ":" + str(seconds)) 
        return datetime.datetime(year,month,day,hour=hour,minute=minutes,second=seconds).timestamp() 
    
    #en input_search ya vienen los pedidos de SAP cargados
    #fijarse pasos 1, 2,... etc para claridad y documentacion
    def generate_structure(self,input_search):
        start = time.time()
        pedidos_opt_table = input_search["pedidos_opt_dynamo"] #Pedido_dev_opt
        pedidos_usr_table = input_search["pedidos_usr_dynamo"] #Pedido_dev_usr
        
        
        if type(input_search["codigo_planta"]).__name__ == "str":
            self.codigo_planta = int(input_search["codigo_planta"])
        elif type(input_search["codigo_planta"]).__name__ == "list":
            self.codigo_planta = int(input_search["codigo_planta"][0])
        #service_pedidos = DynamoService("Pedidos")
        service_pedidos_dynamo_optimizador = DynamoService(pedidos_opt_table)
        service_pedidos_dynamo_usuario = DynamoService(pedidos_usr_table)
        
        soap_pedidos = input_search["soap_pedidos"]
        soap_pedidos_det = input_search["soap_pedidos_det"]
        
        #for debug. If true, we merge with dynamo. otherwise, just sap
        merge_dynamo = input_search["merge_dynamo"]
        
        #True si la funcion es la que limpia
        if "clean" in input_search:
            self.clean = input_search["clean"]
        #1 unificamos en una sola estructura los detalles de los pedidos con los pedidos. Esto es SOLO SAP
        if type(soap_pedidos).__name__ != "str" and type(soap_pedidos_det).__name__ != "str":
            self.completa_detalle(soap_pedidos["item"],soap_pedidos_det["item"])
        #2 Genero la lista de pedidos que vienen de sap, con data normalizada para el programador. 
        #  Tambien se entrega la lista de los ID (VBELN) que vienen de SAP. Esto se usa para mergear con dynamo
        t1_calculateOrder = time.time()
        pedidosListResponse = self.calculateOrderDate(soap_pedidos)
        t2_calculateOrder = time.time()
        print(">calculateOrderDate : " + str(t2_calculateOrder-t1_calculateOrder))
        print("        ===========[orders_structure_generator]")
        #3 obtengo la lista de pedidos de SAP y sus respectivos ID (VBELN) que necesitare para hacer el merge con dynamo
        #lista de id de pedidos de los que encuentro en sap
        listaVBELN_soap = pedidosListResponse["lista_vbeln"]
        #lista de pedidos que encuentro en sap
        pedidos_soap = pedidosListResponse["pedidosList"]
        
        #En dynamo, buscamos los pedidos cuyos Ids vengan de sap
        input = {"VBELN": {"item":listaVBELN_soap}}
        #pedidos_dynamo = service_pedidos.scan_items(input)  

        #Teniendo los pedidos de dynamo que coinciden con SAP, hacemos el merge si corresponde
        #pedidos_merged debera contener los pedidos en sap, y si los id estaban en dynamo, los campos definidos se sobreescriben
        #la idea es que si un pedido de sap se encuentra en dynamo, actualizamos la informacion optimizada solo para mostrar en el pedido
        #pero sap en si no se toca
        pedidos_dynamo = []
        if merge_dynamo:
            #4 Con el input dado, el servicio de scan_items obtiene los pedidos dynamo mas recientes que se encuentren en la lista
            #  de pedidos SAP.
            #  Estos pedidos contienen la info de optimizacion y de los usuarios
            #print("obtener pedidos de dynamo para poder hacer merge con sap")
            #print("scan pedidos_dynamo_opt")
            t1_scan_opt = time.time()
            pedidos_dynamo_opt = service_pedidos_dynamo_optimizador.scan_items(input)   
            t2_scan_opt = time.time()
            print(">obtener pedidos dyna optimizador (scan): " + str(t2_scan_opt-t1_scan_opt))
            print("        ===========[orders_structure_generator]")
            
            #print("scan pedidos_dynamo_usr")
            t1_scan_usr = time.time()
            pedidos_dynamo_usr = service_pedidos_dynamo_usuario.scan_items(input)  
            t2_scan_usr = time.time()
            print(">obtener pedidos dyna user (scan): " + str(t2_scan_usr-t1_scan_usr))
            print("        ===========[orders_structure_generator]")
            
            #opt y usr tienen los pedidos que coinciden con los pedidos SAP.
            #print("pedidos get_most_recent")
            t1_get_most_recent = time.time()
            pedidos_dynamo = self.get_most_recent(pedidos_dynamo_opt["Items"],pedidos_dynamo_usr["Items"])        
            t2_get_most_recent = time.time()
            print(">get_most_recent : " + str(t2_get_most_recent-t1_get_most_recent))
            print("        ===========[orders_structure_generator]")
            
        #print("pedidos merge_sap_dynamo")
        t1_merge_sap_dynamo = time.time() 
        pedidos_merged = self.merge_sap_dynamo(pedidos_soap,pedidos_dynamo)
        t2_merge_sap_dynamo = time.time()
        print(">merge : " + str(t2_merge_sap_dynamo-t1_merge_sap_dynamo))
        print("        ===========[orders_structure_generator]")
        #entregamos las duraciones. Estas se entregan al programador para poder determinar los pedidos programados
        end = time.time()
        print("TIEMPO TOTAL GENERATE STRUCTURE: " + str(end-start))
        return {"pedidos": pedidos_merged, "duraciones":self.matriz_duraciones}    

    #buscamos el mas reciente en caso de estar en opt y usr. 
    #si no esta en alguno, usamos el pedido que esta en el otro (usr u opt)
    def get_most_recent(self,pedidos_opt,pedidos_usr):
        pedidos_mas_recientes_opt = []
        pedidos_mas_recientes_usr = []
        
        #los mas recientes entre opt y usr
        pedidos_mas_recientes = []
        
        #Obtenemos los elementos mas recientes para opt y usr
        pedidos_mas_recientes_opt = self.get_most_recent_one_par(pedidos_opt)
        pedidos_mas_recientes_usr = self.get_most_recent_one_par(pedidos_usr)
        
        #buscamos en los pedidos optimizados. Si es que un pedido que se encuentra en los optimizados, se encuentra en usr
        #elegimos el mas nuevo (mayor timestamp) (comparar casteando a entero, no por string)
        #si es que el pedido que se encuentra en opt, no esta en usr, se utiliza el opt.
        for p_opt in pedidos_mas_recientes_opt:
            
            opt_in_usr = list(filter(lambda x: p_opt["VBELN"] == x["VBELN"]  ,pedidos_mas_recientes_usr))
            if len(opt_in_usr) > 0 :
                #comparo el valor en usr vs opt, cual es mas nuevo
                if int(opt_in_usr[0]["WRITE_TIMESTAMP"]) > int(p_opt["WRITE_TIMESTAMP"]) :
                    pedidos_mas_recientes.append(opt_in_usr[0])
                else:
                    pedidos_mas_recientes.append(p_opt)
            else:
                pedidos_mas_recientes.append(p_opt)
        

        #Iteramos sobre usr. Dado que se comparo con opt, en caso de encontrarse en ambos, agremgamos a la lista de 
        #pedidos dyna solo los que NO SE encuentren en OPT
        for p_usr in pedidos_mas_recientes_usr:
            usr_not_in_opt = list(filter(lambda x: p_usr["VBELN"] == x["VBELN"]  ,pedidos_mas_recientes_opt))
            if len(usr_not_in_opt) == 0:
                pedidos_mas_recientes.append(p_usr)
                
        
        #retornamos los pedidos mas recientes entre opt y usr que hay en comun. 
        return pedidos_mas_recientes
                
    #dado un array de pedidos, obtengo los pedidos mas recientes (puede haber varios pedidos por un mismo id)
    def get_most_recent_one_par(self, pedidos_dyna):
        pedidos_mas_recientes = []
        #agregamos en un objecto json los pedidos. Su llave es el ID de pedido VBELN
        json_vbeln = {}
        for p in pedidos_dyna:
            if p["VBELN"] not in json_vbeln:
                json_vbeln[p["VBELN"]] = []
            json_vbeln[p["VBELN"]].append(p)
        
        for key in json_vbeln:
            self.quickSort(json_vbeln[key],0,len(json_vbeln[key])-1,"WRITE_TIMESTAMP")
            pedidos_mas_recientes.append(json_vbeln[key][0])
        
        return pedidos_mas_recientes
    


    #inicializamos todos los keys definidos en dynamo en caso que no esten
    def check_create_pedido_key_dynamo(self,pedido):
        #fields that should be used in web app (no matter if you save it in sap, you will save in dyna)
        common_fields = ["WARNING_MESSAGE","ERROR_MESSAGE"]
        #TODO usar una lista de keys a definir luego..
        for msg in common_fields:
            if not msg in pedido:
                pedido[msg] = " "
        
    #define a quickSort to order the arrays by key
    #de mayor a menor by default
    def quickSort(self,arr,low,high,key):    
        if low < high:
            pi = self.partition(arr,low,high,key)
            self.quickSort(arr,low,pi-1,key)
            self.quickSort(arr,pi+1,high,key)
            
    def partition(self,arr,low,high,key):
        i = low -1
        pivot = arr[high]
        
        for j in range(low,high):
            #ordenamos de mayor a menor
            if int(arr[j][key])  >= int(pivot[key]):
                i = i + 1
                arr[i],arr[j] = arr[j],arr[i]
                
        arr[i+1],arr[high] = arr[high],arr[i+1]
        return i + 1   
        
        
    #pedidos_soap lista de pedidos que vienen de soap
    #pedidos_dynamo lista de pedidos que vienen de dynamo y que es un subconjunto de los pedidos que vienen en soap.
    #es decir, pueden venir los pedidos cuyos id se encuentren presentes en los pedidos de soap.
    def merge_sap_dynamo(self,pedidos_soap,pedidos_dynamo):
        print("MERGE SAP DYNA")
        #si clean, dejamos solo los confirmados en dynamo pa hacer el merge.
        #los de sap, por la definicion de su servicio, deben venir vacios
        if self.clean == True:
            pedidos_dynamo = list(filter(lambda x: x["ESTADO"]=="3", pedidos_dynamo))
        #inicialmente pedidos_hibrido van a ser los pedidos que vienen de soap
        pedidos_hibrido = pedidos_soap.copy()
        for pedido_h in pedidos_hibrido:
            #busco si mi pedido que viene de soap, esta en dynamo...
            match = list(filter(lambda x: x["VBELN"]==pedido_h["VBELN"], pedidos_dynamo))
            
            print("MATCH (encontramos en dyna) reparando bug de tiempos largos.")
            self.debug = False
            #if pedido_h["VBELN"]  == "5500797654": 
            if pedido_h["VBELN"]  == "0404816767" or pedido_h["VBELN"]  == "0404816768": 
                print(pedido_h)
                self.debug = True
            
            
            if len(match)==0:
                print("NO HAY MATCH DE PEDIDOS SAP CON DYNA")
                #no se encuentran pedidos en dyna que hagan match con los pedidos de sap.
                #dejo pedido sap tal cual y agrego los valores en blanco que necesitaria el programador
                self.calcular_duraciones(pedido_h)
                """
                self.check_create_pedido_key_dynamo(pedido_h)
                ##print("para codigo: " + str(self.codigo_planta))
                ##print(self.matriz_duraciones[str(self.codigo_planta)])
                tupla = []
                if len(self.matriz_duraciones) > 0:
                    tupla = list(filter(lambda x: int(x["destination"]) == int(pedido_h["KUNAG"]), self.matriz_duraciones[str(self.codigo_planta)]))
                
                if len(tupla) > 0:
                    ##print("asignando duracion de matriz athena a pedido")
                    pedido_h["DURACION"] = tupla[0]["duracion"] 
                    pedido_h["DURACION_CALCULA"] = tupla[0]["duracion_calculada"] 
                """
                #else:
                    #print("no se encontro match entre numero de pedido y distancias. Revisar (1). Se deja durac original:" + str(pedido_h["DURACION"]))

            else:
                print("SI HAY MATCH DE PEDIDOS SAP CON DYNA")
                pedido_dyna = match[0]
                  
                
                #HACEMOS CHECK DE CONFIRMACION. EL ESTADO DEL PEDIDO, SI ESTA CONFIRMADO O NO, MANDA SAP
                if pedido_h["ESTADO"] == pedido_dyna["ESTADO"]:
                    print("DIFERENCIA DE ESTADO DETECTADA ENTRE SAP Y DYNA PARA PEDIDO " + pedido_h["VBELN"])
                
                    #escribimos en los registros que vienen de sap, la info relevante que viene de dynamo
                    pedido_h["PRO_BEGDA"] = pedido_dyna["PRO_BEGDA"]
                    pedido_h["PRO_BEGTI"] = pedido_dyna["PRO_BEGTI"]
                    pedido_h["PRO_ENDDA"] = pedido_dyna["PRO_ENDDA"]
                    pedido_h["PRO_ENDTI"] = pedido_dyna["PRO_ENDTI"]
                    #pedido_h["DURACION"] = pedido_dyna["DURACION"]
                    #if "DURACION_CALCULA" in pedido_h["DURACION_CALCULA"] :
                    #    pedido_h["DURACION_CALCULA"] = self.soap_time_to_hhmmss(pedido_dyna["DURACION_CALCULA"])
                    self.calcular_duraciones(pedido_h)
                    #pedido_h["DURACION_CALCULA"] = self.soap_time_to_hhmmss(pedido_dyna["DURACION_CALCULA"])
                    
                    #parche. PEdido dyna debiera tener agrupador , pos_agrupador, duracion_grupo
                    if "AGRUPADOR" not in pedido_dyna:
                        pedido_dyna["AGRUPADOR"] = ""
                    
                    if "POS_AGRUPADOR" not in pedido_dyna:
                        pedido_dyna["POS_AGRUPADOR"] = ""
                    
                    if "DURACION_GRUPO" not in pedido_dyna:
                        pedido_dyna["DURACION_GRUPO"] = ""
                    
                    #EL AGRUPADOR Y POS AGRUPADOR SE SACA DE SAP SIEMPRE por lo que no se le asigna pedido dyna. se deja tal cual viene de sap
                        
                    #pedido_h["AGRUPADOR"] = pedido_dyna["AGRUPADOR"]
                    #pedido_h["POS_AGRUPADOR"] = pedido_dyna["POS_AGRUPADOR"]
                    pedido_h["DURACION_GRUPO"] = pedido_dyna["DURACION_GRUPO"]
                    
                    pedido_h["ESTADO"] = pedido_dyna["ESTADO"]
                    pedido_h["VEHICLE"] = pedido_dyna["VEHICLE"]
                
                    self.check_create_pedido_key_dynamo(pedido_dyna)
                    #Agregamos info adicional 
                    pedido_h["WARNING_MESSAGE"] = pedido_dyna["WARNING_MESSAGE"]
                    pedido_h["ERROR_MESSAGE"] = pedido_dyna["ERROR_MESSAGE"]
                
                else:
                    #si hay diferencia en el estado, predomina sap, pero siempre con la duracion de athena.
                    self.calcular_duraciones(pedido_h)
                    """
                    self.check_create_pedido_key_dynamo(pedido_h)
                    tupla = list(filter(lambda x: int(x["destination"]) == int(pedido_h["KUNAG"]), self.matriz_duraciones[str(self.codigo_planta)]))
                    if len(tupla) > 0:
                        pedido_h["DURACION"] = tupla[0]["duracion"] 
                        pedido_h["DURACION_CALCULA"] = tupla[0]["duracion_calculada"] 
                    """
                    #else:
                        #print("no se encontro match entre numero de pedido y distancias. Revisar (2). Se deja durac original:" + str(pedido_h["DURACION"]))
                
        return pedidos_hibrido
    
    def calcular_duraciones(self,pedido_h):
        self.check_create_pedido_key_dynamo(pedido_h)
        tupla = []
        if len(self.matriz_duraciones) > 0:
            tupla = list(filter(lambda x: int(x["destination"]) == int(pedido_h["KUNAG"]), self.matriz_duraciones[str(self.codigo_planta)]))
        
        if self.debug:
            print("tuplaaaaaaaaaaaaaaaaaaa")
            print(tupla)
        
        
        if len(tupla) > 0:
            pedido_h["DURACION"] = tupla[0]["duracion"] 
            pedido_h["DURACION_CALCULA"] = tupla[0]["duracion_calculada"] 
    
    #given a list of sources and list of dest, give the matrix
    def get_duracion_from_athena(self,list_origin,list_destination):
        #try:
        t1_gen_cond = time.time()
        condition_origin = "origin in ( "

        i = 0
        for origin in list_origin:
            if i == len(list_origin)-1:
                condition_origin = condition_origin+str(origin)+") "    
            else:
                condition_origin = condition_origin+str(origin)+", "
            i=i+1

        condition_destination = "destination in ("
        i = 0
        for destination in list_destination:
            if i == len(list_destination)-1:
                condition_destination = condition_destination +str(destination)+") "    
            else:
                condition_destination = condition_destination +str(destination)+", "    
            i=i+1

        condition = "where "  + condition_origin + " and " + condition_destination
        #nuevo
        #condition += " and plant = \"" + str(self.codigo_planta) + "\"" 
        #condition += " and plant in (" + str(self.codigo_planta) + ")" 
        condition += " and plant = '" + str(self.codigo_planta) + "'" 
        #
        query = 'SELECT origin, destination, travel_time_minutes FROM "repositorio_optimizador"."tiempos_viaje_consolidados" ' + condition 
        print(query)
        t2_gen_cond = time.time()
        print("                ATHENA Generate condition: " + str(t2_gen_cond - t1_gen_cond))
        
        t1_sqe = time.time()
        client = boto3.client('athena')
        
        # QA 'OutputLocation': 's3://athena-results-tests/loporto'
        # 'OutputLocation': os.environ['ATHENA_OUTPUT_LOCATION']#
        queryStart = client.start_query_execution(
        
            QueryString = query,
            QueryExecutionContext = {
                'Database': 'repositorio_optimizador'
            },
            ResultConfiguration={
                'OutputLocation': os.environ['ATHENA_OUTPUT_LOCATION']#'s3://copec-athena-results'
            }
            
        )
        t2_sqe = time.time()
        print("                ATHENA start_query_execution: " + str(t2_sqe - t1_sqe))
        
        if queryStart["ResponseMetadata"]["HTTPStatusCode"] == 200:
            
            t1_qex = time.time()
            response = client.get_query_execution(
                QueryExecutionId=queryStart["QueryExecutionId"]
            )
            t2_qex = time.time()
            print("                ATHENA get_query_execution: " + str(t2_qex - t1_qex))
            
            start_time = time.time()
            #t = 0
            while response["QueryExecution"]["Status"]["State"] == "QUEUED" or response["QueryExecution"]["Status"]["State"]=="RUNNING":
                response = client.get_query_execution(
                    QueryExecutionId=queryStart["QueryExecutionId"]
                )
            end_time = time.time()
            print("                ATHENA EXECUTION QUERY: " + str(end_time - start_time))
            ##print("--- %s seconds ---" % (end_time - start_time))
            ##print(response["QueryExecution"]["Status"])
            
            
            if response["QueryExecution"]["Status"]["State"]== "SUCCEEDED":
                #ahora puedo
                #t_ini = time.time()
                
                acum_rows = []
                
                response = client.get_query_results(
                    QueryExecutionId = queryStart["QueryExecutionId"]
                )
                
                acum_rows = acum_rows + response["ResultSet"]["Rows"]
                
                nextToken = "" 
                
                if "NextToken" in response:
                    nextToken = response["NextToken"] 
                
                
                while nextToken != "":
                    if not "NextToken" in response:
                        break
                    
                    response = client.get_query_results(
                        QueryExecutionId = queryStart["QueryExecutionId"],
                        NextToken = response["NextToken"]
                    )
                    if "NextToken" in response:
                        nextToken = response["NextToken"] 
                        
                    acum_rows = acum_rows + response["ResultSet"]["Rows"]
                
                headers = {}
                ret = {}
                
                #acum_rows
                #rows = response["ResultSet"]["Rows"]
                rows = acum_rows
                if len(rows)>1:
                    i = 0
                    print("LEN ROWS")
                    print(len(rows))
                    for row in rows:
                        #print("ROW RESULT")
                        #print(row)
                        if row['Data'][0]['VarCharValue'] == "1208":
                            print(row)
                        if row['Data'][1]['VarCharValue'] == "1208":
                            print(row)
                        #print(row)
                        
                        if i == 0: 
                            #the header. Necesitamos obtener el indice de las columnas. No sabemos si siempre respetaran el mismo orden
                            col_idx = 0
                            for col in row["Data"]:
                                headers[col["VarCharValue"]]=col_idx
                                col_idx = col_idx + 1
                        else:
                            if all('VarCharValue' in x for x in row["Data"]):
                                src =  row["Data"][headers["origin"]]["VarCharValue"]
                                dest =  row["Data"][headers["destination"]]["VarCharValue"]
                                minutes = row["Data"][headers["travel_time_minutes"]]["VarCharValue"]
                                minutes_float = float(minutes)
                                #ida y vuelta
                                minutes_float = minutes_float*2
                                #sumamos tiempos de carga y descarga
                                #ver si parametrizar, idealmente sacar de la matriz de tiempos para cada planta.
                                tiempo_carga_default = 30
                                tiempo_descarga_default = 15
                                minutes_raw = minutes_float
                                minutes_float = minutes_float + tiempo_carga_default + tiempo_descarga_default
                                
                                duracion = self.get_hhmmss(minutes_float)
                                #la duracion ida y vuelta sin considerar tiempos de carga y descarga
                                duracion_raw = self.get_hhmmss(minutes_raw)
                                
                                if src not in ret:
                                    ret[src] = []
                                #minutes has the raw values from athena (just one way)
                                par = {"destination":dest, "minutes":minutes, "duracion":duracion_raw, "duracion_calculada":duracion, "tiempo_carga_default":tiempo_carga_default, "tiempo_descarga_default":tiempo_descarga_default}
                                ret[src].append(par)
                            else:
                                pass #print("vienen campos vacios")
                                #print(row["Data"])
                        i = i+1
                        
                    ##print(ret)
                    t_fin = time.time()
                    #print("Procesar respuesta Athena " + str(t_fin-t_ini))
                    return ret
                else:
                    #print("no result")
                    t_fin = time.time()
                    #print("Procesar respuesta Athena " + str(t_fin-t_ini))
                    return ret #empty..
                    #return {"msg":"no result"}
                        
            else:
                #print("there was an error")
                t_fin = time.time()
                #print("Procesar respuesta Athena " + str(t_fin-t_ini))
                return {}
                #return {"msg":"Athena error"}
        """
        except Exception as e:
            print("Hubo algun problema con Athena. Retornamos vacio, para que se usen los valores por defecto de las duraciones en SAP")
            print(e)
            return {}#{"retCode":"500", "message": e}
        """
    def get_hhmmss(self, minutes_float):
        #transform hhmmss
        h = math.floor(minutes_float/60)
        m = math.floor(minutes_float%60)
        minutes_remaining = (minutes_float%60) - m
        s = math.ceil(60*minutes_remaining)
        
        hh = str(h) if h > 10 else "0"+str(h)
        mm = str(m) if m > 10 else "0"+str(m)
        ss = str(s) if s > 10 else "0"+str(s)

        return str(hh)+str(mm)+str(ss)
    
    #This method is use to calculate a date using this formula: date = (pedido["ENT_BEGDA"] pedido["ENT_ENDTI"]) - pedido["DURACION_TRAMO1"]
    def completa_detalle(self, soap_pedidos, soap_pedidos_det):
        
        iterable_soap_pedidos = soap_pedidos
        if type(soap_pedidos).__name__ == "dict":
            iterable_soap_pedidos = [soap_pedidos]
            
        #for pedido_soap in soap_pedidos:
        for pedido_soap in iterable_soap_pedidos:
            pedido_soap["DETALLE_PEDIDO"] = []
            
            soap_pedidos_det_list = soap_pedidos_det
            if type(soap_pedidos_det).__name__ == "dict":
                soap_pedidos_det_list = [soap_pedidos_det]
                
            detalles = list(filter(lambda x: x["Vbeln"]==pedido_soap["Vbeln"], soap_pedidos_det_list))
            for detalle in detalles:
                det = {}
                det["VBELN"] = detalle["Vbeln"]
                det["MATNR"] = detalle["Matnr"]
                det["MAKTX"] = detalle["Maktx"]
                det["KWMENG"] = detalle["Kwmeng"]
                det["MEINS"] = detalle["Meins"]
                pedido_soap["DETALLE_PEDIDO"].append(det)
        
    #dynamo no soporta empty strings ("").Luego, si recupero un valor con empty string y despues intento guardar en dynamo, tendre problemas
    def normalize_empty_values(self,value):
        if type(value).__name__ == "Text" and value.strip() == "":
            return " "
        if value == None:
            return " "
        return value
    
    #pasa la info de sap a formato de programador (campos con mayusculas)
    #se hacen algunos calcluos de tiempo, que hay que discutir.
    def calculateOrderDate(self, pedidos):        
        #para obtener las distancias calculadas, necesitamos crear la lista con origenes y destinos (planta-cliente o cliente - cliente)
        lista_origenes = []
        #agregamos la planta
        lista_origenes.append(self.codigo_planta)
        lista_destinos = []
        lista_destinos.append(self.codigo_planta)
        
        pedidosList = []
        lista_vbeln = []
        currentPedido = {}
        
        iterable_pedidos = pedidos["item"]
        if type(pedidos["item"]).__name__ == "dict":
            iterable_pedidos = [pedidos["item"]]
            
        #ignoramos si viene un string vacio   
        
        if type(pedidos["item"]).__name__ == "str":
            iterable_pedidos = []
            
            
        #for pedido in pedidos["item"]:
        for pedido in iterable_pedidos:
            lista_origenes.append(int(pedido["Kunag"]))
            lista_destinos.append(int(pedido["Kunag"]))
        
            currentPedido = {}
            currentPedido["COMENTARIOS"] = ""
            if "COMENTARIOS" in pedido:
                currentPedido["COMENTARIOS"] = self.normalize_empty_values(pedido["COMENTARIOS"])    
            
            currentPedido["VBELN"] = self.normalize_empty_values(pedido["Vbeln"])    
            currentPedido["AGRUPADOR"] = self.normalize_empty_values(pedido["Agrupador"])
            currentPedido["AUART"] = self.normalize_empty_values(pedido["Auart"])
            currentPedido["BEZEI_AUART"] = self.normalize_empty_values(pedido["BezeiAuart"])    
            currentPedido["BEZEI_DELCO"] = self.normalize_empty_values(pedido["BezeiDelco"])    
            currentPedido["BSARK"] = self.normalize_empty_values(pedido["Bsark"])
            currentPedido["CAMION_DEDICADO"] = self.normalize_empty_values(pedido["CamionDedicado"])    
            currentPedido["CMGST"] = self.normalize_empty_values(pedido["Cmgst"])    
            
            #currentPedido["CODIGO_PLANTA"] = self.normalize_empty_values(pedido["CODIGO_PLANTA"])    
            
            currentPedido["CUENTA_LITROS"] = self.normalize_empty_values(pedido["CuentaLitros"])    
            currentPedido["DELCO"] = self.normalize_empty_values(pedido["Delco"])    
            currentPedido["DETALLE"] = self.normalize_empty_values(pedido["Detalle"])
            
            #currentPedido["DETALLE_PEDIDO"] = self.completa_detalle(self.normalize_empty_values(pedido["Vbeln"])) #self.normalize_empty_values(pedido["DETALLE_PEDIDO"]    
            currentPedido["DETALLE_PEDIDO"] = self.normalize_empty_values(pedido["DETALLE_PEDIDO"])    
            
            
            currentPedido["DURACION"] = self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["Duracion"]))    
            currentPedido["DURACION_CALCULA"] = self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["DuracionCalcula"]))    
            currentPedido["DURACION_CLIENTES"] = self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["DuracionClientes"]))    
            currentPedido["DURACION_GRUPO"] = self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["DuracionGrupo"]))    
            currentPedido["DURACION_TRAMO1"] = self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["DuracionTramo1"]))    
            currentPedido["ENT_BEGDA"] = self.soap_date_to_yyyymmdd(self.normalize_empty_values(pedido["EntBegda"]))    
            currentPedido["ENT_BEGTI"] = self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["EntBegti"]))
            currentPedido["ENT_ENDDA"] = self.soap_date_to_yyyymmdd(self.normalize_empty_values(pedido["EntEndda"]))
            currentPedido["ENT_ENDTI"] = self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["EntEndti"]))
            currentPedido["ESPECIAL"] = self.normalize_empty_values(pedido["Especial"])
            currentPedido["ESTADO"] = self.normalize_empty_values(pedido["Estado"])
            
            #currentPedido["FECHA_PEDIDO"] = self.normalize_empty_values(pedido["FECHA_PEDIDO"])
            
            currentPedido["GROUPNAME"] = self.normalize_empty_values(pedido["Groupname"])
            currentPedido["GROUPTEXT"] = self.normalize_empty_values(pedido["Grouptext"])
            currentPedido["KUNAG"] = self.normalize_empty_values(pedido["Kunag"])
            currentPedido["NAME1_KUNAG"] = self.normalize_empty_values(pedido["Name1Kunag"])
            currentPedido["POS_AGRUPADOR"] = self.normalize_empty_values(pedido["PosAgrupador"])
            currentPedido["PRO_BEGDA"] = self.soap_date_to_yyyymmdd(self.normalize_empty_values(pedido["ProBegda"]))
            currentPedido["PRO_BEGTI"] = self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["ProBegti"]))
            currentPedido["PRO_ENDDA"] = self.soap_date_to_yyyymmdd(self.normalize_empty_values(pedido["ProEndda"]))
            currentPedido["PRO_ENDTI"] = self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["ProEndti"]))
            
            #    currentPedido["UTC_ERROR"] = self.normalize_empty_values(pedido["UTC_ERROR"])
            
            currentPedido["VBTYP"] = self.normalize_empty_values(pedido["Vbtyp"])
            currentPedido["VDATU"] = self.normalize_empty_values(pedido["Vdatu"])
            currentPedido["VEHICLE"] = self.normalize_empty_values(pedido["Vehicle"])
            currentPedido["VTEXT"] = self.normalize_empty_values(pedido["Vtext"])
            currentPedido["VTEXT_BSARK"] = self.normalize_empty_values(pedido["VtextBsark"])
            currentPedido["ZONE1"] = self.normalize_empty_values(pedido["Zone1"])
            """
            #OLD LOGIC. 
            # ***
            year = int(self.soap_date_to_yyyymmdd(self.normalize_empty_values(pedido["EntEndda"]))[:4])
            month = int(self.soap_date_to_yyyymmdd(self.normalize_empty_values(pedido["EntEndda"]))[4:6])
            day = int(self.soap_date_to_yyyymmdd(self.normalize_empty_values(pedido["EntEndda"]))[6:8])
            hour=int(self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["EntEndti"]))[:2])
            minutes=int(self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["EntEndti"]))[2:4])
            seconds=int(self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["EntEndti"]))[4:6])
            hourTramo1=int(self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["Duracion"]))[:2])/2
            minutesTramo1=int(self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["Duracion"]))[2:4])/2
            secondsTramo1=int(self.soap_time_to_hhmmss(self.normalize_empty_values(pedido["Duracion"]))[4:6])/2

            #info: date = (pedido["ENT_ENDDA"] pedido["ENT_ENDTI"]) - pedido["DURACION_TRAMO1"]
            # El tramo 1 de acuerdo a copec es simplemente tramo / 2 ya que no se necesita añadir la hora de carga
            entEndda = datetime.datetime(year,month,day,hour,minutes,seconds)
            entEnddaMinusTramo1 = entEndda - datetime.timedelta(hours=hourTramo1,minutes=minutesTramo1,seconds=secondsTramo1)
                       
            entEnddaDay = entEndda.day
            entEnddaMinusTramo1Day = entEnddaMinusTramo1.day
            #VER SI ESTA LOGICA DESDE *** VA A IR CON SAP
            #ESTO HACE UNA DIFERENCIA Y DETERMINA SI EL PEDIDO ES PARA HOY O NO. Discutir con pablo
            #print("discutir con pablo esta logica. Creo que con sap, debieramos agregar todo lo que viene al pedido|")
            if entEnddaDay == entEnddaMinusTramo1Day:
                pedidosList.append(currentPedido)
                lista_vbeln.append(pedido["Vbeln"])
            """
            pedidosList.append(currentPedido)
            lista_vbeln.append(pedido["Vbeln"])
    
        #print("[calculateOrderDate] get_duracion_from_athena")
        t1_athena = time.time()
        self.matriz_duraciones = self.get_duracion_from_athena(lista_origenes,lista_destinos)
        t2_athena = time.time()
        print(">> Obtener duraciones ATHENA: " + str(t2_athena-t1_athena))
        print("        ===========[calculateOrderDate]")

        return {"pedidosList":pedidosList,"lista_vbeln":lista_vbeln}