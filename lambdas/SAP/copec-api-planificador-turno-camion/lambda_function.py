from lib.ms_dynamo_services import DynamoService
import time

import os

def lambda_handler(event, context):
    try:
    
        dyna_table = os.environ['DYNA_TABLE']
        #dyna_table = "Registro_Turnos_Camiones"
        #recibir planta,fecha,camion,motivo,codigo motivo
        
        #event = request.json
        
        
        planta = event["plant_code"]
        fecha = event["date"]
        camion = event["vehicle"]
        motivo = event["reason"]
        motivo_code = event["reason_code"]
        tipo_operacion = event["tipo_operacion"]
        comentarios = event["comentarios"]
        
        #tomar timestamp
        
        #generar input pa dynamo
        #se guardan las dadas de baja de los camiones
        input = {}
        #Key: planta-fecha-camion-timestamp
        timestamp = int(time.time())
        
        input["planta-fecha-camion-timestamp"] = planta + "-" + fecha + "-" + camion + "-" + str(timestamp)
        input["timestamp"] =  timestamp
        input["motivo"] = motivo
        input["motivo_code"] = motivo_code
        input["tipo_operacion"] = tipo_operacion
        input["comentarios"] = comentarios
        #escribir en dynamo
        table_dyna_shift_truck = dyna_table
        service_dynamo = DynamoService(table_dyna_shift_truck)
        dynamo_response = service_dynamo.put_item(input)
        
        print(dynamo_response)
        if dynamo_response["code"] == "ok":
            return {"retCode":"200"}
        else:
            return {"retCode":"500", "message":dynamo_response["msg"]}
    
    except Exception as e:
        return {"retCode":"500", "message": e}