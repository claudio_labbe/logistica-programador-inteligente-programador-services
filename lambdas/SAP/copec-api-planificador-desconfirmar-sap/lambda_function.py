from lib.truck_structure_generator import TruckStructureGenerator
from lib.orders_structure_generator import OrdersStructureGenerator
from lib.orders_structure_store import storeOrdersDynamo
from lib.soap_service import soapService
import time
import math

import os

from datetime import datetime
from datetime import timedelta

def lambda_handler(event, context):
    #event = {"plan": [{pedido-ith formato programador para confirmar},{}], "codigo_planta":["1208"], "fecha":...}
    
    sap_user = event["sap_user"]
    sap_pwd = event["sap_pwd"]
    sap_url = os.environ['SAP_URL']
    #"http://coqabroker1.copec.cl:7812/programador/zws_pr_prog_sap_desconfirmar?wsdl"
    
    sap_url_get_plan = os.environ['SAP_URL_GET_PLAN']
    #'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
    
    dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
    dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
    
    input = {"items": event["plan"]}
     
    try:
        #PASOS PARA GUARDAR
        #1 GUARDAMOS EN SAP
        # ..todo comandos
        service_sap = soapService()
        service_sap.createClient(sap_user,sap_pwd,sap_url)
        sap_response = service_sap.deconfirm(input)
        
        print(sap_response.item)
        ##para cada pedido en la respuesta
        
        vbeln_desconfirmados = []
        messageResp = ""
        responseCode = None
        okMessageWritten = False
        #Criterio para retornar codigo sap de varios pedidos desconfirmados. Si uno de ellos es 0, se retorna 0 (ok)
        for response_item in sap_response.item:
            print(response_item)
            if response_item.Subrc == 0:
                responseCode = response_item.Subrc
                vbeln_desconfirmados.append(response_item.Vbeln)
                if okMessageWritten != True:
                    messageResp += response_item.Message;
                    okMessageWritten= True
                
            
            else:
                if responseCode == None:
                    responseCode = response_item.Subrc
                messageResp += "  " + response_item.Message;
        
        #ahora, para cada pedido que se desconfirma (especificado dentro de objeto_pedido), veo si su vbeln esta dentro de los items desconfirmados y lo guardo en Dynamo
        
        for plan in event["plan"]:
            pedidos_desconfirmados=[]
            
            if type(plan["OBJETO_PEDIDO"]).__name__=="list":
                for obj in plan["OBJETO_PEDIDO"]:
                    if obj["VBELN"] in vbeln_desconfirmados:
                        print("D")
                        obj["ESTADO"] = "1"
                        obj["VEHICLE"] = " "
                        obj["AGRUPADOR"] = " "
                        pedidos_desconfirmados.append(obj)
            
            if type(plan["OBJETO_PEDIDO"]).__name__=="dict":
                obj = plan["OBJETO_PEDIDO"]
                if obj["VBELN"] in vbeln_desconfirmados:
                        print("D")
                        obj["ESTADO"] = "1"
                        obj["VEHICLE"] = " "
                        obj["AGRUPADOR"] = " "
                        pedidos_desconfirmados.append(obj)
                        
            input_dyna = {"items": pedidos_desconfirmados,"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
            #print("input dyna")
            #print(input_dyna)
            
            #dejarlo arriba, como env. 
            table_dynamo_pedidos_user = dyna_pedidos_usr
            service_dynamo = storeOrdersDynamo(table_dynamo_pedidos_user)


            current_time =  datetime.utcnow()
            if(event["timestamp"] != -1 and event["timestamp"] != None):
                timestamp = str(event["timestamp"])
                print("UTILIZANDO TIMESTAMP DESDE FRONT EN DESCONFIRMAR")
            else:
                print("UTILIZANDO TIMESTAMP GENERADO AHORA")
                timestamp = str(int(float(current_time.timestamp())))
            print(timestamp)
            ttl = int(float((current_time + timedelta(days=5)).timestamp()))   
            
            dynamo_store_succes = service_dynamo.store(input_dyna,timestamp,ttl)
            """
            print("ejeutada orden")
            #Guardamos los cambios de desconfirmacion en dynamo
            tries_dyna = 3
            count_dyna = 0
            while (dynamo_store_succes != True) and count_dyna != tries_dyna :
                dynamo_store_succes = service_dynamo.store(input_dyna)
                count_dyna = count_dyna + 1
                time.sleep(0.5)
            """
            #if dynamo_store_succes != True:
            #    return {"retCode":"200","progStatus":"error","message":"Se desconfirman pedidos de la lista en SAP, pero no se puede desconfirmar en dynamo","lista_desconfirmados":ret_id_pedidos_desconfirmados, "ERc" :sap_response_code, "EMessage":sap_response_message}
        return {"retCode":"200","progStatus":"ok","message":"Se desconfirman pedidos de la lista en SAP y se desconfirma en Dynamo","lista_desconfirmados":vbeln_desconfirmados, "ERc" :responseCode, "EMessage":messageResp}
   
    except Exception as e:
        print("err lcals service deconcifrmar")
        #print(e)
        return {"retCode":"500", "message":e,"ERc" :responseCode, "EMessage":messageResp}     
    
    