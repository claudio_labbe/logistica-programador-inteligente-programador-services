from .ms_dynamo_services import DynamoService
import datetime
import boto3
import time
import math 

class storeOrdersDynamo:

    def __init__(self,tale_name): 
        store_table = tale_name 
        self.service = DynamoService(store_table)
      
    
    def store(self,input,ts,ttl):
        #ts = int(time.time())
        
        for pedido in input["items"]:
            #asignamos timestamp al pedido de dynamo.
            pedido["WRITE_TIMESTAMP"] = ts
            pedido["TTL"] = ttl
            pedido["PRO_BEGDA"] = pedido["PRO_BEGDA"].replace("-","")
            pedido["PRO_BEGTI"] = pedido["PRO_BEGTI"].replace(":","")
            pedido["PRO_ENDDA"] = pedido["PRO_ENDDA"].replace("-","")
            pedido["PRO_ENDTI"] = pedido["PRO_ENDTI"].replace(":","")
            pedido["DURACION"] = pedido["DURACION"].replace(":","")
            pedido["DURACION_GRUPO"] = pedido["DURACION_GRUPO"].replace(":","")
        
        try:
            
            self.service.put_items(input)
            return True
        except Exception as e:
            print(e)
            return False
