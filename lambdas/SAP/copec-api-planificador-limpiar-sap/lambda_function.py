from lib.truck_structure_generator import TruckStructureGenerator
from lib.orders_structure_generator import OrdersStructureGenerator
from lib.orders_structure_store import storeOrdersDynamo
from lib.soap_service_zeep import soapServiceZeep
import time
import math

import os

def lambda_handler(event, context):
    
    try:
        
        tg = TruckStructureGenerator()
        og = OrdersStructureGenerator()

        #el event
        #event = {"codigo_fecha":["20200506"], "codigo_planta":["1208"]}
        #event = request.json
        
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        sap_url = os.environ['SAP_URL']
        #sap_url = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
        #'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'

        dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
        dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
        
        
        input_order = {"fecha_pedido":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}
        input_truck = {"fecha_plan":event["codigo_fecha"], "codigo_planta":event["codigo_planta"]}

        fechaSoap = event["codigo_fecha"][0:4] + "-" + event["codigo_fecha"][4:6] + "-" + event["codigo_fecha"][6:8] 
        #soap_service = soapService()
        soapService_zeep = soapServiceZeep()
        #NO HARDCODEAR USER PASS. PASARLO cOMO PARAMETRO
        
        #soap_service.createClient(user,pwd,sap_url)
        soapService_zeep.createClient(sap_user,sap_pwd,sap_url)
        
        #soap_response = soap_service.get_plan("2020-05-06",input["codigo_planta"])
        #soap_response = soap_service.clear(fechaSoap,event["codigo_planta"])
        
        print(fechaSoap)
        print(event["codigo_fecha"])
        print("befho.")
        
        soap_response = soapService_zeep.clear(fechaSoap,[event["codigo_planta"]])
        soap_vehicles_cab = soap_response["EtVehicles"]
        soap_pedidos = soap_response["EtPedidos"]
        soap_pedidos_det = soap_response["EtPedidoDet"]
        
        sap_response_code = soap_response["ERc"]
        sap_response_message = soap_response["EMessage"]

        input_truck["soap_vehicles_cab"] = soap_vehicles_cab
        input_order["soap_pedidos"] = soap_pedidos
        input_order["soap_pedidos_det"] = soap_pedidos_det
        input_order["merge_dynamo"] = True
        input_order["clean"] = True
        input_order["pedidos_opt_dynamo"] = dyna_pedidos_opt
        input_order["pedidos_usr_dynamo"] = dyna_pedidos_usr
        response_truck = tg.generate_structure(input_truck)
        print("DDD")
        response = og.generate_structure(input_order)
        print("EEE")
        response["truck"] = response_truck   
        
        response["ERc"] = sap_response_code
        response["EMessage"] = sap_response_message
        print("DEBUG RESPONSE")
        print(response)
        return response
    
    except Exception as e:
        #print(e)
        return {"retCode":"500", "message": e}