import boto3
import json
import base64
import zlib
import os
import time
import sys
#triggered by sqs. Dispatch
def lambda_handler(event, context):
    sys.setrecursionlimit(2500)
    lambda_start = time.time()
    print("CONSUMER START AT TIMESTAMP: "+str(lambda_start))
    print("#Event")
    print(event)
    
    #table_name = "Sqs_copec_programador"
    table_name = os.environ['SQS_DYNAMO_TABLE']
    
    msg_id = event["msg_id"]
    
    
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(table_name)
    response = table.get_item(Key={'message_id': msg_id})
    
    if "Item" in response:
        #print(response)
        #print(response["Item"])
        has_error = False
        has_s3 = False
        if "s3_reference_body" in response["Item"]: 
            result = response["Item"]["s3_reference_body"]
            has_s3 = True
        if "message_body" in response["Item"]: 
            result = response["Item"]["message_body"]
        if "error" in response["Item"]: 
            result = response["Item"]["error"]
            has_error = True
            
        
        #borramos lo consumido
        #no borramos por ahora para poder tener acceso a las planificaciones 
        #de los usuarios como log mientras se ajusta el sistema en su primera etapa
        #response = table.delete_item(
        #    Key={'message_id': msg_id}
        #)
        
        if has_error:
            print("#Has error")
            return result
        
        #result is boto3 Binary. Need to recover, unzip and return
        bresult = result.__str__()
        
        if has_s3:
            s3_bucket = response["Item"]["s3_bucket"]
            s3_key = response["Item"]["s3_key"]
            s3 = boto3.resource('s3')
            object = s3.Object(s3_bucket, s3_key)
            obj = object.get()
            bresult = obj["Body"].read()
            
        
        
        dec = zlib.decompress(bresult)
        ret_json = json.loads(dec.decode('utf-8'))
        
        s3_client = boto3.client('s3')
        
        #LAMBDA no retorna respuestas mayores a 6 MB. 
        #en vez de entregarle esto al cliente, le daremos una url signed 
        #hosteada en s3, que tendra el contenido.
        s3_bucket_output = os.environ['S3_SQS_ROOT']
        s3_key_output = "sqs_stored/signed-url/" +  msg_id
        
        # Generate the URL to get 'key-name' from 'bucket-name'
        url = s3_client.generate_presigned_url(
            ClientMethod='get_object',
            Params={
                'Bucket':s3_bucket_output,
                'Key': s3_key_output
            }
        )
        #escribimos
        s3_client.put_object(Body=dec.decode('utf-8'), Bucket=s3_bucket_output, Key=s3_key_output)
        

        lambda_end = time.time()
        print("CONSUMER END AT TIMESTAMP: "+str(lambda_end))

        return url
        
    return []
    
    
