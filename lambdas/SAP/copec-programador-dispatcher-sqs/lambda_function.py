import boto3
import json
import os
import time

import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#triggered by sqs. Dispatch
def lambda_handler(event, context):
    #lambda_start = time.time()
    #print("DISPATCHER START AT TIMESTAMP: "+str(lambda_start))
    logging.info

    message_id = event["Records"][0]["messageId"]
    logger.info('## MessageId')
    logging.info(message_id)
    try:
        sqs = boto3.client('sqs')
        
        queue_url = os.environ['SQS_URL']
        table_name = os.environ['SQS_DYNAMO_TABLE']
        
        response = event["Records"][0]
        
        message_body = event["Records"][0]["body"]
        receipt_handle =  event["Records"][0]["receiptHandle"]
        
        logging.info(message_body)
        logging.info(receipt_handle)
        #print(event)
        #print(receipt_handle)
        
        mb = json.loads(message_body)
        
        logging.info(mb)
        #del s3
        json_function = mb["function"]
        s3_bucket = mb["s3_bucket"]
        s3_key = mb["s3_key"]
        
        #retrieve data from s3
        s3 = boto3.resource('s3')
        object = s3.Object(s3_bucket, s3_key)
        obj = object.get()
        logging.info('##Body')
        logging.info(obj["Body"])
        bresult = obj["Body"].read()
        
        logging.info('##bresult')
        logging.info(bresult)
        json_request = json.loads(bresult.decode('utf-8'))
        logging.info('## json_request')
        logging.info(json_request)
        logging.info(json_request['request'])
        
        if(json_request['function']=='nueva_propuesta'):
            del json_request['request']['pedidos']
            json_request['request']['s3_bucket'] = s3_bucket
            json_request['request']['s3_key'] = s3_key
        
        
        #get_plan, get_msg, other...
        logging.info('## FINAL json request')
        logging.info(json_request)
        # Delete received message from queue
        sqs.delete_message(
            QueueUrl=queue_url,
            ReceiptHandle=receipt_handle
        )
        
        
        lambdaClient = boto3.client("lambda")
        lambdaArn = ""
        if json_function == "get_plan":
            lambdaArn = os.environ['LAMBDA_CHILD_ARN_GET_PLAN']
        if json_function == "get_msg":
            lambdaArn = os.environ['LAMBDA_CHILD_ARN_GET_MSG']
        if json_function == "clean":
            lambdaArn = os.environ['LAMBDA_CHILD_ARN_CLEAN']
        if json_function == "nueva_propuesta":
            lambdaArn = os.environ['LAMBDA_CHILD_ARN_NUEVA_PROPUESTA']
        if json_function == "get_all_msg":
            lambdaArn = os.environ['LAMBDA_CHILD_ARN_GET_ALL_MESSAGE']
            
            
        lambda_end = time.time()
        #print("DISPATCHER END AT TIMESTAMP: "+str(lambda_end))
        

        responseLambda = lambdaClient.invoke(
                FunctionName = lambdaArn,
                InvocationType = "RequestResponse",
                Payload = json.dumps({'message_id': message_id , 'body':json_request["request"]}, indent=2).encode('utf-8') #json.loads(message_body)
                
                
        )
        logging.info(responseLambda)
    except Exception as e:
        print(e)
        #write error in dyna
        #store response in DB, using the id of sqs msg
        sqs_table = os.environ['SQS_DYNAMO_TABLE']
        item = {"message_id":message_id,  "error":str(e),"function":"copec_programador_dispatcher_sqs"}
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(sqs_table)
        db_response = table.put_item(
            Item = item
        )
    