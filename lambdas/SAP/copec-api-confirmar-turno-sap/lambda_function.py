from lib.soap_service import soapService
import time
import math

import os

def lambda_handler(event, context):
    try:
        #event.plan debe recibir los pedidos a confirmar en el estado en el que vienen del programador. 
        #El servicio tendra que filtrar que guardar 
        sap_user = event["sap_user"]
        sap_pwd = event["sap_pwd"]
        sap_url = os.environ['SAP_URL']
        #sap_url = "http://coqabroker1.copec.cl:7812/programador/zws_pr_prog_sap_confirmar?wsdl"
        
        #sap_url_get_plan = os.environ['SAP_URL_GET_PLAN']
        #sap_url_get_plan = 'http://coqabroker1.copec.cl:7812/programador/zws_pr_obtiene_prog_sap?wsdl'
        
        #dyna_pedidos_usr = os.environ['DYNA_PEDIDOS_USR'] #"Pedido_dev_usr"
        #dyna_pedidos_opt = os.environ['DYNA_PEDIDOS_OPT'] #"Pedido_dev_opt"
        
        input = {"items": event["plan"],"codigo_planta":event["codigo_planta"],"fecha":event["fecha"]}
        #1 GUARDAMOS EN SAP
        service_sap = soapService()
        service_sap.createClient(sap_user,sap_pwd,sap_url)
        sap_response = service_sap.confirmCambioTurno(input)
        #cuando confirmamos y no hay problemam ERc es 0. El mensaje es "espere un momento hasta que se confirme"
        #vamos a consultar sap hasta que veamos que la lista de pedidos este confirmada
        response = {}
        response["retCode"]="200"
        if sap_response.ERc != 0:
            response["retCode"]="500"
            
        #if sap_response.ERc == 0:
            #TODO TERMINAR ESTO sap serv esta listo    
            #return {"retCode":"200","progStatus":"ok","message":"Se confirman pedidos de la lista en SAP y se confirma en Dynamo","lista_confirmados":id_pedidos_confirmados, "ERc":sap_response["ERc"],"EMessage":sap_response["EMessage"]}
         
        response["ERc"] = sap_response["ERc"]
        response["EMessage"] = sap_response["EMessage"]
        return response

    except Exception as e:
        print("exception")
        print(e)
        return {"retCode":"500", "message":e}