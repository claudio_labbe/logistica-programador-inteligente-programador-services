# Esto es la tabla de disponibilidades
from lib.ms_dynamo_services import DynamoService
import csv
import sys


def generate_json_input(csv_row):
    json_input = {}
    for elem in csv_row:
        val = csv_row[elem]
        if val == '':
            val = ' '
        json_input[elem] = val
    return json_input


def main():
    print("putting data into vehiculos_cab")
    service = DynamoService("Vehiculos_cab")
    filename = sys.argv[1]

    fecha_plan = filename.split('_')[-1].split('.')[-2]
    codigo_planta = filename.split('_')[-2]

    batch_json = {}
    json_list = []

    with open(filename, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter='\t')
        line_count = 0
        for row in csv_reader:
            json_input = generate_json_input(row)
            # primary key viene determinada por la combinacion de estas tres variables
            pk = row['VEHICLE'] + "-" + row["START_DATE"] + "-" + row["START_TIME"]
            json_input["ID"] = pk

            json_input["FECHA_PLAN"] = fecha_plan
            json_input["CODIGO_PLANTA"] = codigo_planta

            json_list.append(json_input)
            # line_count+=1
    batch_json["items"] = json_list
    response = service.put_items(batch_json)


if __name__ == "__main__":
    main()

"""
def handler_put(event, context):
    service = DynamoService("Flota")
    response=service.put_item(event)
    return response

#event should contain a field id_vehiculo of  type string
def handler_get(event, context):
    service = DynamoService("Flota")
    response=service.get_item(event)
    return response
"""
