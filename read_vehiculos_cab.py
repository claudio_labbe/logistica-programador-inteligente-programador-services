from lib.ms_dynamo_services import DynamoService
import sys 
#import json
import boto3
#from boto3.dynamodb.conditions import And, Attr
#from functools import reduce
#from operator import and_

def create_request_items_list():

    # filename = sys.argv[1]
    # f = open(filename)
    # request_items = json.loads(f.read())

    request_items = {"FECHA_PEDIDO": ["0208"],
                     "CODIGO_PLANTA": ["1216"]}
    return request_items

def main():

    print("Scanning data from vehiculos_cab table")
    service = DynamoService("Pedidos")
    request_items = create_request_items_list()
    #query_params = build_query_params(request_items)
    #response = service.scan_items(query_params)
    response = service.scan_items(request_items)
    items = response['Items']
    print(items)
    
"""
def build_query_params(request_items: dict):
    query_params = {}
    if len(request_items) > 0:
        query_params["FilterExpression"] = add_expressions(request_items)
    return query_params

def add_expressions(request_items: dict):
    if request_items:
        conditions = []
        for key, value in request_items.items():
            if isinstance(value, str):          
                conditions.append(Attr(key).eq(value))
            if isinstance(value, list):
                conditions.append(Attr(key).is_in([v for v in value]))
        return reduce(and_, conditions)
"""
if __name__ == "__main__":
    main()
