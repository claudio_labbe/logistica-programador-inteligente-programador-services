from lib.ms_dynamo_services import DynamoService
import csv
import sys
import io
import re


def generate_json_input(csv_row):
    json_input = {}
    for elem in csv_row:
        val = csv_row[elem]
        if val == '':
            val = ' '
        json_input[elem] = val
    return json_input


def main():
    print("putting data into vehiculos_det")
    service = DynamoService("Vehiculos_det")
    # csv detalles de vehiculo. Un row por vehiculo
    filename_det = sys.argv[1]
    # csv detalles de compartimiento
    filename_det_comp = sys.argv[2]

    fecha_plan = filename_det.split('_')[-1].split('.')[-2]
    codigo_planta = filename_det.split('_')[-2]

    batch_json = {}
    json_list = []

    comp_det = {}

    cleansed_file = ''

    with open(filename_det_comp, mode='r') as csv_file:
        skip_lines = 3
        line_count = 0
        for row in csv_file:
            if line_count >= skip_lines and re.match(r'^[_\W]+$', row) is None:
                cleansed_file = cleansed_file + \
                                '|'.join([element.strip() for element in row.split('|')])[2:-1] + \
                                '\n'
            # csv_file.__next__()
            line_count += 1

        csv_reader = csv.DictReader(io.StringIO(cleansed_file), delimiter='|')

        for row in csv_reader:
            vehicle_id = row["TU_NUMBER"]
            print(vehicle_id)
            if vehicle_id not in comp_det:
                comp_det[vehicle_id] = []
            json_input = generate_json_input(row)
            comp_det[vehicle_id].append(json_input)

    with open(filename_det, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter='\t')
        # line_count = 0
        for row in csv_reader:
            # print(comp_det[row["VEHICLE"]])
            json_input = generate_json_input(row)
            json_input["DETALLE_COMPARTIMIENTO"] = comp_det[row["VEHICLE"]]
            json_input["FECHA_PLAN"] = fecha_plan
            json_input["CODIGO_PLANTA"] = codigo_planta
            json_list.append(json_input)
            # print(json_input)
            # response=service.put_item(json_input)
    batch_json["items"] = json_list
    response = service.put_items(batch_json)


if __name__ == "__main__":
    main()
