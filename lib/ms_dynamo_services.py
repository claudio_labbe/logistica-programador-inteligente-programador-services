import boto3
import sys
import json
from boto3.dynamodb.conditions import And, Attr
from functools import reduce
from operator import and_

class DynamoService:

    # {items:[{...},{...}..]
    def put_items(self, items):
        with self.table.batch_writer() as batch:
            idx = 1
            for it in items["items"]:
                batch.put_item(Item=it)
                print("up " + str(idx) + " elements...")
                idx = idx + 1
        return "put items finished"

    # Method to retrieve data from vehiculos_cab table
    def scan_items(self, request_items):
        query_params = self.build_query_params(request_items)
        response = self.table.scan(self.table, **query_params)
        return response

    # vechiculo es un json que debe tener un atributo id_vehiculo de tipo string
    def put_item(self, item):
        response = ""
        try:
            response = self.table.put_item(
                Item=item
            )
            # si no falla, se inserta
            response = {'code': 'ok'}
        except:
            response = {'code': 'error'}
        return response

    # jsonRequest debe traer el json con los ids a buscar
    def get_item(self, jsonRequest):
        response = ""
        try:
            response = self.table.get_item(
                Key=jsonRequest
            )

            if ('Item' in response):
                item = response['Item']
                return {'row': item, 'msg': 'ok'}
            else:
                print('no item to show')
        except:
            response = {'code': 'error'}

        return {'row': {}, 'msg': 'no item to show'}

    def __init__(self, tableName,runlocal=False):
        print("calling constructor")
        if not runlocal :
            dynamodb = boto3.resource('dynamodb')
        else:
            dynamodb = boto3.resource('dynamodb', region_name='local', aws_access_key_id='local', aws_secret_access_key='local', endpoint_url="http://localhost:8000")
        self.table = dynamodb.Table(tableName)

    def build_query_params(self, request_items: dict):
        query_params = {}
        if len(request_items) > 0:
            query_params["FilterExpression"] = self.add_expressions(request_items)
        return query_params

    def add_expressions(self, request_items: dict):
        if request_items:
            conditions = []
            for key, value in request_items.items():
                if isinstance(value, str):          
                    conditions.append(Attr(key).eq(value))
                if isinstance(value, list):
                    conditions.append(Attr(key).is_in([v for v in value]))
            return reduce(and_, conditions)

def main():
    flotaService = FlotaService()
    # flotaService.put_item({'id_vehiculo':'2222'})
    # flotaService.get_item('2a222')


if __name__ == "__main__":
    main()
