from lib.ms_dynamo_services import DynamoService
import csv
import sys


def generate_json_input(csv_row):
    json_input = {}
    for elem in csv_row:
        val = csv_row[elem]
        if val == '':
            val = ' '
        json_input[elem] = val
    return json_input


def main():
    print("putting data into pedidos")
    service = DynamoService("Pedidos")
    # csv pedidos cab(cabecera, tabla principal de pedidos)
    filename_cab = sys.argv[1]
    # csv pedidos detalle
    filename_det = sys.argv[2]

    batch_json = {}
    json_list = []

    pedido_det = {}

    fecha_plan = filename_cab.split('_')[-1].split('.')[-2]
    codigo_planta = filename_cab.split('_')[-2]

    with open(filename_det, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter='\t')
        line_count = 0
        for row in csv_reader:
            # if line_count < 10:
            pedido_id = row["VBELN"]
            if pedido_id not in pedido_det:
                pedido_det[pedido_id] = []
            json_input = generate_json_input(row)
            pedido_det[pedido_id].append(json_input)
            # line_count+=1

    with open(filename_cab, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter='\t')
        # line_count = 0
        for row in csv_reader:
            json_input = generate_json_input(row)
            json_input["DETALLE_PEDIDO"] = pedido_det[row["VBELN"]]
            json_input["FECHA_PEDIDO"] = fecha_plan
            json_input["CODIGO_PLANTA"] = codigo_planta

            json_list.append(json_input)
            # print(json_input)
            # response=service.put_item(json_input)
    batch_json["items"] = json_list
    response = service.put_items(batch_json)


if __name__ == "__main__":
    main()
