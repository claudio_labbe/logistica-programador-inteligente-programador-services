from lib.ms_dynamo_services import DynamoService

def main():
    print("main")
    service = DynamoService("Vehiculos_cab")
    #response = service.put_item({'id':'monster666'})
    #response = service.get_item({'id':'monster666'})
    #print(response)
    return response

if __name__ == "__main__":
    main()

def handler_put(event, context):
    service = DynamoService("Flota")
    response=service.put_item(event)
    return response

#event should contain a field id_vehiculo of  type string
def handler_get(event, context):
    service = DynamoService("Flota")
    response=service.get_item(event)
    return response
