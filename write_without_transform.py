from lib.ms_dynamo_services import DynamoService
import csv
import sys


def generate_json_input(csv_row):
    json_input = {}
    for elem in csv_row:
        val = csv_row[elem]
        if val == '':
            val = ' '
        json_input[elem] = val
    return json_input


def main():
    filename = sys.argv[1]
    tablename = sys.argv[2]
    filedelimiter = sys.argv[3]

    print("putting data into %s" % tablename)
    service = DynamoService(tablename)
    # csv plantas

    batch_json = {}
    json_list = []

    with open(filename, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=filedelimiter)
        line_count = 0
        for row in csv_reader:
            # if line_count < 10:
            json_input = generate_json_input(row)
            json_list.append(json_input)
            # line_count+=1

    batch_json["items"] = json_list
    response = service.put_items(batch_json)


if __name__ == "__main__":
    main()
