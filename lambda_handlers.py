from lib.ms_dynamo_services import DynamoService

def handler_put(event, context):
    service = DynamoService("Flota")
    response=service.put_item(event)
    return response

#event should contain a field id_vehiculo of  type string
def handler_get(event, context):
    service = DynamoService("Flota")
    response=service.get_item(event)
    return response
