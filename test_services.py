from lib.ms_dynamo_services import DynamoService
import pytest
#debiera haber un before y after test, donde se cree y bote la tabla de tests
import time

fixture_finished="NO"

@pytest.fixture(scope="module", autouse=True)
def prepare():
    for i in range(1,11):
        print("Waiting..." + str(i))
        time.sleep(1)
    fixture_finished="YES"

def test_insert_item():
    print("test_insert_item")
    print(fixture_finished)
    service=DynamoService("ds_dtt_table")
    response=service.put_item({'id':'testingId','col1':'col 1'})
    assert response.get("code")=="ok"


def test_insert_item_error():
    print("test_insert_item_error")
    print(fixture_finished)
    service=DynamoService("ds_dtt_table")
    response=service.put_item({'idWrong':'testingId','col1':'col 1'})
    assert response.get("code")=="error"


def test_get_item():
    print("test_get_item")
    print(fixture_finished)
    service=DynamoService("ds_dtt_table")
    response=service.get_item({'id':'testingId'})
    assert response.get("row").get("id")=="testingId"

def test_get_item_not_found():
    print("test_get_item_not_found")
    print(fixture_finished)
    service=DynamoService("ds_dtt_table")
    response=service.get_item({'id':'testingId_noExists'})
    assert response.get("msg")=="no item to show"




